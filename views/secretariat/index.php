<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SecretariatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Secretariats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="secretariat-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Adauga secretar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nume',
            'prenume',
            'email:email',
            'parola',
            // 'data_inregistrarii',
            // 'data_actualizarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
