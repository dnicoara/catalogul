<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Selectarea  arhivei pe care doriti sa o folositi';
$this->params['breadcrumbs'][] = $this->title;
$baza_date = \Yii::$app->params['database'];
?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>
    <br>

    <div class="classSecretariat-form">

        <?php $form = ActiveForm::begin([
            'action' => ['secretariat/afiseaza-date-din-arhiva'],
            'method' => 'get',
        ]);
        ?>
        <?php
        echo $form->field($model, 'an_scolar')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $baza_date,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza anul ...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza anul dorit');
        ?>
        <?php
        echo $form->field($model1, 'id')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza clasa');

        ?>
        <?= $form->field($model, 'hidden1')->hiddenInput(['value' => $sursa])->label(false); ?>

        <div class="form-group">
            <?= Html::submitButton('Continua', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Renunta', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
