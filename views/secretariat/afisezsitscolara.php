<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogabsenteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
switch ($optiunea) {
    case 1:
        $text = 'Lista mediilor semestriale pe obiecte';
        break;
    case 2:
        $text = 'Lista mediilor semestriale';
        break;
    case 3:
        $text = 'Lista elevilor corigenti';
        break;
}

$this->title = ' ' . $text . ' - ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title;

//retin numele si id-ul clasei selectate
//Yii::$app->session['nume_clasa'] = $nume_clasa;
//Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['secretariat/statistici-situatia-scolara'], ['class' => 'btn btn-primary']) ?>

    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'showFooter' => TRUE,
        'layout' => '{items} {pager} {summary}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nr_matricol',
            [
                'label' => 'Nume elev',
                'content' => function ($data) {
                    $absenta = $data['nume'];
                    return $absenta;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Prenume elev',
                'content' => function ($data) {
                    $absenta = $data['prenume'];
                    return $absenta;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'visible' => ($materiavisible == 1),
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Media semestriala',
                'attribute' => 'media',
                'visible' => ($mediavisible == 1)
            ],
            [
                'label' => 'Media semestriala',
                'attribute' => 'Media_semestriala',
                'visible' => ($mediasemvisible == 1),
            ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

        ],

    ]); ?>
    <br><br>
    <?php
    if (isset($medii_pe_categorii)) {
        echo '<b>Mediile semestriale pe categorii</b><br><br><br>';

        //echo'Numar elevi corigenti : '.$medii_pe_categorii[0][0].' din care fete : '.$medii_pe_categorii[0][1];
        //echo'<br><br>';
        echo 'Numar medii semestriale intre [5-7) : ' . $medii_pe_categorii[1][0] . ' din care fete : ' . $medii_pe_categorii[1][1];
        echo '<br><br>';
        echo 'Numar medii semestriale intre [7-9) : ' . $medii_pe_categorii[2][0] . ' din care fete : ' . $medii_pe_categorii[2][1];
        echo '<br><br>';
        echo 'Numar medii semestriale intre [9-10] : ' . $medii_pe_categorii[3][0] . ' din care fete : ' . $medii_pe_categorii[3][1];
        echo '<br><br>';

    }

    ?>

</div>
