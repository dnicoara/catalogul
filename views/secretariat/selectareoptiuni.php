<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Selectarea  optiunilor  de  afisare  a  mediilor';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>
    <br>

    <div class="classSecretariat-form">

        <?php $form = ActiveForm::begin([
            'action' => ['secretariat/tabel-cu-medii-generale-elevi'],
            'method' => 'get',
        ]);
        ?>
        <?php
        echo $form->field($model, 'id')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza clasa');

        ?>
        <?= $form->field($model, 'Clasa')->widget(Select2::classname(), [
            'data' => ['I' => 'Medii semestriale pe semestrul I', 'II' => 'Medii semestriale pe semestrul II', 'An' => 'Medii generale anuale'],
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza optiunea ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px',
            ],
        ])->label('Selecteaza optiunea'); ?>




        <div class="form-group">
            <?= Html::submitButton('Afiseaza', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
