<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Secretariat */

$this->title = 'Update Secretariat: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Secretariats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="secretariat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
