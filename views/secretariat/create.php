<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Secretariat */

$this->title = 'Create Secretariat';
$this->params['breadcrumbs'][] = ['label' => 'Secretariats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="secretariat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
