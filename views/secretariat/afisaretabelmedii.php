<?php
use kartik\export\ExportMenu;
use yii\db\ActiveRecord;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Infoelevi */
/* @var $dataProvider yii\data\ActiveDataProvider */
$clasa = \app\models\Listaclase::getNumeClasa($id_clasa);
if ($op == 'An')
    $titlu = 'Lista mediilor generale anuale ale clasei :' . ' ' . $clasa . ' - an scolar : ' . $an_scolar;
if ($op == 'I')
    $titlu = 'Lista mediilor semestriale pe semestrul I ale clasei :' . ' ' . $clasa . ' - an scolar : ' . $an_scolar;
if ($op == 'II')
    $titlu = 'Lista mediilor semestriale pe semestrul II ale clasei :' . ' ' . $clasa . ' - an scolar : ' . $an_scolar;

//Setez la NULL  variabila statica $database pentru a nu folosi baza de date din arhiva
//cand vreau sa folosesc alta baza de date setez variabila $database la baza de date dorita
ActiveRecord::$database = Null;

$this->title = 'Mediile generale ale clasei ';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clasa-index">
    <h4><?= Html::encode($titlu) ?></h4>

    <p>
        <?= Html::a('Revenire la selectii', isset($sursa) && $sursa == 'arhiva' ? ['secretariat/afiseaza-date-din-arhiva', 'sursa' => $sursa] : ['secretariat/tabel-cu-medii-generale-elevi'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>

    </p>
    <br><br>
    <?php

    $gridColumns = $materii;

    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items} {pager} {summary}',
        'columns' =>
            $materii,

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]
//
//        ],

    ]); ?>


</div>
