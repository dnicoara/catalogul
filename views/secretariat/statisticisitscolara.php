<?php
use app\models\Profesori;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Situatia scolara semestriala - statistici';
$this->params['breadcrumbs'][] = $this->title;

//obtin valorile parametrilor globali definiti in config/params.php
$data = \Yii::$app->params['data'];
$semestrul = \Yii::$app->params['semestrul'];
?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>

    <div class="classSecretariat-form">


        <?php $form = ActiveForm::begin([
            'action' => ['secretariat/statistici-situatia-scolara'],
            'method' => 'get',
        ]);
        ?>
        <?php
        echo $form->field($model, 'id_clasa')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza clasele ...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza clasele');



        ?>
        <?= $form->field($model, 'sem')->widget(Select2::classname(), [
            'data' => $semestrul,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza semestrul ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px',
            ],
        ])->label('Selecteaza semestrul'); ?>


        <?= $form->field($model, 'media')->radioList(
            ['1' => 'Lista mediilor semestriale pe obiecte',
                '2' => 'Lista mediilor semestriale',
                '3' => 'Lista corigentilor'],
            ['labelOptions' => ['style' => 'display:block']]

        ); ?>

        <div class="form-group">
            <?= Html::submitButton('Trimite', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
