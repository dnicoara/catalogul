<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DirectoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Directori';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directori-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Adauga director', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nume',
            'prenume',
            'email:email',
            //'parola',
            //'data_inregistrarii',
            //'data_actualizarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
