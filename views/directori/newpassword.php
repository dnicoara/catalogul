<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Parola pierduta';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directori-newpassword">
    <h4><?= Html::encode($this->title) ?></h4>

    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>


    <div class="newpassword-form">

        <?php $form = ActiveForm::begin([
            'action' => ['directori/trimite-parola-noua'],
            'method' => 'get',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]);

        ?>
        <?= $form->field($model, 'email')->textInput(['readonly' => false])->label('E-mailul') ?>


        <div class="form-group">
            <?= Html::submitButton('Trimite', ['class' => 'btn btn-primary']) ?>

        </div>
        <div class="form-group">
            <?= Html::a('Revenire in pagina de logare', ['directori/login'], ['class' => 'btn btn-success']) ?>


        </div>

        <?php ActiveForm::end(); ?>


    </div>


</div>
