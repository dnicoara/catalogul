<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directori */

$this->title = 'Editeaza Directori: ';
$this->params['breadcrumbs'][] = ['label' => 'Directori', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza';
?>
<div class="directori-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
