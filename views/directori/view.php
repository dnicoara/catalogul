<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Directori */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Directoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directori-view">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }

    ?>
    <br>
    <p>
        <?= Html::a('Editeaza inregistrarea', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge inregistrarea', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Revenire pagina principala', ['directori/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nume',
            'prenume',
            'email:email',
            //'parola',
            'data_inregistrarii',
            'data_actualizarii',
        ],
    ]) ?>

</div>
