<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Directori */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<br>
<div class="directori-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nume')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'prenume')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['admin/administrator-adauga'], ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
