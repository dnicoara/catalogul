<?php
use app\models\Profesori;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Afisez notele elevului selectat';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
$request = Yii::$app->request;

// In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
// <div class="alert alert-danger" role="alert"></div>
if ($request->isPost) {
    echo '<div class="alert alert-success" role="alert">';
    if (isset($mesaj)) {
        echo Html::encode($mesaj);
    } elseif ($request->post('mesaj') !== null) {
        echo Html::encode($request->post('mesaj'));
    }
    echo '</div>';
}
if ($request->isGet && $request->get('mesaj') !== null) {
    echo '<div class="alert alert-success" role="alert">';
    echo Html::encode($request->get('mesaj'));
    echo '</div>';
}
?>

<div class="site-about">

    <div class="clasaElevi-form">


        <?php $form = ActiveForm::begin([
            'action' => ['directori/afisare-note-elev'],
            'method' => 'get',
        ]);
        ?>

        <?php

        echo $form->field($model, 'nr_matricol')->widget(Select2::classname(), [
            'name' => 'elevii',
            'data' => $lista_elevi,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza elevul ...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza elevul');
        ?>

        <?= $form->field($model1, 'sem')->widget(Select2::classname(), [
            'data' => ['I' => 'Note pe  semestrul I', 'II' => 'Note pe semestrul II'],
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza semestrul ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px',
            ],
        ])->label('Selecteaza semestrul'); ?>

        <div class="form-group">
            <?= Html::submitButton('Afiseaza notele', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Renunta', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
