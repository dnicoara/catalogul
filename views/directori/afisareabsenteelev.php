<?php
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogabsenteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Absentele elevului   ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev;
$abs_motivate = 0;
$abs_nemotivate = 0;
for ($i = 0; $i < count($dataProvider->getModels()); $i++) {
    if ($dataProvider->getModels()[$i]['motivata'] == 'Motivata')
        $abs_motivate++;
    else
        $abs_nemotivate++;
}
?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev . ' - ' . $nume_clasa) ?></h4>
    <h4><?= Html::encode($optiunea == 'An' ? 'Situatie anuala' : 'Situatie semestriala') ?></h4>
    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['directori/afisare-absente-elev'], ['class' => 'btn btn-primary']) ?>
    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            'listamaterii.materia',

            [
                'label' => 'Data absentei',
                'content' => function ($data) {
                    $data_abs = $data['absente'];
                    return $data_abs;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Statut',
                'content' => function ($data) {
                    $statut = $data['motivata'];
                    return $statut;
                },
                'format' => 'text'
            ],
            [
                'label' => $optiunea == 'An' ? 'Situatie anuala' : 'Semestrul',
                'content' => function ($data) {
                    $sem = $data['sem'];
                    return $sem;
                },
                'format' => 'text'
            ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view}',
//            ]

        ],

    ]); ?>
    <br>
    <?php
    echo '<div class="alert alert-success" role="alert">';
    echo Html::encode('Numar absente motivate=' . $abs_motivate);
    echo '<br>';
    echo Html::encode('Numar absente nemotivate=' . $abs_nemotivate);
    echo '<br>';
    echo '<b>' . Html::encode('Total absente =' . ($abs_nemotivate + $abs_motivate)) . '</b>';
    echo '</div>';
    ?>
</div>
