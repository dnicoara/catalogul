<?php
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogabsenteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notele elevului   ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev . ' - ' . $nume_clasa) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['directori/afisare-note-elev'], ['class' => 'btn btn-primary']) ?>


    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            'listamaterii.materia',

            [
                'label' => 'Nota',
                'content' => function ($data) {
                    $nota = $data['nota'];
                    return $nota;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Este nota la teza',
                'content' => function ($data) {
                    $teza = $data['teza'];
                    return $teza;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Data',
                'content' => function ($data) {
                    $data_nota = $data['data'];
                    return $data_nota;
                },
                'format' => 'text'
            ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view}',
//            ]

        ],

    ]); ?>


</div>
