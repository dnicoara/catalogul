<?php

use app\models\Profesori;
use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$nume_profesor = Profesori::getNumeProfesor($id_profesor) . ' ' . Profesori::getPrenumeProfesor($id_profesor);
$this->title = 'Catedra profesorului  : ' . $nume_profesor;
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<div class="clasa-index">
    <br>
    <h4>Catedra profesorului <?= Html::encode($nume_profesor)?></h4>
    <br>
    <p>
        <?= Html::a('Adauga clasa la catedra profesorului', ['profesorclasa/create?id_prof=' . $id_profesor], ['class' => 'btn btn-success']) ?>

        <?= Html::a('Revenire la lista profesorilor', ['profesori/index'], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>

    </p>
    <br><br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'clasa',
        'materia',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items} {pager} {summary}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'clasa',
                'value' => 'clasa'
            ],
            [
                'attribute' => 'materia',
                'value' => 'materia'
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model, $id) use ($id_profesor) {
                        $url = Yii::$app->urlManager->createUrl(['profesorclasa/update', 'id' => $model->id, 'id_prof' => $id_profesor]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Editeaza inregistrarea')]);
                    },
                    'delete' => function ($url, $model, $id) use ($id_profesor) {
                        $url = Url::to(['directori/sterge-clasa-din-catedra', 'id' => $model->id, 'id_prof' => $id_profesor]);
                        //Yii::$app->urlManager->createUrl(['directori/Sterge']);
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            $url,
                            ['title' => Yii::t('app', 'Sterge clasa din catedra'), 'data-confirm' => 'Sunteti sigur ca doriti stergerea inregistrarii ?']

                            );
                    }
                ]
            ]

        ],

    ]); ?>


</div>
