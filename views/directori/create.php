<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Directori */

$this->title = 'Adauga director';
$this->params['breadcrumbs'][] = ['label' => 'Directori', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directori-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
