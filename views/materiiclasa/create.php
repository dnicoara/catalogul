<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Materiiclasa */

$this->title = 'Adauga materie la clasa';
$this->params['breadcrumbs'][] = ['label' => 'Materii clasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="materiiclasa-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'lista_materiilor' => $lista_materiilor,
        'lista_claselor' => $lista_claselor,
        'an_scolar' => $an_scolar,
        'mesaj' => $mesaj
    ]) ?>

</div>
