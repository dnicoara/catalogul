<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Materiiclasa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Materii clasas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="materiiclasa-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Revenire', ['materiiclasa/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_materie',
            'id_clasa',
            'an_scolar',
        ],
    ]) ?>

</div>
