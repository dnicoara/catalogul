<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Materiiclasa */

$this->title = 'Editeaza materiile clasei : ' . $clasa;
$this->params['breadcrumbs'][] = ['label' => 'Materii clasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza';
?>
<div class="materiiclasa-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'lista_materiilor' => $lista_materiilor,
        'lista_claselor' => $lista_claselor,
        'clasa' => $clasa,
    ]) ?>

</div>
