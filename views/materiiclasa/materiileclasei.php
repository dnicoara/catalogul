<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$anul_scolar= \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Adaug materii la clasa selectata';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php 
    $request = Yii::$app->request;
    
    // In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
    // <div class="alert alert-danger" role="alert"></div>
    if ($request->isPost) {
    	echo '<div class="alert alert-success" role="alert">';
        if (isset($mesaj)) {
            echo Html::encode($mesaj);
	} elseif ($request->post('mesaj') !== null) {
            echo Html::encode($request->post('mesaj'));
	}
     	echo '</div>';
     }


?>

<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>

    <div class="materiileClasei-form">


        <?php $form = ActiveForm::begin([
            'action' => ['materiiclasa/edit-materiiclase'],
            'method' => 'post',
        ]);
        ?>
        <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'readonly' => true, 'style' => 'width:300px']) ?>

        <?php

        echo $form->field($model, 'id_clasa')->widget(Select2::classname(), [
            'name'=>'clasa',
            'data' =>$id_clasa,
            'language' => 'ro',
            'size'=>'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'300px'
            ],
        ])->label('Selecteaza clasa');


        echo $form->field($model, 'id')->widget(Select2::classname(), [
            'name'=>'materii',
            'data' =>$lista_materii,
            'language' => 'ro',
            'size'=>'md',
            'options' => ['placeholder' => 'Selecteaza materiile clasei ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
                'width'=>'300px'
            ],
        ])->label('Selecteaza materiile');
        ?>
        <br/>
        <div class="form-group">
            <?= Html::submitButton('Adauga inregistrarea la baza de date', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Iesire', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
