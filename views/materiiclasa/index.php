<?php
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MateriiclasaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista materiilor fiecarei clase';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="materiiclasa-index">

	<p>
        <?php
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        ?>
        <?= Html::a('Adauga o materie la o clasa', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Iesire', [$este_administratorul_conectat ? 'admin/administrator-edit' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        'clasa',
        'materia',
        'id_materie',
        'id_clasa',
        'an_scolar',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [['class' => 'yii\grid\SerialColumn'],

//         ['label' => 'Clasa', 'content' => function ($data) {
//        $clasa = $data['Clasa'];
//        return $clasa;
//    }, 'format' => 'text'],
//        ['label' => 'Materia', 'content' => function ($data) {
//            $clasa = $data['materia'];
//            return $clasa;
//        }, 'format' => 'text'],
            'clasa',
            'materia',
        'id_materie',
        'id_clasa',
        'an_scolar',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}'
        ]]]); ?>

</div>
