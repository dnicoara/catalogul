<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Materiiclasa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materiiclasa-form">
    <?php if (isset($mesaj))
        echo '<span style="color:red "><h5>' . Html::encode($mesaj) . '</h5></span>';
    ?>
    <?php if (Yii::$app->request->get('mesaj'))
        echo '<span style="color:red "><h5>' . Html::encode(Yii::$app->request->get('mesaj')) . '</h5></span>';
    ?>
    <br>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_materie')->widget(Select2::classname(), [
        'data' => $lista_materiilor,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza materia ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza materia'); ?>

    <?= $form->field($model, 'id_clasa')->widget(Select2::classname(), [
        'data' => $lista_claselor,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza clasa ...'],
        'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza clasa'); ?>



    <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $an_scolar, 'style' => 'width:400px']) ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['materiiclasa/index'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
