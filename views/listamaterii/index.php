<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ListamateriiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista materiilor din unitatea scolara';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listamaterii-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        ?>
        <?= Html::a('Adauga materie', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', [$este_administratorul_conectat ? 'admin/administrator-edit' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'materia',
            'modul',
//            'data_inregistrarii',
//            'data_actualizarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
