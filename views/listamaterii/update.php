<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Listamaterii */

$this->title = 'Editeaza materia';
$this->params['breadcrumbs'][] = ['label' => 'Lista materii', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="listamaterii-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
