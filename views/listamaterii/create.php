<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Listamaterii */

$this->title = 'Adauga materie la lista materiilor scolii';
$this->params['breadcrumbs'][] = ['label' => 'Lista materii', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listamaterii-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
