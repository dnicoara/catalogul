<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Listamaterii */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
}
echo '</div>';

?>
<br>
<div class="listamaterii-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'materia')->textInput(['maxlength' => true, 'style' => 'width:400px'])->label('Introduceti materia') ?>

    <?= $form->field($model, 'modul')->widget(Select2::classname(), [
        'data' => ['da' => 'da', 'nu' => 'nu'],
        'language' => 'ro',
        'options' => ['value' => 'nu', 'placeholder' => 'Selecteaza optiunea ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selectati  "da" , daca materia este modul la anumite clase'); ?>
    <br>
    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['admin/administrator-adauga'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
