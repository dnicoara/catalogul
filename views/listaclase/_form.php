<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Listaclase */
/* @var $form yii\widgets\ActiveForm */
$clasele_scolii = \Yii::$app->params['clasele_scolii'];
?>
<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
}
echo '</div>';

?>
<br>
<div class="listaclase-form">
    <br>

    <?php $form = ActiveForm::begin(); ?>

    <?php //= $form->field($model, 'Clasa')->textInput(['maxlength' => true, 'style' => 'width:400px'])->label($model->isNewRecord ? 'Adauga clasa IX-A .... , etc' : 'Clasa ') ?>

    <?= $form->field($model, 'Clasa')->widget(Select2::classname(), [
        'data' => $clasele_scolii,
        'language' => 'ro',
        'options' => ['value' => 'nu', 'placeholder' => 'Selecteaza clasa ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label($model->isNewRecord ? 'Adauga clasa IX-A .... , etc' : 'Clasa '); ?>

    <?= $form->field($model, 'modul')->widget(Select2::classname(), [
        'data' => ['da' => 'da', 'nu' => 'nu'],
        'language' => 'ro',
        'options' => ['value' => 'nu', 'placeholder' => 'Selecteaza optiunea ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selectati  "da" , daca clasa are module  la anumite materii'); ?>
    <br>
    <br>
    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['admin/administrator-adauga'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
