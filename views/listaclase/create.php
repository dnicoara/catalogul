<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Listaclase */

$this->title = 'Adauga clasa la lista claselor scolii';
$this->params['breadcrumbs'][] = ['label' => 'Lista clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listaclase-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
