<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Listaclase */

$this->title = 'Editeaza clasa';
$this->params['breadcrumbs'][] = ['label' => 'Lista clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza';
?>
<div class="listaclase-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
