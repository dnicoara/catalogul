<?php
use app\models\Profesori;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$nume_profesor = Yii::$app->session['user']->nume . ' ' . Yii::$app->session['user']->prenume;
?>

<?php
$this->title = 'Clasele profesorului : ' . $nume_profesor;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>
    <br>
    <div class="claseProfesori-form">


        <?php $form = ActiveForm::begin([
            //'action' => [isset($actiune) && $actiune == 'medii_gen' ? '/diriginti/calculare-medii-generale-clasa-elevi' : 'elevi/afisare-clasa'],
            'action' => ['elevi/afisare-clasa'],
            'method' => 'get',
        ]);
        ?>

        <?php

        // Top most parent
        echo $form->field($model, 'id')->widget(Select2::classname(), [
            'name'=>'Clasa',
            'data' =>$datele,
            'language' => 'ro',
            'size'=>'md',
            //'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'options' => ['placeholder' => 'Selecteaza clasa ...', 'onchange' => 'this.form.submit()'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'300px'
            ],
        ])->label('Selecteaza clasa');

        ?>

        <div class="form-group">
            <?php //= Html::submitButton(isset($actiune) && $actiune == 'medii_gen' ? 'Calculeaza mediile generale ale elevilor clasei' : 'Afisarea elevilor clasei', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
