<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ListaclaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista claselor din unitatea scolara';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listaclase-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        ?>
        <?= Html::a('Adauga clasa la lista claselor', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', [$este_administratorul_conectat ? 'admin/administrator-edit' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'Clasa',
            'modul',
//            'data_inregistrarii',
//            'data_actualizarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
