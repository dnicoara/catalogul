<?php
use app\models\Profesori;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'profesor' :
                                        false;

if ($este_profesorul_conectat)
    $nume_prof = Yii::$app->session['user']->nume . ' ' . Yii::$app->session['user']->prenume;
?>

<?php
$this->title = 'Calcularea mediilor  generale ale elevilor clasei la materia selectata ';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<div class="site-about">
    <h4><?= Html::encode('Materiile  predate de profesorul ' . $nume_prof . ' la clasa :  ' . $nume_clasa) ?></h4>
    <br>
    <div class="materiiProfesori-form">


        <?php $form = ActiveForm::begin([
            'action' => ['/diriginti/calculare-medii-generale-clasa-elevi'],
            'method' => 'get',
        ]);
        ?>

        <?php

        // Top most parent
        echo $form->field($model, 'id')->widget(Select2::classname(), [
            'name' => 'Materia',
            'data' => $datele,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza materia ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza materia');

        ?>
        <br>
        <div class="form-group">
            <?= Html::submitButton('Calculeaza mediile generale ale elevilor clasei', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
