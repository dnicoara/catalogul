<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InfoprofesoriSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="infoprofesori-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fk_profesor') ?>

    <?= $form->field($model, 'sex') ?>

    <?= $form->field($model, 'studii') ?>

    <?= $form->field($model, 'telefon') ?>

    <?php // echo $form->field($model, 'localitate') ?>

    <?php // echo $form->field($model, 'strada') ?>

    <?php // echo $form->field($model, 'numar') ?>

    <?php // echo $form->field($model, 'bloc') ?>

    <?php // echo $form->field($model, 'scara') ?>

    <?php // echo $form->field($model, 'apartament') ?>

    <?php // echo $form->field($model, 'judet') ?>

    <?php // echo $form->field($model, 'buletin_seria') ?>

    <?php // echo $form->field($model, 'buletin_nr') ?>

    <?php // echo $form->field($model, 'grad_didactic') ?>

    <?php // echo $form->field($model, 'vechime') ?>

    <?php // echo $form->field($model, 'data_actualizarii') ?>

    <?php // echo $form->field($model, 'data_inregistrarii') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
