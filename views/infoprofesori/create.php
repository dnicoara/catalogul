<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Infoprofesori */

$this->title = 'Create Infoprofesori';
$this->params['breadcrumbs'][] = ['label' => 'Infoprofesoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infoprofesori-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
