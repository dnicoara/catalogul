<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Infoprofesori */

$nume_profesor =Html::encode($model->getProfesori()->one()->nume);
$prenume_profesor =Html::encode($model->getProfesori()->one()->prenume);

$this->title = 'Editeaza informatii despre profesorul  ' . $nume_profesor . ' ' . $prenume_profesor;
$this->params['breadcrumbs'][] = ['label' => 'Editare Informatii despre profesor', 'url' => ['index']];
?>
<div class="infoprofesori-update">

    <h4><?= Html::encode($this->title) ?></h4>
    <br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
