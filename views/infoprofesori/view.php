<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Infoprofesori */

$nume_profesor = Html::encode($model->getProfesori()->one()->nume);
$prenume_profesor = Html::encode($model->getProfesori()->one()->prenume);

$this->title = 'Profesorul ' . $nume_profesor . ' ' . $prenume_profesor;

$this->params['breadcrumbs'][] = ['label' => 'Infoprofesoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infoprofesori-view">
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>
    <h4>Informatii privitoare la profesorul: <?= $nume_profesor . ' ' . $prenume_profesor ?></h4>
    <br>
    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Doriti sa stergeti aceasta inregistrare ?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        
        $este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'profesor' :
                                        false;

        ?>
        <?= Html::a(
            $este_profesorul_conectat ? 
                'Revenire in cont profesor' : 
                'Revenire la lista profesorilor', 
            [
                $este_profesorul_conectat ? 
                    'infoprofesori/cont-profesor' : 
                    'profesori/index'], 
            ['class' => 'btn btn-success']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sex',
            'studii',
            'telefon',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',
            'buletin_seria',
            'buletin_nr',
            'grad_didactic',
            'vechime',
            'data_actualizarii',
            'data_inregistrarii',
        ],
    ]) ?>

</div>
