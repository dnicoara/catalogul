<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InfoprofesoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Informatii despre profesori';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infoprofesori-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>

    <p>
        <?= Html::a('Adauga  inregistrare', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fk_profesor',
            'sex',
            'studii',
            'telefon',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',
            'buletin_seria',
            'buletin_nr',
            'grad_didactic',
            'vechime',
            //'data_actualizarii',
            //'data_inregistrarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
