<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Despre Catalogul-digital';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        Situl Catalogul-digital pune la dispozitia unitatii de invatamant o platforma digitala pentru a gestiona
        situatia scolara a elevilor
        din institutia respectiva.<br>
        Exista sase categorii de utilizatori :<br>

    <div style="margin-left: 4em;">
        -profesori <br>
        -directori <br>
        -diriginti <br>
        -secretari <br>
        -elevi/parinti <br>
        -administrator <br><br>
    </div>
    Profesorii pot introduce note si absente in catalogul digital si de asemenea pot calcula mediile semestriale si
    generale ale elevilor la obiectul/obiectele predate<br>
    De asemenea pot sa-si modifice anumite date personale din contul de profesor<br><br>
    Directorii au acces la lista profesorilor din scoala pe care o pot edita , la informatii legate de fiecare profesor
    , pot vizualiza si edita catedra fiecarui profesor <br><br>
    Dirigintii pot vizualiza lista elevilor din clasa si pentru fiecare elev au la dispozitie diverse optiuni cum ar fi
    :<br><br>

    <div style="margin-left: 4em;">
        - vizualizare informatii despre elev<br>
        - editare informatii despre elev<br>
        - eliminare/stergere sau adaugare a unui elev din clasa<br>
        - afisarea notelor elevului pe semestrul I sau II<br>
        - calcularea mediilor semestriale ale elevului<br>
        - afisarea mediilor semestriale ale elevului<br>
        - afisarea/motivarea absentelor <br>
        - stabilirea notei la purtare <br>
        - calcularea mediilor generale anuale ale elevului<br><br>
    </div>
    Dirigintele mai poate calcula printr-un click mediile generale anuale ale tuturor elevilor din clasa<br>
    Dirigintele poate afisa diverse situatii statistice legate de situatia scolara a elevilor din clasa , situatia
    absentelor intr-un anume interval de timp selectat,situatia navetistilor ,varstele elevilor din clasa<br>
    Dirigintele poate vizualiza notificari trimise de catre elevi si poate trimite e-mailuri cu diverse mesaje elevilor
    selectati din clasa<br><br>
    Secretarii pot vizualiza diverse situatii statistice la nivel de clasa , grupuri de clase sau institutie :<br><br>

    <div style="margin-left: 4em;">
        - se pot vizualiza elevii din clasele selectate<br>
        - se pot vizualiza elevii navetisti din clasele selectate<br>
        - se pot vizualiza varstele elevilor din clasele selectate<br>
        - se poate afisa situatia scolara semestriala a elevilor din clasele selectate<br>
        - se pot afisa elevii corigenti semestrial/anual din clasele selectate<br>
        - se pot afisa elevii cu nota scazuta la purtare din clasele selectate<br>
        - se poate vedea situatia absentelor elevilor din clasele selectate<br>
        - se pot vizualiza mediile generale ale elevilor din clasele selectate<br>
        Secretarii pot sa-si modifice datele personale din contul de secretar<br><br>
    </div>
    Elevul/parintele poate sa-si consulte situatia scolara : notele si mediile semestriale la obiectele predate la clasa
    , precum si situatia absentelor<br>
    Elevul/parintele poate sa trimita din contul sau un mesaj catre dirigintele clasei <br><br>
    Administratorul este persoana care gestioneaza baza de date a sitului.In aceasta calitate are o multime de atributii
    :<br><br>

    <div style="margin-left: 4em;">
        - Actualizeaza lista materiilor predate la nivel de scoala si lista claselor din unitatea scolara<br>
        - Actualizeaza lista elevilor din unitatea de invatamant<br>
        - Alcatuieste la inceput de an clasele de elevi<br>
        - Alcatuieste catedra fiecarui profesor <br>
        - Are drepturi depline de editare a tuturor datelor din baza de date a sitului<br><br>
    </div>
    Pe parcursul utilizari sitului , toate problemele aparute vor fi tratate si rezolvate in conformitate cu cerintele
    utilizatorilor !

    </p>

</div>
