<?php
use app\models\Elevi;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Catalogul-digital';
?>
<div class="site-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10 col-md-offset-3 search-form-position">

            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    <div class="site-about">
        <?php
        if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
            echo '<div class="alert alert-success" role="alert">';
            echo Yii::$app->session->getFlash('success');
            echo '</div>';
        }
        if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
            echo '<div class="alert alert-danger" role="alert">';
            echo Yii::$app->session->getFlash('error');
            echo '</div>';
        }
        ?>

        <?php $request = Yii::$app->request;
        // In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
        // <div class="alert alert-danger" role="alert"></div>
        if ($request->isPost) {
            echo '<div class="alert alert-success" role="alert">';
            if (isset($mesaj)) {
                echo Html::encode($mesaj);
            } elseif ($request->post('mesaj') !== null) {
                echo Html::encode($request->post('mesaj'));
            }
            echo '</div>';
        }
        if ($request->isGet) {

            if (isset($mesaj)) {
                echo '<div class="alert alert-danger" role="alert">';
                echo Html::encode($mesaj);
                echo '</div>';
            } elseif ($request->get('mesaj') !== null) {
                echo '<div class="alert alert-danger" role="alert">';
                echo Html::encode($request->get('mesaj'));
                echo '</div>';
            }
        }
        ?>
        <br><br>

        <?php

        if (isset($note_insuficiente) && count($note_insuficiente) > 0) {
            echo 'Lista elevilor care nu au suficiente note pentru a li se incheia media semestriala :  <br><br>';

            for ($i = 0; $i < count($note_insuficiente); $i++) {

                echo '<b>' . Elevi::getNumeElev($note_insuficiente[$i]) . '  ' . Elevi::getPrenumeElev($note_insuficiente[$i]) . '  ;  </b>';
                echo "</br>";
            }

        }
        ?>
