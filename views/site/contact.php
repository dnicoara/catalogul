<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacteaza administratorul sitului';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Va multumim ca ne-ati contactat. Va vom raspunde cat mai curand posibil.
        </div>

        <p>
            <!--            Note that if you turn on the Yii debugger, you should be able-->
            <!--            to view the mail message on the mail panel of the debugger.-->
            <?php if (Yii::$app->mailer->useFileTransport): ?>
                Because the application is in development mode, the email is not sent but saved as
                a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                application component to be false to enable email sending.
            <?php endif; ?>
        </p>

    <?php else: ?>
        <h4><?= Html::encode($this->title) ?></h4>

        <p>
            Daca aveti nelamuriri sau orice fel de intrebari , va rugam sa completati formularul de mai jos pentru a ne
            contacta<br>
            Va multumim !

        </p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Numele') ?>

                <?= $form->field($model, 'email')->label('Email-ul') ?>

                <?= $form->field($model, 'subject')->label('Subiectul') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6])->label('Mesajul') ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ])->label('Cod de verificare') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Trimite', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                        <?= Html::a('Renunta', ['site/index'], ['class' => 'btn btn-danger']) ?>

                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
