<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Logare';
$this->params['breadcrumbs'][] = $this->title;
switch ($tip_utilizator) {
    case 'profesor':
        $modelul = 'profesori';
        break;
    case 'diriginte':
        $modelul = 'diriginti';
        break;
    case 'director':
        $modelul = 'directori';
        break;
    case 'elev':
        $modelul = 'elevi';
        break;
    case 'administrator':
        $modelul = 'admin';
        break;
    case 'secretar':
        $modelul = 'secretariat';
        break;

    default :
        $modelul = '';
        break;
}
?>
<div class="site-login">
    <h4><?= Html::encode($this->title) ?></h4>

    <p>Pentru a va loga va rugam sa completati urmatoarele campuri :</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('E-mailul') ?>

    <?= $form->field($model, 'password')->passwordInput()->label('Parola') ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <br>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::a('Ati uitat parola ?', [$modelul . '/trimite-parola-noua'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="col-lg-offset-1" style="color:#999;">
        <!--        You may login with <strong>admin/admin</strong> or <strong>demo/demo</strong>.<br>-->
        <!--        To modify the username/password, please check out the code <code>app\models\User::$users</code>.-->
    </div>
</div>
