<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogabsenteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Catalog absente';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogabsente-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Catalog absente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nr_matricol',
            'id_materie',
            'id_clasa',
            'data',
            //'an_scolar',
            //'absente',
            //'motivata',
            //'data_actualizarii',
            //'sem',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
