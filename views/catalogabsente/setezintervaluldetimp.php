<?php
use app\models\Elevi;
use app\models\Profesori;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php
$nr_matricol = Elevi::getNumarMatricolElev($id_elev);
$nume_elev = Elevi::getNumeElev($nr_matricol) . ' ' . Elevi::getPrenumeElev($nr_matricol);
$this->title = 'Motivare absente elev in bloc';
$this->params['breadcrumbs'][] = $this->title;

//obtin valorile parametrilor globali definiti in config/params.php
$data = \Yii::$app->params['data'];
?>
<div class="site-about">
    <h4><?= Html::encode('Perioada de motivare a absentelor elevului  ' . $nume_elev) ?></h4>
    <br>

    <div class="motivareAbsente-form">


        <?php $form = ActiveForm::begin([
            'action' => ['catalogabsente/motivare-absente-in-bloc'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model, 'data1')->widget(DatePicker::classname(), [
            'options' => ['value' => $data, 'placeholder' => 'Selecteaza data initiala ...', 'style' => 'width:300px;'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ],
        ])->label('Data initiala') ?>

        <?= $form->field($model, 'data2')->widget(DatePicker::classname(), [
            'options' => ['value' => $data, 'placeholder' => 'Selecteaza data finala ...', 'style' => 'width:300px;'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ],
        ])->label('Data finala') ?>
        <?= Html::activeHiddenInput($model, 'sem', ['value' => $sem]) ?>
        <?= Html::activeHiddenInput($model, 'id', ['value' => $id_elev]) ?>

        <br>
        <h4><?= Html::encode('ATENTIE : vor fi motivate toate absentele elevului din intervalul selectat !!!') ?></h4>

        <br>

        <div class="form-group">
            <?= Html::submitButton('Motiveaza', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
