<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Catalogabsente */

$this->title = 'Editeaza absenta elevului :  ' . Html::encode($nume_elev) . ' ' . Html::encode($prenume_elev);
$this->params['breadcrumbs'][] = ['label' => 'Catalog absente', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editare absente';
?>
<div class="catalogabsente-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'nr_matricol' => $nr_matricol,
        'materii_profesor' => $materii_profesor,
    ]) ?>

</div>
