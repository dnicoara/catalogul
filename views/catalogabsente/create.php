<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Catalogabsente */


$this->params['breadcrumbs'][] = ['label' => 'Catalog-absente', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="catalogabsente-create">

    <h4><?= Html::encode('Introduceti absenta elevului : ' . $nume_elev . ' ' . $prenume_elev) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'nr_matricol'=>$nr_matricol,
        'nume_elev'=>$nume_elev,
        'prenume_elev'=>$prenume_elev,
        'materii_profesor'=>$materii_profesor,
    ]) ?>

</div>
