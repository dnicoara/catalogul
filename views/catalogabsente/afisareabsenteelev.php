<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogabsenteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Absentele elevului : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev;

//retin numele si id-ul clasei selectate
//Yii::$app->session['nume_clasa'] = $nume_clasa;
//Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev . ' ' . 'pe semestrul' . ' ' . $semestrul) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Motivare absente in bloc ', ['catalogabsente/motivare-absente-in-bloc', 'id_elev' => $id_elev, 'sem' => $semestrul], ['class' => 'btn btn-info']) ?>


    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol=$data['nr_matricol'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia=$data['materia'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Absenta',
                'content' => function ($data) {
                    $absenta=$data['absente'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $absenta;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Motivata',
                'content' => function ($data) {
                    $motivata=$data['motivata'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $motivata;
                },
                'format' => 'text'
            ],

            //'email',


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} ',
            ]

        ],

    ]); ?>


</div>
