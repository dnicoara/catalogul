<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Absentele elevului : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev;

//retin numele si id-ul clasei selectate
//Yii::$app->session['nume_clasa'] = $nume_clasa;
//Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Adaugare absenta', ['catalogabsente/create', 'id_elev' => $id_elev], ['class' => 'btn btn-info']) ?>

    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Absenta',
                'content' => function ($data) {
                    $absenta = $data['absente'];
                    return $absenta;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Motivata/Nemotivata',
                'content' => function ($data) {
                    $mot = $data['motivata'];
                    return $mot;
                },
                'format' => 'text'
            ],
            'sem',


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} ',
            ]

        ],

    ]); ?>


</div>
