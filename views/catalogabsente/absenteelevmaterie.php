<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Absentele elevului : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev . '  pe semestrul' . ' ' . $semestrul;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev . '  pe semestrul' . ' ' . $semestrul) ?></h4>

    <br>
    <p>
        <?php 
        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'diriginte' :
                                        false;
        ?>

        <?= Html::a($este_dirigintele_conectat ? 'Revenire la clasa dirigintelui' : 'Revenire la clasele profesorului', $este_dirigintele_conectat ? ['elevi/afisare-clasa-diriginte'] : ['listaclase/clase-profesor'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', $este_dirigintele_conectat ? ['elevi/afisare-clasa-diriginte'] : ['elevi/afisare-clasa', 'id_clasa' => $id_clasa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($este_dirigintele_conectat ? 'Revenire la pagina principala' : 'Adauga absenta', $este_dirigintele_conectat ? ['aite/index'] : ['catalogabsente/create', 'id_elev' => $id_elev], ['class' => 'btn btn-info']) ?>

    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            'listamaterii.materia',

            [
                'label' => 'Data',
                'content' => function ($data) {
                    $data = $data['absente'];
                    return $data;
                },
                'format' => 'text'
            ],
            'motivata',
            'sem',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} ',
            ]

        ],

    ]); ?>


</div>
