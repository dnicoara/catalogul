<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Catalogabsente */
/* @var $form yii\widgets\ActiveForm */
//obtin valorile parametrilor globali definiti in config/params.php
$data = \Yii::$app->params['data'];
$anul_scolar= \Yii::$app->params['anul_scolar'];
$semestrul=\Yii::$app->params['semestrul'];
$statut_absenta = \Yii::$app->params['statut_absenta'];
$luna = \Yii::$app->params['luna'];

$id_clasa=Yii::$app->session['id_clasa'];

?>

<div class="catalogabsente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nr_matricol')->textInput(['maxlength' => true, 'value' => $nr_matricol, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'id_materie')->widget(Select2::classname(), [
        'data' => $materii_profesor,
        'language' => 'ro',
        //'options' => ['placeholder' => 'Selecteaza materia ...'],
        'options' => count($materii_profesor) === 1 ? ['value' => $materii_profesor[key($materii_profesor)]] : ['placeholder' => 'Selecteaza materia ...'],
        'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza materia'); ?>


    <?= $form->field($model, 'id_clasa')->textInput(['value' => $id_clasa, 'readonly' => true, 'style' => 'width:400px'])->label('Clasa ' . Yii::$app->session['nume_clasa']); ?>


    <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'absente')->widget(DatePicker::classname(), [
        'options' => ['readonly' => $model->isNewRecord ? false : false, 'value' => $model->isNewRecord ? $data : $model->absente, 'style' => 'width:322px'],
        //'disabled' => $model->isNewRecord ? false : false,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ],
    ])->label('Data absentei') ?>


    <?= $form->field($model, 'motivata')->widget(Select2::classname(), [
        //'data' =>['nemotivata'=>'nemotivata','motivata'=>'motivata'],
        'data' => $statut_absenta,
        'options' => ['placeholder' => 'Selecteaza optiunea pentru a motiva absenta ...', 'value' => $model->isNewRecord ? 'Nemotivata' : $model->motivata],
        'language' => 'ro',
        'disabled' => $model->isNewRecord ? true : false,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label($model->isNewRecord ? "" : 'Motiveaza absenta selectand optiunea '); ?>


    <?= $form->field($model, 'sem')->widget(Select2::classname(), [
        'data' =>$semestrul,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza semestrul ...', 'value' => $model->isNewRecord ? ($luna >= 2 && $luna <= 6) ? 'II' : 'I' : $model->sem],
        'disabled' => $model->isNewRecord ? false : false,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza semestrul'); ?>


    <?php
    
    $este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'profesor' :
                                        false;

    ?>


    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', [$este_profesorul_conectat ? 'elevi/afisare-clasa' : 'elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
