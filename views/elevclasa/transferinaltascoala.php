<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Transferare elev in alta unitate scolara : ';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<h4><?= Html::encode('Selectati elevul care va fi transferat') ?></h4>
<br>

<div class="site-about">

    <div class="transferExtern-form">

        <?php $form = ActiveForm::begin([
            'action' => ['elevclasa/transfer-elev-in-alta-scoala'],
            'method' => 'get',
        ]);
        ?>

        <?php
        echo $form->field($model1, 'nr_matricol')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $lista_elevi_pentru_transfer,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selectati elevul ce va fi transferat in alta scoala...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '350px'
            ],
        ])->label('Selectati elevul ce va fi transferat in alta scoala');

        ?>
        <?= $form->field($model2, 'transferat_de_la')->textInput(['maxlength' => true, 'style' => 'width:350px'])->label('Unitatea scolara de unde se va transfera ...'); ?>

        <br>

        <div class="form-group">
            <?= Html::submitButton('Continua...', ['class' => 'btn btn-success']) ?><br><br>
            <?= Html::a('Revenire la pagina principala', ['admin/administrator-transfer-elevi'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
