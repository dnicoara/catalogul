<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$sexul = \Yii::$app->params['sexul'];
?>

<?php
$this->title = 'Transferare elev provenit din alta unitate scolara : ';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<h4><?= Html::encode('Introduceti datele elevului care va fi transferat') ?></h4>
<br>

<div class="site-about">

    <div class="transferExtern-form">

        <?php $form = ActiveForm::begin([
            'action' => ['elevclasa/transfer-elev-din-alta-scoala'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model1, 'nr_matricol')->textInput(['maxlength' => true, 'style' => 'width:350px']) ?>

        <?= $form->field($model1, 'nume')->textInput(['maxlength' => true, 'style' => 'width:350px']) ?>

        <?= $form->field($model1, 'prenume')->textInput(['maxlength' => true, 'style' => 'width:350px']) ?>

        <?= $form->field($model1, 'email')->textInput(['maxlength' => true, 'style' => 'width:350px']) ?>

        <?= $form->field($model1, 'sex')->widget(Select2::classname(), [
            'data' => $sexul,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza sexul elevului ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '350px'
            ],
        ])->label('Selecteaza sexul elevului'); ?>

        <?= $form->field($model2, 'transferat_de_la')->textInput(['maxlength' => true, 'style' => 'width:350px'])->label('Unitatea scolara de unde se transfera ...'); ?>

        <?php
        echo $form->field($model3, 'id')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selectati clasa in care va fi transferat elevul...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '350px'
            ],
        ])->label('Selectati clasa in care va fi transferat elevul');

        ?>

        <div class="form-group">
            <?= Html::submitButton('Continua...', ['class' => 'btn btn-success']) ?><br><br>
            <?= Html::a('Revenire la pagina principala', ['admin/administrator-transfer-elevi'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
