<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Elevclasa */

$this->title = 'Puteti transfera  elevul :  ' . $nume_elev . '  ' . $prenume_elev . ' aflat in clasa : ' . $clasa_elev;
$this->params['breadcrumbs'][] = ['label' => 'Elev clasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza';

?>
<div class="elevclasa-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'nume_elev' => $nume_elev,
        'prenume_elev' => $prenume_elev,
        'clasa_elev' => $clasa_elev,
        'lista_claselor' => $lista_claselor,
        'lista_elevilor' => $lista_elevilor
    ]) ?>

</div>
