<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ElevclasaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="elevclasa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nr_matricol') ?>

    <?= $form->field($model, 'id_clasa') ?>

    <?= $form->field($model, 'an_scolar') ?>

    <?= $form->field($model, 'transferat') ?>

    <?= $form->field($model, 'transferat_de_la') ?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
