<?php
use app\models\Profesori;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$anul_scolar= \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Formare clasa elevi';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php 
    $request = Yii::$app->request;
    
    // In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
    // <div class="alert alert-danger" role="alert"></div>
    if ($request->isPost) {
    	echo '<div class="alert alert-success" role="alert">';
        if (isset($mesaj)) {
            echo Html::encode($mesaj);
	} elseif ($request->post('mesaj') !== null) {
            echo Html::encode($request->post('mesaj'));
	}
     	echo '</div>';
     }
if ($request->isGet && $request->get('mesaj') !== null) {
    echo '<div class="alert alert-success" role="alert">';
    echo Html::encode($request->get('mesaj'));
    echo '</div>';
}
?>

<div class="site-about">

    <div class="clasaElevi-form">


        <?php $form = ActiveForm::begin([
            'action' => ['elevclasa/edit-clase'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'readonly' => true, 'style' => 'width:300px']) ?>

        <?php

        echo $form->field($model, 'id')->widget(Select2::classname(), [
            'name'=>'elevii',
            'data' =>$lista_elevi,
            'language' => 'ro',
            'size'=>'md',
            'options' => ['placeholder' => 'Selecteaza elevii ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
                'width'=>'300px'
            ],
        ])->label('Selecteaza elevii');

        echo $form->field($model, 'id_clasa')->widget(Select2::classname(), [
            'name'=>'clasa',
            'data' =>$id_clasa,
            'language' => 'ro',
            'size'=>'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'300px'
            ],
        ])->label('Selecteaza clasa');


        ?>
        <br/>
        <div class="form-group">
            <?= Html::submitButton('Adauga inregistrarea la baza de date', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Iesire', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
