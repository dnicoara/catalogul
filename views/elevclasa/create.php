<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Elevclasa */

$this->title = 'Adauga elev in clasa';
$this->params['breadcrumbs'][] = ['label' => 'Elev clasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elevclasa-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'lista_elevilor' => $lista_elevilor,
        'lista_claselor' => $lista_claselor,
        'mesaj' => $mesaj

    ]) ?>

</div>
