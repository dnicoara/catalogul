<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Sterge elevii clasei selectate';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<div class="site-about">

    <div class="StergeEleviiClasei-form">

        <?php $form = ActiveForm::begin([
            'action' => ['elevclasa/sterge-elevii-clasei'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model1, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'readonly' => true, 'style' => 'width:300px'])->label('Anul scolar curent') ?>

        <?php

        echo $form->field($model1, 'id_clasa')->widget(Select2::classname(), [
            'name' => 'clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza clasa din care vor fi stersi elevii');

        ?>

        <div class="form-group">
            <?= Html::submitButton('Sterge', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Iesire', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
