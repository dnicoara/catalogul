<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Elevclasa */
/* @var $form yii\widgets\ActiveForm */
$anul_scolar = \Yii::$app->params['anul_scolar'];
?>

<div class="elevclasa-form">
    <?php if (isset($mesaj))
        echo '<span style="color:red "><h5>' . Html::encode($mesaj) . '</h5></span>';
    ?>
    <?php if (Yii::$app->request->get('mesaj'))
        echo '<span style="color:red "><h5>' . Html::encode(Yii::$app->request->get('mesaj')) . '</h5></span>';
    ?>
    <br>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_clasa')->widget(Select2::classname(), [
        'data' => $lista_claselor,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza noua clasa ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selectati noua clasa a elevului'); ?>

    <?= $form->field($model, 'nr_matricol')->widget(Select2::classname(), [
        'data' => $lista_elevilor,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza elevul ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Elevul ce va fi transferat'); ?>


    <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'transferat')->widget(Select2::classname(), [
        'data' => ['da' => 'da', 'nu' => 'nu'],
        'language' => 'ro',
        'options' => ['value' => 'da', 'placeholder' => 'Selecteaza optiunea ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Daca elevul s-a transferat din clasa , selectati da'); ?>

    <?= $form->field($model, 'transferat_de_la')->textInput(['maxlength' => true, 'value' => $clasa_elev, 'style' => 'width:400px'])
        ->label('Clasa sau scoala de unde se transfera elevul (daca este cazul)'); ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['elevclasa/index'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
