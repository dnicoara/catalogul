<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ElevclasaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Clasa/clasele de  elevi';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="elevclasa-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>

    <p>
        <?php
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        ?>
        <?= Html::a('Introduceti elev in clasa', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Iesire', [$este_administratorul_conectat ? 'admin/administrator-edit' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'clasa',
                'value' => 'Clasa'
            ],
            'id_clasa',
            [
                'attribute' => 'nume',
                'value' => 'nume'
            ],


//            [
//                'label' => 'Clasa',
//                'content' => function ($data) {
//                    $clasa=$data['Clasa'];
//                    //$clasa = $data->getListaclase()->one()->Listaclase;
//                    return $clasa;
//                },
//                'format' => 'text'
//            ],

//            [
//                'label' => 'Nume elev',
//                'content' => function ($data) {
//                    $nume=$data['nume'];
//                    //$clasa = $data->getListaclase()->one()->Listaclase;
//                    return $nume;
//                },
//                'format' => 'text'
//            ],


            [
                'label' => 'Prenume elev',
                'content' => function ($data) {
                    $prenume=$data['prenume'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $prenume;
                },
                'format' => 'text'
            ],
            'nr_matricol',
            'an_scolar',
            'transferat',
            'transferat_de_la',
            'data_inregistrarii',
            'data_actualizarii',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>
</div>
