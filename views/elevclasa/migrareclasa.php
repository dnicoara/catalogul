<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Formare clasa elevi';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
$request = Yii::$app->request;

// In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
// <div class="alert alert-danger" role="alert"></div>
if ($request->isPost) {
    echo '<div class="alert alert-success" role="alert">';
    if (isset($mesaj)) {
        echo Html::encode($mesaj);
    } elseif ($request->post('mesaj') !== null) {
        echo Html::encode($request->post('mesaj'));
    }
    echo '</div>';
}
if ($request->isGet && $request->get('mesaj') !== null) {
    echo '<div class="alert alert-success" role="alert">';
    echo Html::encode($request->get('mesaj'));
    echo '</div>';
}
?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<div class="site-about">

    <div class="MigrareClasaElevi-form">


        <?php $form = ActiveForm::begin([
            'action' => ['elevclasa/migrare-clasa'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model1, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'readonly' => false, 'style' => 'width:300px']) ?>

        <?php

        echo $form->field($model1, 'id_clasa')->widget(Select2::classname(), [
            'name' => 'clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza clasa care va migra');


        echo $form->field($model2, 'id')->widget(Select2::classname(), [
            'name' => 'clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza clasa in care va migra');


        ?>

        <div class="form-group">
            <?= Html::submitButton('Migreaza', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Iesire', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
