<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Elevclasa */

$this->title = 'Elevul' . ' ' . $nume_elev . ' ' . $prenume_elev . '  a fost introdus in clasa ';
$this->params['breadcrumbs'][] = ['label' => 'Elev clasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elevclasa-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Iesire', ['elevclasa/index'], ['class' => 'btn btn-primary']) ?>


    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nr_matricol',
            'id_clasa',
            'an_scolar',
            'transferat',
            'transferat_de_la',
            'data_inregistrarii',
            'data_actualizarii'
        ],
    ]) ?>

</div>
