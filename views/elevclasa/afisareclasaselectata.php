<?php
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$clasa = \app\models\Listaclase::getNumeClasa($clasa_plecare);

$this->title = 'Lista elevilor din clasa selectata : ' . $clasa;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="elevi-afisareclasaselectata">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Revenire la selectii', ['elevclasa/selectare-clasa-elevi'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Revenire la pagina principala', ['admin/administrator-transfer-elevi'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nr_matricol',
        'nume',
        'prenume',
        'listaclase.Clasa',
//        'elevi.email',
//        'infoelevi.telefon',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nr_matricol',
            'nume',
            'prenume',
            'listaclase.Clasa',
//            'elevi.email',
//            'infoelevi.telefon',
//            'infoelevi.buletin_seria',


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{transfer}',
                'buttons' => [
                    'transfer' => function ($url, $model, $id) use ($clasa_plecare, $clasa_sosire) {
                        $url = Yii::$app->urlManager->createUrl(['elevclasa/transfer-elevul', 'id_elev' => $id, 'id_clasa_plecare' => $clasa_plecare, 'id_clasa_sosire' => $clasa_sosire]);
                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['title' => Yii::t('app', 'Transfera elevul din clasa')]);
                    },

                ]
            ],
        ],
    ]); ?>


</div>
