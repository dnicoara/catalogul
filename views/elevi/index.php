<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EleviSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista elevilor scolii';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elevi-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $request = Yii::$app->request;

    // In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
    // <div class="alert alert-danger" role="alert"></div>
    if ($request->isPost) {
        echo '<div class="alert alert-success" role="alert">';
        if (isset($mesaj)) {
            echo Html::encode($mesaj);
        } elseif ($request->post('mesaj') !== null) {
            echo Html::encode($request->post('mesaj'));
        }
        echo '</div>';
    }
    if ($request->isGet) {

        if (isset($mesaj)) {
            echo '<div class="alert alert-success" role="alert">';
            echo Html::encode($mesaj);
            echo '</div>';
        } elseif ($request->get('mesaj') !== null) {
            echo '<div class="alert alert-success" role="alert">';
            echo Html::encode($request->get('mesaj'));
            echo '</div>';
        }
    }
    ?>

    <br>

    <p>
        <?php
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        ?>
        <?= Html::a('Adauga elev', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la pagina principala', [$este_administratorul_conectat ? 'admin/administrator-edit' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nr_matricol',
            'sex',
            'nume',
            'prenume',
            'email:email',
            //'parola',
            //'data_inregistrarii',
            //'data_actualizarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
