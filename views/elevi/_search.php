<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EleviSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="elevi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nr_matricol') ?>

    <?= $form->field($model, 'nume') ?>

    <?= $form->field($model, 'prenume') ?>

    <?= $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'parola') ?>

    <?php // echo $form->field($model, 'data_inregistrarii') ?>

    <?php // echo $form->field($model, 'data_actualizarii') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
