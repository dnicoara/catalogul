<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Elevi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Elevi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="elevi-view">


    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge elev', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Revenire la lista elevilor', ['elevi/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nr_matricol',
            'nume',
            'prenume',
            'email:email',
            'sex',
            //'parola',
            'data_inregistrarii',
            'data_actualizarii',
        ],
    ]) ?>

</div>
