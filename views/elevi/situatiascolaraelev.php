<?php
use app\models\Profesori;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Situatia scolara a elevului- statistici';
$this->params['breadcrumbs'][] = $this->title;

//obtin valorile parametrilor globali definiti in config/params.php
$data = \Yii::$app->params['data'];
$semestrul = \Yii::$app->params['semestrul'];
?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>
    <br>
    <div class="claseElevi-form">


        <?php $form = ActiveForm::begin([
            'action' => ['elevi/situatia-scolara-elev'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model1, 'sem')->widget(Select2::classname(), [
            'data' => $semestrul,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza semestrul ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px',
            ],
        ])->label('Selecteaza semestrul'); ?>

        <br>
        <?= $form->field($model2, 'radiolist')->radioList(
            ['1' => 'Lista notelor',
                '2' => 'Lista mediilor semestriale',
                '3' => 'Lista absentelor'],
            ['labelOptions' => ['style' => 'display:block']]

        )->label('Selectati optiunea'); ?>

        <br>
        <div class="form-group">
            <?= Html::submitButton('Afiseaza', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Renunta', ['site/index'], ['class' => 'btn btn-danger']) ?>

        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
