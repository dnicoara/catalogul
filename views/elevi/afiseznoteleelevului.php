<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogabsenteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
switch ($optiunea) {
    case 1:
        $text = 'Lista notelor elevului  ' . $nume_elev . ' ' . $prenume_elev;
        break;
    case 2:
        $text = 'Lista mediilor semestriale ale elevului  ' . $nume_elev . ' ' . $prenume_elev;
        break;

}

$this->title = ' ' . $text . ' - ' . $nume_clasa . ' - semestrul ' . $semestrul;
$this->params['breadcrumbs'][] = $this->title;

//retin numele si id-ul clasei selectate
//Yii::$app->session['nume_clasa'] = $nume_clasa;
//Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['elevi/situatia-scolara-elev'], ['class' => 'btn btn-primary']) ?>

    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'showFooter' => TRUE,
        'layout' => '{items} {pager} {summary}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nr_matricol',
            [
                'label' => 'Nume elev',
                'content' => function ($data) {
                    $absenta = $data['nume'];
                    return $absenta;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Prenume elev',
                'content' => function ($data) {
                    $absenta = $data['prenume'];
                    return $absenta;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Nota',
                'attribute' => 'nota',
                'visible' => ($notavisible == 1)
            ],
            [
                'label' => 'Media semestriala',
                'attribute' => 'media',
                'visible' => ($mediasemvisible == 1),
            ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

        ],

    ]); ?>


</div>
