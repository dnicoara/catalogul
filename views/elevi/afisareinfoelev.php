<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Informatii despre elevul : ' . $nume_elev . ' ' . $prenume_elev;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="elevi-afisareinfoelev">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Nume elev',
                'content' => function ($data) {
                    $nume = $data['nume'];
                    return $nume;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Prenume elev',
                'content' => function ($data) {
                    $prenume = $data['prenume'];
                    return $prenume;
                },
                'format' => 'text'
            ],
            'nr_matricol',
            'scutit_la',
            'email',
            'telefon',
            'buletin_seria',
            'buletin_nr',
            'cod_num',
            'religie',
            'naveta',
            'tata',
            'mama',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $id) {
	                    $url = Yii::$app->urlManager->createUrl(['infoelevi/update', 'id' => $id]); // your own url generation logic                
	                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Editare date elev')]);                
                    },
                ],
            ],
        ],
    ]); ?>

    <p>
        <?= Html::a('Revenire la lista elevilor clasei', ['elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-primary']) ?>

    </p>
</div>
