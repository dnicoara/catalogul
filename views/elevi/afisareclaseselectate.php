<?php
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$clase = '';
if (isset($clase_selectate)) {
    for ($i = 0; $i < count($clase_selectate); $i++)
        $clase = $clase . \app\models\Listaclase::getNumeClasa($clase_selectate[$i]) . ' ; ';

}
$this->title = 'Lista elevilor din clasele selectate : ' . $clase;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="elevi-afisareclaseselectate">

    <h4><?= Html::encode($this->title) ?></h4>
    <br>

    <p>
        <?= Html::a('Revenire la selectii', ['elevi/selectare-clase', 'op' => 'infoelevi'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>


    </p>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nr_matricol',
        'nume',
        'prenume',
        'listaclase.Clasa',
        'elevi.email',
        'infoelevi.telefon',
        'infoelevi.buletin_seria',
        'infoelevi.buletin_nr',
        'infoelevi.cod_num',
        'infoelevi.religie',
        'infoelevi.naveta',
        'infoelevi.tata',
        'infoelevi.mama',
        'infoelevi.localitate',
        'infoelevi.strada',
        'infoelevi.numar',
        'infoelevi.bloc',
        'infoelevi.scara',
        'infoelevi.apartament',
        'infoelevi.judet',
        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nr_matricol',
            'nume',
            'prenume',
//            [
//                'label' => 'Nume elev',
//                'content' => function ($data) {
//                    $nume = $data['nume'];
//                    return $nume;
//                },
//                'format' => 'text'
//            ],
//            [
//                'label' => 'Prenume elev',
//                'content' => function ($data) {
//                    $prenume = $data['prenume'];
//                    return $prenume;
//                },
//                'format' => 'text'
//            ],
//
            'listaclase.Clasa',
            'elevi.email',
            'infoelevi.telefon',
            'infoelevi.buletin_seria',
            'infoelevi.buletin_nr',
            'infoelevi.cod_num',
            'infoelevi.religie',
            'infoelevi.naveta',
            'infoelevi.tata',
            'infoelevi.mama',
            'infoelevi.localitate',
            'infoelevi.strada',
            'infoelevi.numar',
            'infoelevi.bloc',
            'infoelevi.scara',
            'infoelevi.apartament',
            'infoelevi.judet',

            ['class' => 'yii\grid\ActionColumn',
                'template' => ' ',
            ],
        ],
    ]); ?>


</div>
