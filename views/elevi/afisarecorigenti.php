<?php

use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Infoelevi */
/* @var $dataProvider yii\data\ActiveDataProvider */

$clase = '';
if (isset($clase_selectate)) {
    for ($i = 0; $i < count($clase_selectate); $i++)
        $clase = $clase . \app\models\Listaclase::getNumeClasa($clase_selectate[$i]) . ' ; ';

}
switch ($tipSituatie) {
    case 'I':
        $sit = 'Sitatia pe semestrul I';
        break;
    case 'II':
        $sit = 'Sitatia pe semestrul II';
        break;
    case 'Anual':
        $sit = 'Sitatia anuala';
        break;
}
$this->title = $optiunea == 'corigenti' ? 'Lista elevilor corigenti din clasele  : ' . $clase : 'Lista elevilor cu nota scazuta la purtare din clasele  : ' . $clase;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clasa-corigenti">
    <h4><?= Html::encode($this->title) ?><br>
        <h4><?= Html::encode($sit) ?></h4>
        <br>
        <p>
            <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>

            <?= Html::a(
                'Revenire la selectii',
                $optiunea == 'corigenti' ?
                    ['elevi/selectare-clase-semestrul', 'op' => 'corigenti']
                    :
                    ['elevi/selectare-clase-semestrul', 'op' => 'purtare'],
                ['class' => 'btn btn-primary'])
            ?>
        </p>
        <br><br>
        <?php
        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],
            'elevi.nume',
            'elevi.prenume',
            'listamaterii.materia',

            ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
                return '#';
            }],
        ];
        ?>
        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Exporta',
                'class' => 'btn btn-default',
            ],
        ]);
        // Renders a export dropdown menu
        ?>
        <br><br>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items} {pager} {summary}',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'elevi.nume',
                    'value' => 'elevi.nume'
                ],
                [
                    'attribute' => 'elevi.prenume',
                    'value' => 'elevi.prenume'
                ],

                [
                    'attribute' => 'listamaterii.materia',
                    'value' => 'listamaterii.materia'
                ],
                [
                    'attribute' => 'media',
                    'value' => 'media'
                ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

            ],

        ]); ?>


</div>
