<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Introduceti noua adresa de e-mail';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<div class="site-about">

    <div class="changeMail-form">

        <?php $form = ActiveForm::begin([
            'action' => ['elevi/schimba-adresa-mail'],
            'method' => 'get',
        ]);

        ?>
        <?= $form->field($model, 'email')->textInput(['readonly' => true])->label('E-mailul curent') ?>
        <?= $form->field($model, 'noul_email')->input('email')->label('Introduceti noul e-mail') ?>


        <?= Html::activeHiddenInput($model, 'id_elev', ['value' => $model->id]) ?>

        <div class="form-group">
            <?= Html::submitButton('Schimba adresa e-mail', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        <?= Html::a('Revenire in cont elev', ['infoelevi/cont-elev'], ['class' => 'btn btn-success']) ?>

    </div>

</div>
