<?php

use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Infoelevi */
/* @var $dataProvider yii\data\ActiveDataProvider */
$clase = '';
if (isset($clase_selectate)) {
    for ($i = 0; $i < count($clase_selectate); $i++)
        $clase = $clase . \app\models\Listaclase::getNumeClasa($clase_selectate[$i]) . ' ; ';
}
$this->title = 'Absente elevi - statistici - clasele : ' . $clase;
$this->params['breadcrumbs'][] = 'Absente elevi - statistici';
?>
<div class="clasa-absente">
    <h4><?= Html::encode($this->title) ?><br>

        <br>

        <p>
            <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Revenire la selectii', ['elevi/selectare-optiuni-absente', 'op' => 'absente'], ['class' => 'btn btn-primary']) ?>


        </p>
        <br><br>
        <?php
        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],
            'motivate',
            'nemotivate',
            'total',
            ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
                return '#';
            }],
        ];
        ?>
        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Exporta',
                'class' => 'btn btn-default',
            ],
        ]);
        // Renders a export dropdown menu
        ?>
        <br><br>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items} {pager} {summary}',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'motivate',
                    'value' => 'motivate'
                ],
                [
                    'attribute' => 'nemotivate',
                    'value' => 'nemotivate'
                ],

                [
                    'attribute' => 'total',
                    'value' => 'total'
                ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

            ],

        ]); ?>


</div>
