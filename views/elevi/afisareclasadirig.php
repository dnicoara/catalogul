<?php

use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\view;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CititoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$nume_diriginte = Yii::$app->session['user']->nume . ' ' . Yii::$app->session['user']->prenume;

$this->title = 'Elevii clasei : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_clasa . '  (diriginte: ' . $nume_diriginte . ')';

//retin numele si id-ul clasei selectate
Yii::$app->session['nume_clasa'] = $nume_clasa;
//Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">
    <?php
    if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <h4><?= Html::encode($this->title . ' ' . $nume_clasa . '  (diriginte: ' . $nume_diriginte . ')') ?></h4>

    <br>
    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php if (isset($mesaj))
        echo '<span style="color:red "><h5>' . Html::encode($mesaj) . '</h5></span>';
    ?>
    <?php if (Yii::$app->request->get('mesaj'))
        echo '<span style="color:red "><h5>' . Html::encode(Yii::$app->request->get('mesaj')) . '</h5></span>';
    ?>
    <br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nr_matricol',
        'nume',
        'prenume',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nr_matricol',
            'nume',
            'prenume',
//            [
//                'label' => 'Numar matricol',
//                'content' => function ($data) {
//                    $nr_matricol=$data['nr_matricol'];
//                    //$clasa = $data->getListaclase()->one()->Listaclase;
//                    return $nr_matricol;
//                },
//                'format' => 'text'
//            ],
//
//            [
//                'label' => 'Nume elev',
//                'content' => function ($data) {
//                    $nume=$data['nume'];
//                    //$clasa = $data->getListaclase()->one()->Listaclase;
//                    return $nume;
//                },
//                'format' => 'text'
//            ],
//            [
//                'label' => 'Prenume elev',
//                'content' => function ($data) {
//                    $prenume=$data['prenume'];
//                    //$clasa = $data->getListaclase()->one()->Listaclase;
//                    return $prenume;
//                },
//                'format' => 'text'
//            ],
//            [
//                'label' => 'Clasa',
//                'content' => function ($data) {
//                    $clasa=$data['Clasa'];
//                    //$clasa = $data->getListaclase()->one()->Listaclase;
//                    return $clasa;
//                },
//                'format' => 'text'
//            ],
            //'email',


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}  {delete}      {note1}   {note2}     {absente1}    {absente2}    {calcularemediisemestriale1}     {calcularemediisemestriale2}
                               {afisaremediisemestriale1}   {afisaremediisemestriale2}                  {purtare1}         {medii_gen}',
                'buttons' => [
                    'view' => function ($url, $model, $id) {

                        $url = Yii::$app->urlManager->createUrl(['elevi/afisare-informatii-elev', 'id' => $id]); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Afisare elev ')]);

                    },
                    'delete' => function ($url, $model, $id) {

                        $url = Yii::$app->urlManager->createUrl(['elevclasa/sterge-elev-din-clasa', 'id' => $id]); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('app', 'Sterge elev din clasa'), 'class' => 'delete']);

                    },

                    'note1' => function ($url, $model, $id) {

                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-note-elev-pe-semestru', 'id_elev' => $id, 'sem' => 'I']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, ['title' => Yii::t('app', 'Afisare note semestrul I')]);

                    },
                    'note2' => function ($url, $model, $id) {

                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-note-elev-pe-semestru', 'id_elev' => $id, 'sem' => 'II']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, ['title' => Yii::t('app', 'Afisare note semestrul II')]);

                    },
                    'absente1' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalogabsente/absente-elev', 'id_elev' => $id, 'sem' => 'I']);
                        return Html::a('<span class="glyphicon glyphicon-dashboard"></span>', $url, ['title' => Yii::t('app', 'Afisare/motivare absente elev , semestrul I')]);
                    },
                    'absente2' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalogabsente/absente-elev', 'id_elev' => $id, 'sem' => 'II']);
                        return Html::a('<span class="glyphicon glyphicon-dashboard"></span>', $url, ['title' => Yii::t('app', 'Afisare/motivare absente elev , semestrul II')]);
                    },
                    'calcularemediisemestriale1' => function ($url, $model, $id) {


                        $url = Yii::$app->urlManager->createUrl(['catalognote/calculez-medii-semestriale', 'id_elev' => $id, 'sem' => 'I']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['title' => Yii::t('app', 'Calculare medii semestriale  semestrul I')]);

                    },
                    'calcularemediisemestriale2' => function ($url, $model, $id) {


                        $url = Yii::$app->urlManager->createUrl(['catalognote/calculez-medii-semestriale', 'id_elev' => $id, 'sem' => 'II']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['title' => Yii::t('app', 'Calculare medii semestriale semestrul II')]);

                    },
                    'afisaremediisemestriale1' => function ($url, $model, $id) {


                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-medii-semestriale-elev', 'id_elev' => $id, 'sem' => 'I']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-share"></span>', $url, ['title' => Yii::t('app', 'Afisare medii semestriale  semestrul I')]);

                    },
                    'afisaremediisemestriale2' => function ($url, $model, $id) {


                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-medii-semestriale-elev', 'id_elev' => $id, 'sem' => 'II']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-share"></span>', $url, ['title' => Yii::t('app', 'Afisare medii semestriale semestrul II')]);

                    },

                    'purtare1' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['mediisemestriale/set-nota-purtare', 'id_elev' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-comment"></span>', $url, ['title' => Yii::t('app', 'Stabileste nota la purtare ')]);
                    },

                    'medii_gen' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['diriginti/calculare-medii-generale-elev', 'id_elev' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-cog"></span>', $url, ['title' => Yii::t('app', 'Calculeaza mediile generale anuale ale elevului')]);
                    }

                ]

            ]

        ],

    ]); ?>

    <?php
    $this->registerJs("
    $('.delete').click(function(){
    if(!confirm('Sunteti sigur ca vreti sa stergeti elevul din clasa ?'))
    {
    return false;
    }
});
    ");
    ?>

</div>
