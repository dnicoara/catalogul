<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Catalognote */
/* @var $form yii\widgets\ActiveForm */

//obtin valorile parametrilor globali definiti in config/params.php
$anul_scolar = \Yii::$app->params['anul_scolar'];
$semestrul = \Yii::$app->params['semestrul'];
$id_clasa = Yii::$app->session['id_clasa'];

?>

<?php if (isset($mesaj))
    echo '<span style="color:red "><h5>' . Html::encode($mesaj) . '</h5></span>';
?>
<?php if (Yii::$app->request->get('mesaj'))
    echo '<span style="color:red "><h5>' . Html::encode(Yii::$app->request->get('mesaj')) . '</h5></span>';
?>
<br>

<div class="eleviclasa-form">


    <?php $form = ActiveForm::begin([
        'action' => ['elevi/adauga-elev-in-clasa'],
        'method' => 'get',
    ]);
    ?>

    <?php

    echo $form->field($model, 'id')->widget(Select2::classname(), [
        'name' => 'elevii',
        'data' => $lista_elevi,
        'language' => 'ro',
        'size' => 'md',
        'options' => ['placeholder' => 'Selecteaza elevii ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza elevii');

    ?>

    <br><br>

    <div class="form-group">
        <?= Html::submitButton('Introduceti elevii in clasa', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['site/index'], ['class' => 'btn btn-success']) ?>

    </div>
    <?= Html::a('Elevul nu se afla in lista', ['elevi/create'], ['class' => 'btn btn-primary']) ?>
    <br>
    <?php ActiveForm::end(); ?>

</div>
