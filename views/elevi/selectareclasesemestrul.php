<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php
if (isset($_GET['op'])) {
    if ($_GET['op'] == 'corigenti')
        $optiunea = 'elevii corigenti';
    else
        $optiunea = 'elevi cu note scazute la purtare';
} else
    $optiunea = '';

$this->title = 'Selectare clase : ' . $optiunea;
$this->params['breadcrumbs'][] = $this->title;

?>

<?php
$request = Yii::$app->request;

// In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
// <div class="alert alert-danger" role="alert"></div>
if ($request->isPost) {
    echo '<div class="alert alert-success" role="alert">';
    if (isset($mesaj)) {
        echo Html::encode($mesaj);
    } elseif ($request->post('mesaj') !== null) {
        echo Html::encode($request->post('mesaj'));
    }
    echo '</div>';
}
?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>

<div class="site-about">

    <div class="selectareclase-form">


        <?php $form = ActiveForm::begin([
            'action' => ['selectare-clase-semestrul', 'op' => $_GET['op']],
            'method' => 'get',
        ]);
        ?>
        <?php
        echo $form->field($model, 'Clasa')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => ['I' => 'I', 'II' => 'II', 'Anual' => 'Anual'],
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Situatie semestriala sau anuala ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Situatie semestriala sau anuala');

        ?>


        <?php
        echo $form->field($model, 'id')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza clasele ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza clasele');

        ?>

        <div class="form-group">
            <?= Html::submitButton('Continua...', ['class' => 'btn btn-success']) ?><br><br>
            <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
