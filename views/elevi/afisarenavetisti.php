<?php

use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Infoelevi */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Date elevi-Statistici --clasa  : afisare elevi navetisti';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clasa-index">
    <h4><?= Html::encode('Lista elevilor navetisti') ?></h4>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['elevi/afisare-clase-selectate', 'op' => 'navetisti'], ['class' => 'btn btn-primary']) ?>

    </p>
    <br><br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nume',
        'prenume',
        'naveteaza_din',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items} {pager} {summary}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'nume',
                'value' => 'nume'
            ],
            [
                'attribute' => 'prenume',
                'value' => 'prenume'
            ],

            [
                'attribute' => 'naveteaza_din',
                'value' => 'naveteaza_din'
            ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

        ],

    ]); ?>


</div>
