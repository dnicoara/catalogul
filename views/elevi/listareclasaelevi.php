<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EleviSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clasa selectata : ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<br>

<div class="clasa-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la clasele profesorului', ['listaclase/clase-profesor'], ['class' => 'btn btn-success']) ?>
        <br><br>

        <?= Html::a('Calculeaza mediile semestriale ale elevilor clasei', ['catalognote/selectez-materia', 'id_clasa' => $id_clasa, 'medie_elev' => false], ['class' => 'btn btn-primary']) ?>
        <br><br>
        <?= Html::a('Calculeaza mediile generale ale elevilor clasei', ['listaclase/materii-profesor', 'id_clasa' => $id_clasa], ['class' => 'btn btn-primary']) ?>
        <br><br>
        <?= Html::a('Afisarea situatiei scolara a clasei', ['profesori/selectez-materia-profesorului', 'id_clasa' => $id_clasa], ['class' => 'btn btn-info']) ?>

    </p>
    <br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nr_matricol',
            'nume',
            'prenume',
            'email',


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{note} {absente}  {vadnote1} {vadnote2} {vadabsente1}  {vadabsente2}   {calculezmediisem} {calculezmediigen}',
                'buttons' => [
                    'note' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalognote/create', 'id_elev' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['title' => Yii::t('app', 'Catalog note elev - introduceti nota elev')]);
                    },
                    'absente' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalogabsente/create', 'id_elev' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-dashboard"></span>', $url, ['title' => Yii::t('app', 'Catalog absente-introduceti / motivati absente elev')]);
                    },
                    'vadnote1' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-note-elev-la-materia-profesorului', 'id_elev' => $id, 'sem' => 'I']);
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Vizualizare / editare note elev , sem I')]);
                    },
                    'vadnote2' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-note-elev-la-materia-profesorului', 'id_elev' => $id, 'sem' => 'II']);
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Vizualizare / editare note elev , sem II')]);
                    },
                    'vadabsente1' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalogabsente/afiseaza-absente-elev-la-materia-profesorului', 'id_elev' => $id, 'sem' => 'I']);
                        return Html::a('<span class="glyphicon glyphicon-th-list"></span>', $url, ['title' => Yii::t('app', 'Vizualizare absente elev , sem I')]);
                    },
                    'vadabsente2' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalogabsente/afiseaza-absente-elev-la-materia-profesorului', 'id_elev' => $id, 'sem' => 'II']);
                        return Html::a('<span class="glyphicon glyphicon-th-list"></span>', $url, ['title' => Yii::t('app', 'Vizualizare absente elev , sem II')]);
                    },
                    'calculezmediisem' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalognote/selectez-materia', 'id_elev' => $id, 'medie_elev' => true]);
                        return Html::a('<span class="glyphicon glyphicon-share"></span>', $url, ['title' => Yii::t('app', 'Calculez media semestriala a elevului')]);
                    },
                    'calculezmediigen' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['mediigenerale/selectez-materia', 'id_elev' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-cog"></span>', $url, ['title' => Yii::t('app', 'Calculez media generala a elevului')]);
                    }

                ]
            ]
        ]
    ]); ?>


</div>
