<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Elevi */

$this->title = 'Adaugare elev in lista elevilor scolii';
$this->params['breadcrumbs'][] = ['label' => 'Elevi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elevi-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
