<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Elevi */
/* @var $form yii\widgets\ActiveForm */
$sexul = \Yii::$app->params['sexul'];

?>
<?php
$this->registerJs(
    " $('#nr_matricol').on('change',function()
     {
     var nrmatricol=$('#nr_matricol').val().toString();
    $('#email').val(nrmatricol+'@yahoo.com');
     } );",
    View::POS_END
);
?>

<div class="elevi-form">

    <?php $form = ActiveForm::begin(['id' => 'elevi']); ?>

    <?php //= $form->field($model, 'nr_matricol')->textInput(['maxlength' => true, 'style' => 'width:400px','id'=>'nr_matricol','onchange'=>"$('#email').val($(this).val().toString()+'@yahoo.com');"]) ?>

    <?= $form->field($model, 'nr_matricol')->textInput(['maxlength' => true, 'style' => 'width:400px', 'id' => 'nr_matricol']) ?>

    <?= $form->field($model, 'nume')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'prenume')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'style' => 'width:400px', 'id' => 'email']) ?>

    <?= $form->field($model, 'sex')->widget(Select2::classname(), [
        'data' => $sexul,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza sexul elevului ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza sexul elevului'); ?>



    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?=
            $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                            Yii::$app->session['user']->role == 'diriginte' :
                                            false; 
        ?>
        <?= Html::a('Renunta', [$este_dirigintele_conectat ? 'site/index' : 'admin/administrator-adauga'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>


</div>
