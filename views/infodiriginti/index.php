<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InfodirigintiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Infodirigintis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infodiriginti-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Infodiriginti', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fk_diriginte',
            'sex',
            'studii',
            'telefon',
            //'localitate',
            //'strada',
            //'numar',
            //'bloc',
            //'scara',
            //'apartament',
            //'judet',
            //'buletin_seria',
            //'buletin_nr',
            //'grad_didactic',
            //'vechime',
            //'data_actualizarii',
            //'data_inregistrarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
