<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Infodiriginti */

$this->title = 'Adauga diriginte';
$this->params['breadcrumbs'][] = ['label' => 'Infodiriginti', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infodiriginti-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
