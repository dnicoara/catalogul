<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Infodiriginti */

$this->title = 'Editeaza informatii despre diriginte ';
$this->params['breadcrumbs'][] = ['label' => 'Infodiriginti', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza';
?>
<div class="infodiriginti-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
