<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Infodiriginti */
/* @var $form yii\widgets\ActiveForm */
$lista_judete = \Yii::$app->params['judete'];

?>

<div class="infodiriginti-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //= $form->field($model, 'sex')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'sex')->widget(Select2::classname(), [
        'data' => ['M' => 'M', 'F' => 'F'],
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza sexul ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza sexul'); ?>

    <?= $form->field($model, 'studii')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'telefon')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'localitate')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'strada')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'numar')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'bloc')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'scara')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'apartament')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?php //= $form->field($model, 'judet')->textInput(['maxlength' => true,'style' => 'width:400px']) ?>

    <?= $form->field($model, 'judet')->widget(Select2::classname(), [
        'data' => $lista_judete,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza judetul ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza judetul'); ?>

    <?= $form->field($model, 'buletin_seria')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'buletin_nr')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'grad_didactic')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'vechime')->textInput(['style' => 'width:400px']) ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= 
        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'diriginte' :
                                        false;
        ?>
        <?= Html::a('Renunta', [$este_dirigintele_conectat ? 'infodiriginti/cont-diriginte' : 'site/index'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
