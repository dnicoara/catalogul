<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Infodiriginti */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Infodirigintis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infodiriginti-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?=
            $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                            Yii::$app->session['user']->role == 'diriginte' :
                                            false;
        ?>
        <?= Html::a('Revenire', [$este_dirigintele_conectat ? 'infodiriginti/cont-diriginte' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fk_diriginte',
            'sex',
            'studii',
            'telefon',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',
            'buletin_seria',
            'buletin_nr',
            'grad_didactic',
            'vechime',
            'data_actualizarii',
            'data_inregistrarii',
        ],
    ]) ?>

</div>
