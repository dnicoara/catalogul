<?php

use exocet\BootstrapMD\widgets\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notele elevului : ' . ' ' . $nume_elev . ' ' . $prenume_elev;;
$this->params['breadcrumbs'][] = $this->title

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Adaugare nota', ['catalognote/create', 'id_elev' => $id_elev], ['class' => 'btn btn-info']) ?>
    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Nota',
                'content' => function ($data) {
                    $nota = $data['nota'];
                    return $nota;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Data',
                'content' => function ($data) {
                    $data = $data['data'];
                    return $data;
                },
                'format' => 'text'
            ],

            'teza',
            'sem',


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} ',
            ]

        ],

    ]); ?>


</div>
