<?php

use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notele elevului : ' . ' ' . $nume_elev . ' ' . $prenume_elev;
$this->params['breadcrumbs'][] = $this->title

//retin numele si id-ul clasei selectate
//Yii::$app->session['nume_clasa'] = $nume_clasa;
//Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . 'pe semestrul' . ' ' . $semestrul) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-primary']) ?>
    </p>
    <br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nr_matricol',
        'materia',
        'nota',
        'teza',
        'data',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol=$data['nr_matricol'];
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia=$data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Nota',
                'content' => function ($data) {
                    $nota=$data['nota'];
                    return $nota;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Teza',
                'content' => function ($data) {
                    $teza = $data['teza'];
                    return $teza;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Data',
                'content' => function ($data) {
                    $data=$data['data'];
                    return $data;
                },
                'format' => 'text'
            ],

            //'email',


//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} {delete} ',
//            ]

        ],

    ]); ?>


</div>
