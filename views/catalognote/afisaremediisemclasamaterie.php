<?php

use app\models\Elevi;
use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mediile semestriale ale  elevilor clasei : ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa'], ['class' => 'btn btn-primary']) ?>
    </p>
    <br>
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
    }
    echo '</div>';

    ?>
    <br><br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nr_matricol',
        'nume',
        'prenume',
        'materia',
        'media',
        'sem',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            'nume',
            'prenume',
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Media semestriala',
                'content' => function ($data) {
                    $media = $data['media'];
                    return $media;
                },
                'format' => 'text'
            ],
            'sem'


//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} {delete} ',
//            ]

        ],

    ]); ?>




    <?php

    if (isset($note_insuficiente) && count($note_insuficiente) > 0) {
        echo 'Lista elevilor care nu au suficiente note pentru a li se incheia media semestriala :  <br><br>';

        for ($i = 0; $i < count($note_insuficiente); $i++) {

            echo '<b>' . ($i + 1) . ') ' . Elevi::getNumeElev($note_insuficiente[$i]) . '  ' . Elevi::getPrenumeElev($note_insuficiente[$i]) . '  ;  </b>';
            echo "</br>";
        }

    }
    ?>


</div>
