<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Notele elevului : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev . '  pe semestrul' . ' ' . $semestrul;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev . '  pe semestrul' . ' ' . $semestrul) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la clasele profesorului', ['listaclase/clase-profesor'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa', 'id_clasa' => $id_clasa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Adaugare nota', ['catalognote/create', 'id_elev' => $id_elev], ['class' => 'btn btn-info']) ?>

    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            'listamaterii.materia',
            [
                'label' => 'Nota',
                'content' => function ($data) {
                    $nota = $data['nota'];
                    return $nota;
                },
                'format' => 'text'
            ],
            'teza',
            'sem',
            [
                'label' => 'Data',
                'content' => function ($data) {
                    $data = $data['data'];
                    return $data;
                },
                'format' => 'text'
            ],


//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} {delete} ',
//            ]

        ],

    ]); ?>


</div>
