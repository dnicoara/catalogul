<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mediile semestriale ale  elevului : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev;


?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev . ' ' . 'pe semestrul' . ' ' . $semestrul) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-primary']) ?>
    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Media semestriala',
                'content' => function ($data) {
                    $media = $data['media'];
                    return $media;
                },
                'format' => 'text'
            ],


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} ',
                'buttons' => [
                    'update' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['mediisemestriale/update', 'id' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Editati media')]);
                    },

                ]
            ]

        ],

    ]); ?>


</div>
