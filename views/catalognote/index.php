<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Catalog note';
$this->params['breadcrumbs'][] = $this->title;

$este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'profesor' :
                                        false;

?>
<div class="catalognote-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Adauga note in catalog', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire', [$este_profesorul_conectat ? 'elevi/afisare-clasa' : 'diriginti/afisare-clasa-diriginte'], ['class' => 'btn btn-success']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nr_matricol',
            'id_materie',
            'id_clasa',
            'data',
            //'an_scolar',
            //'nota',
            //'sem',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
