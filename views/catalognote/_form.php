<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Catalognote */
/* @var $form yii\widgets\ActiveForm */

//obtin valorile parametrilor globali definiti in config/params.php
$data = \Yii::$app->params['data'];
$anul_scolar= \Yii::$app->params['anul_scolar'];
$semestrul=\Yii::$app->params['semestrul'];
$id_clasa=Yii::$app->session['id_clasa'];
$luna = \Yii::$app->params['luna'];

if (isset(Yii::$app->session['retinoptiunea']))
    $retin_optiunea = Yii::$app->session['retinoptiunea'];
else
    $retin_optiunea = 0;
if (isset(Yii::$app->session['retindata']))
    $retin_data = Yii::$app->session['retindata'];
else
    $retin_data = 0;

if (isset(Yii::$app->session['datafolosita']))
    $data_folosita = Yii::$app->session['datafolosita'];
else
    $data_folosita = $data;
?>
<div class="catalognote-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nr_matricol')->textInput(['maxlength' => true, 'value' => $nr_matricol, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'id_materie')->widget(Select2::classname(), [
        'data' => $materii_profesor,
        'disabled' => $model->isNewRecord ? false : true,
        'language' => 'ro',
        'options' => count($materii_profesor) === 1 ? ['value' => $materii_profesor[key($materii_profesor)]] : ['placeholder' => 'Selecteaza materia ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza materia'); ?>

    <?= $form->field($model, 'id_clasa')->textInput(['value' => $id_clasa, 'readonly' => true, 'style' => 'width:400px'])->label('Clasa ' . Yii::$app->session['nume_clasa']); ?>

    <?= $form->field($model, 'data')->widget(DatePicker::classname(), [
        'options' => ['readonly' => $model->isNewRecord ? false : false, 'value' => $retin_data == 0 ? $data : $data_folosita, 'style' => 'width:322px'],
        'disabled' => $model->isNewRecord ? false : false,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true,
        ],
    ]) ?>

    <?= Html::checkbox('retindata', $retin_data == 0 ? false : true, ['label' => $model->isNewRecord ? 'Retine data' : '', 'uncheck' => 0, 'style' => ['display' => $model->isNewRecord ? '' : 'none']]) ?>
    <br><br>
    <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'readonly' => true, 'style' => 'width:400px']) ?>


    <?= $form->field($model, 'nota')->widget(Select2::classname(), [
        'data' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
        'language' => 'ro',
        'options' => ['value' => $model->isNewRecord ? 0 : $model->nota, 'placeholder' => 'Selecteaza nota ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Nota'); ?>

    <?= $form->field($model, 'teza')->widget(Select2::classname(), [
        'data' => [1 => 'da', 0 => 'nu'],
        'language' => 'ro',
        'options' => ['value' => $model->isNewRecord ? ($retin_optiunea == 0 ? 0 : 1) : $model->teza, 'placeholder' => 'Selecteaza da daca nota este teza si nu daca nu este teza ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza daca este nota la teza'); ?>

    <?= Html::checkbox('retinoptiunea', $retin_optiunea == 0 ? false : true, ['label' => $model->isNewRecord ? 'Retine optiunea' : '', 'uncheck' => 0, 'style' => ['display' => $model->isNewRecord ? '' : 'none']]) ?>
    <br><br>

    <?= $form->field($model, 'sem')->widget(Select2::classname(), [
        'data' =>$semestrul,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza semestrul ...', 'value' => ($luna >= 2 && $luna <= 6) ? 'II' : 'I'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza semestrul'); ?>

    <?php
    $este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'profesor' :
                                    false;
    ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success', 'materii_profesor' => $materii_profesor]) ?>
        <?= Html::a('Renunta', [$este_profesorul_conectat ? 'elevi/afisare-clasa' : 'elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
