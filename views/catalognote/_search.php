<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CatalognoteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalognote-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nr_matricol') ?>

    <?= $form->field($model, 'id_materie') ?>

    <?= $form->field($model, 'id_clasa') ?>

    <?= $form->field($model, 'data') ?>

    <?php // echo $form->field($model, 'an_scolar') ?>

    <?php // echo $form->field($model, 'nota') ?>

    <?php // echo $form->field($model, 'sem') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
