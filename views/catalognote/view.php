<?php
use app\models\Elevi;
use app\models\Listaclase;
use app\models\Listamaterii;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Catalognote */
$nume = Elevi::getNumeElev($model->nr_matricol);
$prenume = Elevi::getPrenumeElev($model->nr_matricol);
$clasa = Listaclase::getNumeClasa($model->id_clasa);
$materia = Listamaterii::getNumeMaterie($model->id_materie);

$nume_elev = $nume . ' ' . $prenume . ' - clasa' . ' ' . $clasa;

$this->title = $nume_elev;
//$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Catalog-note', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'profesor' :
                                        false;

?>
<div class="catalognote-view">

    <p>
        <?= Html::a('Revenire la clasa elevi', [$este_profesorul_conectat ? 'elevi/afisare-clasa' : 'elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id, 'materii_profesor' => $materii_profesor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Afiseaza toate notele elevului la materia selectata', ['view-note-elev-materie', 'id' => $model->id, 'id_materie' => $model->id_materie, 'id_clasa' => $model->id_clasa, 'an_scolar' => $model->an_scolar, 'sem' => $model->sem], ['class' => 'btn btn-primary']) ?>
    </p>
    <br>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'nr_matricol',
            //'id_materie',
            //'id_clasa',
            'data',
            'an_scolar',
            'nota',
            'teza',
            'sem',
        ],
    ]) ?>
    <h4><?= Html::encode($nume_elev) ?></h4>
    <h4><?= Html::encode('Obiectul : ' . $materia) ?></h4>

</div>
