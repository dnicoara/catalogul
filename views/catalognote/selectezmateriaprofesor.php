<?php
use app\models\Profesori;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Selecteaza semestrul si materia predata de profesorul : ' . $nume_profesor . ' ' . $prenume_profesor;
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<?php if (isset($mesaj)) {
    echo '<div class="alert alert-success" role="alert">';
    echo Html::encode($mesaj);
    echo '</div>';
} else {
    if (Yii::$app->request->get('mesaj')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Html::encode(Yii::$app->request->get('mesaj'));
        echo '</div>';
    }
}
if (isset($_GET['medie_elev']))
$medie_elev = $_GET['medie_elev'];
else
    return Yii::$app->response->redirect(Url::to(['elevi/afisare-clasa']));


if (isset($_GET['id_elev']) && !empty($_GET['id_elev']))
    $id_elev = $_GET['id_elev'];
else
    $id_elev = "";
?>

<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>
    <br>
    <div class="materiaProfesor-form">


        <?php $form = ActiveForm::begin([
            'action' => [!$medie_elev ? 'catalognote/profesor-mediisem' : 'catalognote/profesor-mediisem-elev', ['id_elev' => $id_elev]],
            'method' => 'get',
        ]);
        ?>

        <?php
        //        echo $form->field($modelclase, 'id')->widget(Select2::classname(), [
        //            'name' => 'Clasa',
        //            'data' => $datele,
        //            'language' => 'ro',
        //            'size' => 'md',
        //            'options' => ['placeholder' => 'Selecteaza clasa ...'],
        //            'pluginOptions' => [
        //                'allowClear' => true,
        //                'width' => '300px'
        //            ],
        //        ])->label('Selecteaza clasa');

        echo $form->field($modelmaterii, 'id')->widget(Select2::classname(), [
            //'data' => ArrayHelper::map(CartiCustom::CartiDisponibile(), 'id_carte', 'titlu_carte'),
            'name' => 'profesorul',
            'data' => $materii_predate,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza materia ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza materia');

        ?>

        <?= $form->field($modelnote, 'sem')->widget(Select2::classname(), [
            'data' => ['I' => 'I', 'II' => 'II'],
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza semestrul ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza semestrul'); ?>

        <br>
        <div class="form-group">
            <?= Html::submitButton('Calculez media semestriala la materia selectata', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Iesire', ['elevi/afisare-clasa'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
