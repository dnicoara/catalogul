<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Catalognote */

$this->title = 'Editare nota elev ';
$this->params['breadcrumbs'][] = ['label' => 'Catalog note', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="catalognote-update">

    <h4><?= Html::encode('Editeaza nota elevului ') . Html::encode($nume_elev) . ' ' . Html::encode($prenume_elev) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'nr_matricol' => $model->nr_matricol,
        'materii_profesor' => $materii_profesor,
        'nume_elev' => $nume_elev,
        'prenume_elev' => $prenume_elev
    ]) ?>

</div>
