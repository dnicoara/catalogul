<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InfoeleviSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Informatii despre elevi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infoelevi-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>
    <?php
    $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
        Yii::$app->session['user']->role == 'admin' :
        false;
    ?>
    <?= Html::a('Rvenire la pagina principala', [$este_administratorul_conectat ? 'admin/administrator-edit' : 'site/index'], ['class' => 'btn btn-primary']) ?>
    <br><br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nr_matricol',
        'scutit_la',
        'nume',
        'prenume',
        'telefon',
        'tata',
        'mama',
        'ocupatie_mama',
        'ocupatie_tata',
        'religie',
        'naveta',
        'nr_frati',
        'localitate',
        'strada',
        'numar',
        'bloc',
        'scara',
        'apartament',
        'judet',
        'buletin_seria',
        'buletin_nr',
        'cod_num',
        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nr_matricol',
            'scutit_la',
            'nume',
            'prenume',
            'telefon',
            'tata',
            'mama',
            'ocupatie_mama',
            'ocupatie_tata',
            'religie',
            'naveta',
            'nr_frati',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',
            'buletin_seria',
            'buletin_nr',
            'cod_num',
            //'data_actualizarii',
            //'data_inregistrarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
