<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Infoelevi */

$this->title = 'Editeaza informatiile  elevului : ' . $nume_elev . ' ' . $prenume_elev;
$this->params['breadcrumbs'][] = ['label' => 'Infoelevi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="infoelevi-update">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php
    if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <?= $this->render('_form', [
        'model' => $model,
        'modelElevi' => $modelElevi,
        'nume_elev' => $nume_elev,
        'prenume_elev' => $prenume_elev,
        'lista_materiilor' => $lista_materiilor,


    ]) ?>

</div>
