<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Infoelevi */

$this->title = 'Adauga informatii despre elevul selectat';
$this->params['breadcrumbs'][] = ['label' => 'Informatii elev', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infoelevi-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'modelElevi' => $modelElevi,
        'nume_elev' => $nume_elev,
        'prenume_elev' => $prenume_elev,
    ]) ?>

</div>
