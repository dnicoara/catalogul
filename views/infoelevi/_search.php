<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InfoeleviSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="infoelevi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nr_matricol') ?>

    <?php //= $form->field($model, 'scutit_la') ?>

    <?= $form->field($model, 'telefon') ?>

    <?= $form->field($model, 'tata') ?>

    <?= $form->field($model, 'mama') ?>

    <?php // echo $form->field($model, 'ocupatie_mama') ?>

    <?php // echo $form->field($model, 'ocupatie_tata') ?>

    <?php // echo $form->field($model, 'religie') ?>

    <?php // echo $form->field($model, 'naveta') ?>

    <?php // echo $form->field($model, 'nr_frati') ?>

    <?php // echo $form->field($model, 'localitate') ?>

    <?php // echo $form->field($model, 'strada') ?>

    <?php // echo $form->field($model, 'numar') ?>

    <?php // echo $form->field($model, 'bloc') ?>

    <?php // echo $form->field($model, 'scara') ?>

    <?php // echo $form->field($model, 'apartament') ?>

    <?php // echo $form->field($model, 'judet') ?>

    <?php // echo $form->field($model, 'buletin_seria') ?>

    <?php // echo $form->field($model, 'buletin_nr') ?>

    <?php // echo $form->field($model, 'cod_num') ?>

    <?php // echo $form->field($model, 'data_actualizarii') ?>

    <?php // echo $form->field($model, 'data_inregistrarii') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
