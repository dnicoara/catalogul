<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Infoelevi */
/* @var $form yii\widgets\ActiveForm */
$naveta = \Yii::$app->params['naveta'];
$lista_judete = \Yii::$app->params['judete'];

$este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
    Yii::$app->session['user']->role == 'admin' :
    false;

if ($este_administratorul_conectat)
    $adminConectat = true;
else
    $adminConectat = false;
?>

<div class="infoelevi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nr_matricol')->textInput(['style' => 'width:400px', 'readonly' => $adminConectat ? false : true]) ?>

    <?php //= $form->field($model, 'scutit_la')->textInput(['style' => 'width:400px']) ?>
    <?= $form->field($model, 'hidden')->hiddenInput(['value' => $model->nr_matricol])->label(false); ?>

    <?= $form->field($model, 'scutit_la')->widget(Select2::classname(), [
        'data' => $lista_materiilor,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza materia ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
            'width' => '400px'
        ],
    ])->label('Scutit la materia'); ?>

    <?= $form->field($modelElevi, 'nume')->textInput(['style' => 'width:400px']) ?>

    <?= $form->field($modelElevi, 'prenume')->textInput(['style' => 'width:400px']) ?>

    <?= $form->field($modelElevi, 'email')->textInput(['style' => 'width:400px']) ?>

    <?= $form->field($model, 'telefon')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'tata')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'mama')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'ocupatie_mama')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'ocupatie_tata')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'religie')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'naveta')->widget(Select2::classname(), [
        'data' => $naveta,
        'language' => 'ro',
        'value' => 'nu',
        //'options' => ['placeholder' => 'Selecteaza optiunea ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza daca elevul naveteaza'); ?>


    <?= $form->field($model, 'nr_frati')->textInput(['style' => 'width:400px']) ?>

    <?= $form->field($model, 'localitate')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'strada')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'numar')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'bloc')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'scara')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'apartament')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?php //= $form->field($model, 'judet')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'judet')->widget(Select2::classname(), [
        'data' => $lista_judete,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza judetul ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza judetul'); ?>

    <?= $form->field($model, 'buletin_seria')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'buletin_nr')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'cod_num')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>


    <div class="form-group">
        <?= Html::submitButton('Salveaza modificarile', ['class' => 'btn btn-success']) ?>
        <?=
            $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                            Yii::$app->session['user']->role == 'diriginte' :
                                            false;
        ?>
        <?= Html::a('Renunta', $este_dirigintele_conectat ? ['elevi/afisare-informatii-elev', 'id' => $model->id] : ['admin/administrator-adauga'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
