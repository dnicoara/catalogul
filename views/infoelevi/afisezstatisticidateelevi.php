<?php

use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Infoelevi */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Date elevi-Statistici --clasa  : ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clasa-index">

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['infoelevi/statistici-date-elevi'], ['class' => 'btn btn-primary']) ?>


    </p>
    <br><br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        'nume',
        'prenume',
        $optiunea == 'varsta' ? 'varsta' : ($optiunea == 'navetisti' ? 'naveteaza_din' : 'apartenenta_religioasa'),

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items} {pager} {summary}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'nume',
                'value' => 'nume'
            ],
            [
                'attribute' => 'prenume',
                'value' => 'prenume'
            ],

            [
                'attribute' => $optiunea == 'varsta' ? 'varsta' : ($optiunea == 'navetisti' ? 'naveteaza_din' : 'apartenenta_religioasa'),
                'value' => $optiunea == 'varsta' ? 'varsta' : ($optiunea == 'navetisti' ? 'naveteaza_din' : 'apartenenta_religioasa')
            ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

        ],

    ]); ?>


</div>
