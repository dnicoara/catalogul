<?php

use yii\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$nume = $prenume_elev . ' ' . $nume_elev;
$this->title = 'Cont elev : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume;
?>
<div class="site-about">
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>

    <div class="infoelevi-index">

    <h4><?= Html::encode($this->title . ' ' . $nume) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire', ['site/index'], ['class' => 'btn btn-success']) ?>

    </p>


    <br><br>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'elevi.email',
            'telefon',
            'tata',
            'mama',
            'ocupatie_tata',
            'ocupatie_mama',
            'religie',
            'naveta',
            'nr_frati',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',
            'buletin_seria',
            'buletin_nr',
            'cod_num',


//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view}  {update} {delete}',
//            ]
        ],

    ]); ?>
    <br><br>

        <?= Html::a('Modifica adresa mail', ['elevi/schimba-adresa-mail-form'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Modifica parola', ['elevi/schimba-parola-form'], ['class' => 'btn btn-success']) ?>


</div>
