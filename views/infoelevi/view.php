<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Infoelevi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Informatii despre elevi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infoelevi-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?=
            $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                            Yii::$app->session['user']->role == 'diriginte' :
                                            false; 
        ?>
        <?= Html::a($este_dirigintele_conectat ? 'Revenire la elevii clasei' : 'Revenire la pagina principala', [$este_dirigintele_conectat ? 'elevi/afisare-clasa-diriginte' : 'admin/administrator-edit'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Revenire la infoelevi ', 'index', ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nr_matricol',
            'scutit_la',
            'telefon',
            'tata',
            'mama',
            'ocupatie_mama',
            'ocupatie_tata',
            'religie',
            'naveta',
            'nr_frati',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',
            'buletin_seria',
            'buletin_nr',
            'cod_num',
            'data_actualizarii',
            'data_inregistrarii',
        ],
    ]) ?>

</div>
