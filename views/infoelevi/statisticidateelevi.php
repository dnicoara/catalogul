<?php
use yii\helpers\Html;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Meniul statistici-date-elevi';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
$request = Yii::$app->request;

// In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
// <div class="alert alert-danger" role="alert"></div>
if ($request->isPost) {
    echo '<div class="alert alert-success" role="alert">';
    if (isset($mesaj)) {
        echo Html::encode($mesaj);
    } elseif ($request->post('mesaj') !== null) {
        echo Html::encode($request->post('mesaj'));
    }
    echo '</div>';
}
?>

<div class="site-about">

    <div class="clasaInfoelevi-form">

        <?= Html::a('Varsta elevilor clasei', ['infoelevi/statistici-date-elevi', 'optiunea' => 'varsta'], ['class' => 'btn btn-primary']) ?>
        <br><br>
        <?= Html::a('Elevii navetisti', ['infoelevi/statistici-date-elevi', 'optiunea' => 'navetisti'], ['class' => 'btn btn-primary']) ?>
        <br><br>
        <?= Html::a('Religia elevilor', ['infoelevi/statistici-date-elevi', 'optiunea' => 'religia'], ['class' => 'btn btn-primary']) ?>
        <br><br>

        <?= Html::a('Iesire', ['/site/index'], ['class' => 'btn btn-success']) ?><br>


    </div>

</div>
