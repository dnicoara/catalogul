<?php
use app\models\Profesori;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Clasa : ' . $nume_clasa . ' - selectare elev';
$this->params['breadcrumbs'][] = 'Adaugare informatii despre elevi';

?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>
    <br>
    <div class="infoElevi-form">


        <?php $form = ActiveForm::begin([
            'action' => ['infoelevi/create'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model, 'id')->widget(Select2::classname(), [
            'data' => $lista_elevi,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza elevul ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px',
            ],
        ])->label('Selecteaza elevul din lista'); ?>

        <br>
        <div class="form-group">
            <?= Html::submitButton('Adauga informatii', ['class' => 'btn btn-success']) ?>
            <?=
                $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                                Yii::$app->session['user']->role == 'diriginte' :
                                                false; 
            ?>
            <?= Html::a('Renunta', [$este_dirigintele_conectat ? 'site/index' : 'admin/administrator-adauga'], ['class' => 'btn btn-danger']) ?>

        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
