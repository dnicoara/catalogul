<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Notificaridiriginte */
$diriginte = \app\models\Diriginti::getDiriginteDupaIdClasa($id_clasa);
$nume = $diriginte['nume'];
$prenume = $diriginte['prenume'];

$this->title = 'Editeaza notificarea catre dirigintele: ' . $nume . ' ' . $prenume;
$this->params['breadcrumbs'][] = ['label' => 'Notificari diriginte', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza';
?>
<div class="notificaridiriginte-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'nr_matricol' => $nr_matricol,
        'nume_elev' => $nume_elev,
        'prenume_elev' => $prenume_elev,
        'id_clasa' => $id_clasa,
        'an_scolar' => $an_scolar
    ]) ?>

</div>
