<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NotificaridiriginteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notificaridiriginte-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nr_matricol') ?>

    <?= $form->field($model, 'id_clasa') ?>

    <?= $form->field($model, 'nume') ?>

    <?= $form->field($model, 'prenume') ?>

    <?php // echo $form->field($model, 'notificarea') ?>

    <?php // echo $form->field($model, 'an_scolar') ?>

    <?php // echo $form->field($model, 'statut') ?>

    <?php // echo $form->field($model, 'data_inregistrarii') ?>

    <?php // echo $form->field($model, 'data_actualizarii') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
