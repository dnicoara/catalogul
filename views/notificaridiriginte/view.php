<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Notificaridiriginte */
$diriginte = \app\models\Diriginti::getDiriginteDupaIdClasa($id_clasa);
$nume = $diriginte['nume'];
$prenume = $diriginte['prenume'];

$this->title = $nume . ' ' . $prenume;
$this->params['breadcrumbs'][] = ['label' => 'Notificari diriginte', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notificaridiriginte-view">

    <h4><?= Html::encode('Notificare pentru diriginte') ?></h4>

    <p>
        <?= Html::a('Editeaza notificarea', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge notificarea', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Revenire pagina principala', ['site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nr_matricol',
            'id_clasa',
            'nume',
            'prenume',
            'notificarea',
            'an_scolar',
            'statut',
            'data_inregistrarii',
            'data_actualizarii',
        ],
    ]) ?>

</div>
