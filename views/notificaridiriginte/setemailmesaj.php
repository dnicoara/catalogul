<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php
$this->title = 'Selectare elevi din clasa  : ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>

    <div class="emailElevi-form">

        <?php $form = ActiveForm::begin([
            'action' => ['notificaridiriginte/trimit-email-elevi'],
            'method' => 'get',
        ]);
        ?>
        <?php
        echo $form->field($model, 'id')->widget(Select2::classname(), [
            'name' => 'elevii',
            'data' => $lista_elevi,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza elevii ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza elevii');
        ?>

        <?= $form->field($model, 'notificarea')->textarea(['maxlength' => 500, 'rows' => 11, 'cols' => 50, 'style' => 'width:400px'])->hint('Max 500 caractere.')->label('Subiectul e-mailului') ?>

        <div class="form-group">
            <?= Html::submitButton('Trimite', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Renunta', ['site/index'], ['class' => 'btn btn-danger']) ?>

        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>









