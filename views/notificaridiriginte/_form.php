<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Notificaridiriginte */
/* @var $form yii\widgets\ActiveForm */

$este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'diriginte' :
                                        false;
?>

<div class="notificaridiriginte-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nr_matricol')->textInput(['maxlength' => true, 'value' => $nr_matricol, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'id_clasa')->textInput(['value' => $id_clasa, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'nume')->textInput(['maxlength' => true, 'value' => $nume_elev, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'prenume')->textInput(['maxlength' => true, 'value' => $prenume_elev, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $an_scolar, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'notificarea')->textarea(['maxlength' => 500, 'rows' => 11, 'cols' => 50, 'style' => 'width:400px', 'readonly' => $este_dirigintele_conectat])->hint('Max 500 caractere.') ?>

    <?= $form->field($model, 'statut')->widget(Select2::classname(), [
        'data' => ['nerezolvat' => 'nerezolvat', 'rezolvat' => 'rezolvat'],
        'language' => 'ro',
        'options' => ['placeholder' => 'Seteaza statutul notificarii ...', 'value' => 'nerezolvat'],
        'pluginOptions' => [
            'allowClear' => true,
            'disabled' => !$este_dirigintele_conectat,
            'width' => '400px'
        ],
    ])->label('Seteaza statutul notificarii'); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Adauga notificarea' : 'Editeaza notificarea', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?= Html::a('Renunta', ['site/index'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
