<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Notificaridiriginte */

$this->title = 'Adauga notificare pentru diriginte';
$this->params['breadcrumbs'][] = ['label' => 'Notificari diriginte', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notificaridiriginte-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'nr_matricol' => $nr_matricol,
        'nume_elev' => $nume_elev,
        'prenume_elev' => $prenume_elev,
        'id_clasa' => $id_clasa,
        'an_scolar' => $an_scolar
    ]) ?>

</div>
