<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NotificaridiriginteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notificarile dirigintelui';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notificaridiriginte-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //= Html::a('Adauga notificare  pentru diriginte', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nr_matricol',
            //'id_clasa',
            'nume',
            'prenume',
            'notificarea',
            'an_scolar',
            'statut',
            'data_inregistrarii',
            'data_actualizarii',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} ',
                'buttons' => [
                    'update' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['notificaridiriginte/actualizeaza', 'id_notificare' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Editeaza notificarea')]);
                    },
                    'delete' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['notificaridiriginte/sterge', 'id_notificare' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('app', 'Sterge notificarea')]);
                    },
                ]

            ],
        ],
    ]); ?>
    <?= Html::a('Revenire pagina principala', ['site/index'], ['class' => 'btn btn-primary']) ?>

</div>
