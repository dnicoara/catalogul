<?php

use app\models\Elevi;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NotificaridiriginteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$nume_elev = Elevi::getNumeElev($nr_matricol) . ' ' . Elevi::getPrenumeElev($nr_matricol);
$this->title = 'Notificarile facute dirigintelui de catre : ' . $nume_elev;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notificaridiriginte-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <br>

    <p>
        <?= Html::a('Revenire pagina principala', ['site/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Adauga notificare  pentru diriginte', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nr_matricol',
            //'id_clasa',
            'nume',
            'prenume',
            'notificarea',
            [
                'label' => 'Anul scolar',
                'content' => function ($data) {
                    $an_scolar = $data['an_scolar'];
                    return $an_scolar;
                },
                'format' => 'text'
            ],
            //'an_scolar',
            'statut',
            [
                'label' => 'Data inregistrarii notificarii',
                'content' => function ($data) {
                    $data_inregistrarii = $data['data_inregistrarii'];
                    return $data_inregistrarii;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Data actualizarii notificarii',
                'content' => function ($data) {
                    $data_actualizarii = $data['data_inregistrarii'];
                    return $data_actualizarii;
                },
                'format' => 'text'
            ],
            //'data_inregistrarii',
            //'data_actualizarii',
        ],
    ]); ?>

</div>
