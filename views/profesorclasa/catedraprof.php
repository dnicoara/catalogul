<?php
use app\models\Profesori;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$anul_scolar= \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Formare catedra profesor';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php 
    $request = Yii::$app->request;
    
    // In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
    // <div class="alert alert-danger" role="alert"></div>
    if ($request->isPost) {
    	echo '<div class="alert alert-success" role="alert">';
        if (isset($mesaj)) {
            echo Html::encode($mesaj);
	} elseif ($request->post('mesaj') !== null) {
            echo Html::encode($request->post('mesaj'));
	}
     	echo '</div>';
     }
echo '<div>Selectati profesorul , clasele  si materia pe care profesorul o preda la clasele selectate .Se pot selecta mai multe clase , dar o singura materie<br>
Reluati operatia de cate ori este necesar pentru a introduce toate  materiile si clasele la care are ore profesorul</div>';

?>

<div class="site-about">

    <div class="catedraProfesor-form">


        <?php $form = ActiveForm::begin([
            'action' => ['profesorclasa/edit-catedra'],
            'method' => 'post',
        ]);
        ?>
        <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'readonly' => true, 'style' => 'width:300px']) ?>

        <?php

        echo $form->field($model, 'id_profesor')->widget(Select2::classname(), [
            //'data' => ArrayHelper::map(CartiCustom::CartiDisponibile(), 'id_carte', 'titlu_carte'),
            'name'=>'profesorul',
            'data' =>$lista_profesorilor,
            'language' => 'ro',
            'size'=>'md',
            'options' => ['placeholder' => 'Selecteaza profesorul ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'300px'
            ],
        ])->label('Selecteaza profesorul');

        echo $form->field($model, 'id_clasa')->widget(Select2::classname(), [
            //'data' => ArrayHelper::map(CartiCustom::CartiDisponibile(), 'id_carte', 'titlu_carte'),
            'name' => 'clase',
            'data' =>$lista_claselor,
            'language' => 'ro',
            'size'=>'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
                'width'=>'300px'
            ],
        ])->label('Selecteaza clasa');

        echo $form->field($model, 'id_materie')->widget(Select2::classname(), [
            //'data' => ArrayHelper::map(CartiCustom::CartiDisponibile(), 'id_carte', 'titlu_carte'),
            'name'=>'profesorul',
            'data' =>$lista_materiilor,
            'language' => 'ro',
            'size'=>'md',
            'options' => ['placeholder' => 'Selecteaza materia ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'300px'
            ],
        ])->label('Selecteaza materia');

        ?>

        <div class="form-group">
            <?= Html::submitButton('Adauga inregistrarea la baza de date', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Iesire', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
