<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Profesorclasa */
/* @var $form yii\widgets\ActiveForm */
$anul_scolar = \Yii::$app->params['anul_scolar'];
if ($id_prof != -1) {
    $nume_prof = \app\models\Profesori::getNumeProfesor($id_prof);
    $prenume_prof = \app\models\Profesori::getPrenumeProfesor($id_prof);
    $numele = $nume_prof . ' ' . $prenume_prof;
}
?>

<div class="profesorclasa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_profesor')->widget(Select2::classname(), [
        'data' => $lista_profesori,
        'language' => 'ro',
        'options' => ['placeholder' => $id_prof != -1 ? $numele : 'Selecteaza profesorul ...', 'value' => $id_prof != -1 ? $id_prof : ''],
        'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza profesorul'); ?>



    <?= $form->field($model, 'id_clasa')->widget(Select2::classname(), [
        'data' => $lista_clase,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza clasa ...'],
        'disabled' => $model->isNewRecord ? false : false,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza clasa'); ?>

    <?= $form->field($model, 'id_materie')->widget(Select2::classname(), [
        'data' => $lista_materii,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza materia ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza materia'); ?>


    <?= $form->field($model, 'an_scolar')->textInput(['value' => $anul_scolar, 'style' => 'width:400px']) ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?php
        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;
        ?>
        <?= Html::a('Renunta', [$este_directorul_conectat ? 'directori/catedra-profesor?id_prof=' . $id_prof : 'profesorclasa/index'], ['class' => 'btn btn-primary']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
