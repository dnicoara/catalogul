<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Profesorclasa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Profesorclasas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesorclasa-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Revenire - catedre profesori', ['profesorclasa/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Revenire - catedra profesorului', ['directori/catedra-profesor?id_prof=' . $model->id_profesor], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_profesor',
            'id_clasa',
            'id_materie',
            'an_scolar',
        ],
    ]) ?>

</div>
