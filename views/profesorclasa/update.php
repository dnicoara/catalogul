<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profesorclasa */

$nume_profesor = Html::encode($model->getProfesori()->one()->nume);
$prenume_profesor = Html::encode($model->getProfesori()->one()->prenume);

$this->title = 'Editeaza catedra profesorului: ' . $nume_profesor . ' ' . $prenume_profesor;
$this->params['breadcrumbs'][] = ['label' => 'Catedra profesor', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profesorclasa-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'lista_profesori' => $lista_profesori,
        'lista_materii' => $lista_materii,
        'lista_clase' => $lista_clase,
        'id_prof' => $id_prof
    ]) ?>

</div>
