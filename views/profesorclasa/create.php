<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Profesorclasa */

$this->title = 'Adauga o clasa catedrei unui profesor ';
$this->params['breadcrumbs'][] = ['label' => 'Catedra profesor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesorclasa-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'lista_profesori' => $lista_profesori,
        'lista_materii' => $lista_materii,
        'lista_clase' => $lista_clase,
        'id_prof' => $id_prof

    ]) ?>

</div>
