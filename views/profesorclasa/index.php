<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesorclasaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Catedra profesor-editare';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catedraprof-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        ?>
        <?= Html::a('Adauga clasa la catedra', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', [$este_administratorul_conectat ? 'admin/administrator-edit' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= GridView::widget(['dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [['class' => 'yii\grid\SerialColumn'],
//            ['label' => 'Nume profesor', 'content' => function ($data) {
//                $nume = $data['nume'];
//                // $clasa = $data->getListaclase()->one()->Listaclase;
//                return $nume;
//            }, 'format' => 'text'],
            'nume',
            'prenume',
            'Clasa',
            'materia',
            'id_profesor',
            'id_clasa',
            'id_materie',
            'an_scolar',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ]]]); ?>
</div>
