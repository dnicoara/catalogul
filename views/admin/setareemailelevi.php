<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Setare email initial pentru elevii scolii';
$this->params['breadcrumbs'][] = $this->title;

//obtin valorile parametrilor globali definiti in config/params.php
$data = \Yii::$app->params['data'];
$semestrul = \Yii::$app->params['semestrul'];
?>
<div class="site-about">
    <h5><?= Html::encode('Aceasta operatie va seta toate e-mailurile elevilor din lista elevilor scolii la forma : numarMatricol@yahoo.com') ?></h5>
    <h5><?= Html::encode('Atentie : Vor fi setate doar acele e-mailuri care sunt empty !') ?></h5>

    <div class="setareEmailInitial-form">

        <?php $form = ActiveForm::begin([
            'action' => ['admin/set-mail-initial-elev'],
            'method' => 'get',
        ]);
        ?>

        <?= $form->field($model, 'id')->radioList(
            ['1' => 'Setare e-mail initial',
                '2' => 'Renunta la setare e-mail'],
            ['labelOptions' => ['style' => 'display:block']]

        )->label(''); ?>

        <div class="form-group">
            <?= Html::submitButton('Trimite', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
