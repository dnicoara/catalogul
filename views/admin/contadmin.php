<?php

use yii\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$nume = $prenume_administrator . ' ' . $nume_administrator;
$this->title = 'Cont administrator : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume;
?>
<div class="site-about">

    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
    }
    echo '</div>';

    ?>
    <br>

    <div class="infoelevi-index">

        <h4><?= Html::encode($this->title . ' ' . $nume) ?></h4>

        <br>

        <p>
            <?= Html::a('Revenire', ['site/index'], ['class' => 'btn btn-success']) ?>

        </p>


        <br><br>



        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'nume',
                'prenume',
                'email',
                //'parola',
                'data_inregistrarii',
                'data_actualizarii',


                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}  ',  //{delete}
                ]
            ],

        ]); ?>
        <br><br>

        <?= Html::a('Modifica adresa mail', ['admin/schimba-adresa-mail-form'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Modifica parola', ['admin/schimba-parola-form'], ['class' => 'btn btn-success']) ?>


    </div>
