<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
if (isset($_GET['user']))
    $utilizator = $_GET['user'];
else
    $utilizator = '';
$this->title = 'Logare' . ' ' . $utilizator;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h4><?= Html::encode($this->title) ?></h4>

    <p>Va rugam sa completati urmatoarele campuri pentru a va loga :</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'parola')->passwordInput() ?>

    <!--    --><?php //= $form->field($model, 'rememberMe', [
    //        'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
    //    ])->checkbox() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <br>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::a('Ati uitat parola ?', ['admin/trimite-parola-noua'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>


</div>
