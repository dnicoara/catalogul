<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Adminstrator', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Revenire in cont administrator', ['admin/cont-administrator'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nume',
            'prenume',
            'email:email',
            //'parola',
            'data_inregistrarii',
            'data_actualizarii',
        ],
    ]) ?>

</div>
