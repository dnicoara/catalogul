<?php
use yii\helpers\Html;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Meniul admin-transferare elevi';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
$request = Yii::$app->request;

// In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
// <div class="alert alert-danger" role="alert"></div>
if ($request->isPost) {
    echo '<div class="alert alert-success" role="alert">';
    if (isset($mesaj)) {
        echo Html::encode($mesaj);
    } elseif ($request->post('mesaj') !== null) {
        echo Html::encode($request->post('mesaj'));
    }
    echo '</div>';
}
if ($request->isGet && $request->get('mesaj') !== null) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Html::encode($request->get('mesaj'));
    echo '</div>';
}

?>

<div class="site-about">

    <div class="clasaAdmin-form">

        <?= Html::a('Transferare elevi in interiorul scolii', ['elevclasa/selectare-clasa-elevi'], ['class' => 'btn btn-primary']) ?>
        <br><br>
        <?= Html::a('Transferare elevi din alta scoala', ['elevclasa/transfer-elev-din-alta-scoala'], ['class' => 'btn btn-primary']) ?>
        <br><br>
        <?= Html::a('Transferare elevi in alta scoala', ['elevclasa/transfer-elev-in-alta-scoala'], ['class' => 'btn btn-primary']) ?>
        <br><br>
        <br><br>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>


    </div>

</div>
