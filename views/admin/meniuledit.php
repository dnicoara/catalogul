<?php
use yii\helpers\Html;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Meniul admin-editeaza';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
$request = Yii::$app->request;

// In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
// <div class="alert alert-danger" role="alert"></div>
if ($request->isPost) {
    echo '<div class="alert alert-success" role="alert">';
    if (isset($mesaj)) {
        echo Html::encode($mesaj);
    } elseif ($request->post('mesaj') !== null) {
        echo Html::encode($request->post('mesaj'));
    }
    echo '</div>';
}

if ($request->isGet && $request->get('mesaj') !== null) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Html::encode($request->get('mesaj'));
    echo '</div>';
}
?>

<div class="site-about">

    <div class="clasaAdmin-form">

        <?= Html::a('Editare catedra profesor', ['/profesorclasa/index'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Editare diriginti clase', ['/diriginti/index'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Editare lista elevilor scolii', ['elevi/index'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Editare informatii despre elevi', ['infoelevi/index'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Editare clase elevi', ['elevclasa/index'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Editare materii la clasa', ['materiiclasa/index'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Editare lista materii', ['/listamaterii/index'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Editare lista clase', ['/listaclase/index'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Iesire din meniu', ['/site/index'], ['class' => 'btn btn-success']) ?><br>


    </div>

</div>
