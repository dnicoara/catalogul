<?php
use yii\helpers\Html;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Meniul admin-adauga';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
$request = Yii::$app->request;

// In cazul in care primim o eroare de la 'POST', aceasta va fi afisate folosind:
// <div class="alert alert-danger" role="alert"></div>
if ($request->isPost) {
    echo '<div class="alert alert-success" role="alert">';
    if (isset($mesaj)) {
        echo Html::encode($mesaj);
    } elseif ($request->post('mesaj') !== null) {
        echo Html::encode($request->post('mesaj'));
    }
    echo '</div>';
}
if ($request->isGet && $request->get('mesaj') !== null) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Html::encode($request->get('mesaj'));
    echo '</div>';
}

?>

<div class="site-about">

    <div class="clasaAdmin-form">

        <?= Html::a('Adauga profesori', ['profesori/create'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Adauga diriginti', ['diriginti/create'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Adauga elevi', ['elevi/create'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Adauga directori', ['directori/create'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Adauga informatii despre elev', ['infoelevi/admin-adauga-info-elevi'], ['class' => 'btn btn-primary']) ?>
        <br><br>
        <?= Html::a('Adauga clase', ['/listaclase/create'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Adauga materii', ['/listamaterii/create'], ['class' => 'btn btn-primary']) ?><br><br>
        <?= Html::a('Adauga medii semestriale elevilor  transferati', ['/mediisemestriale/selectare-clasa-elevi'], ['class' => 'btn btn-primary']) ?>
        <br><br>

        <?= Html::a('Iesire din meniu', ['/site/index'], ['class' => 'btn btn-success']) ?><br>


    </div>

</div>
