<?php
use kartik\password\PasswordInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Introduceti noua parola';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">

    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
    }
    echo '</div>';

    ?>
    <br>


    <div class="changePassword-form">

        <?php $form = ActiveForm::begin([
            'action' => ['admin/schimba-parola'],
            'method' => 'get',
        ]);

        ?>
        <?php $model->parola = ''; ?>
        <?= $form->field($model, 'vechea_parola', ['labelOptions' => ['class' => 'col-sm-3 col-md-3 col-lg-3']])->widget(PasswordInput::classname(), [])->label('Introduceti vechea parola') ?>
        <?= $form->field($model, 'parola', ['labelOptions' => ['class' => 'col-sm-3 col-md-3 col-lg-3']])->widget(PasswordInput::classname(), [])->label('Introduceti noua parola') ?>
        <?= $form->field($model, 'confirm_parola', ['labelOptions' => ['class' => 'col-sm-3 col-md-3 col-lg-3']])->widget(PasswordInput::classname(), [])->label('Confirmati parola') ?>


        <?= Html::activeHiddenInput($model, 'id_administrator', ['value' => $model->id]) ?>

        <div class="form-group">
            <?= Html::submitButton('Modifica parola', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        <?= Html::a('Revenire in cont administrator', ['admin/cont-administrator'], ['class' => 'btn btn-success']) ?>

    </div>

</div>
