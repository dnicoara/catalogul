<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nume')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'prenume')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?php //= $form->field($model, 'parola')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?php //= $form->field($model, 'data_inregistrarii')->textInput(['style' => 'width:400px']) ?>

    <?php //= $form->field($model, 'data_actualizarii')->textInput(['style' => 'width:400px']) ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
