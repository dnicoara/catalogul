<?php
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$clasa = \app\models\Listaclase::getNumeClasa($clasa_selectata);

$this->title = 'Lista elevilor din clasa selectata : ' . $clasa;
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<br>
<div class="elevi-afisareclasaselectata">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Revenire la selectii', ['mediisemestriale/selectare-clasa-elevi'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Revenire la pagina principala', ['admin/administrator-adauga'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nr_matricol',
        'nume',
        'prenume',
        'listaclase.Clasa',
//        'elevi.email',
//        'infoelevi.telefon',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nr_matricol',
            'nume',
            'prenume',
            'listaclase.Clasa',
//            'elevi.email',
//            'infoelevi.telefon',
//            'infoelevi.buletin_seria',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{transfer}',
                'buttons' => [
                    'transfer' => function ($url, $model, $id) use ($clasa_selectata) {
                        $url = Yii::$app->urlManager->createUrl(['mediisemestriale/adaug-medii-sem-elevi-transferati', 'id_elev' => $id, 'id_clasa' => $clasa_selectata]);
                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['title' => Yii::t('app', 'Introduceti media')]);
                    },

                ]
            ],
        ],
    ]); ?>


</div>
