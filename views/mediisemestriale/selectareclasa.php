<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Selectare clasa in care se afla elevul : ';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<br>

<div class="site-about">

    <div class="selectareclase-form">

        <?php $form = ActiveForm::begin([
            'action' => ['mediisemestriale/selectare-clasa-elevi'],
            'method' => 'get',
        ]);
        ?>
        <?php
        echo $form->field($model, 'id')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $lista_claselor,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza clasa ...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza clasa');

        ?>
        <div class="form-group">
            <?= Html::submitButton('Continua...', ['class' => 'btn btn-success']) ?><br><br>
            <?= Html::a('Revenire la pagina principala', ['admin/administrator-adauga'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
