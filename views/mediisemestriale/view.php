<?php

use app\models\Elevi;
use app\models\Listamaterii;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mediisemestriale */
$materia = Listamaterii::getNumeMaterie($model->id_materie);
$nume_elev = Elevi::getNumeElev($model->nr_matricol) . ' ' . Elevi::getPrenumeElev($model->nr_matricol);
$this->title = 'Medii semestriale';
$this->params['breadcrumbs'][] = ['label' => 'Medii semestriale', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mediisemestriale-view">

    <h4><?= Html::encode('Media semestriala a elevului  ' . $nume_elev . '  la  ' . $materia) ?></h4>
    <br>
    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Revenire la clasa elevi', ['elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-success']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'nr_matricol',
            'id_materie',
            'id_clasa',
            'an_scolar',
            'media',
            'sem',
        ],
    ]) ?>

</div>
