<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mediisemestriale */
/* @var $form yii\widgets\ActiveForm */
$semestrul = \Yii::$app->params['semestrul'];
$anul_scolar = \Yii::$app->params['anul_scolar'];
$luna = \Yii::$app->params['luna'];

//$id_clasa=Yii::$app->session['id_clasa'];

?>

<div class="mediisemestriale-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nr_matricol')->textInput(['maxlength' => true, 'value' => $nr_matricol, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'id_materie')->textInput(['maxlength' => true, 'value' => $id_materie, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'id_clasa')->textInput(['maxlength' => true, 'value' => $id_clasa, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true, 'value' => $anul_scolar, 'readonly' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'media')->widget(Select2::classname(), [
        'data' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
        'language' => 'ro',
        'options' => ['value' => $model->isNewRecord ? 0 : $model->media, 'placeholder' => 'Selecteaza media ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Media'); ?>

    <?php //= $form->field($model, 'media')->textInput(['style' => 'width:400px', 'value' => $model->isNewRecord ? '' : $model->media]) ?>

    <?= $form->field($model, 'sem')->widget(Select2::classname(), [
        'data' => $semestrul,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza semestrul ...', 'value' => $model->isNewRecord ? ($luna >= 2 && $luna <= 6) ? 'II' : 'I' : $model->sem],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza semestrul'); ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['elevi/afisare-clasa-diriginte'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
