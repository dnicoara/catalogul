<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediisemestrialeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Medii semestriale';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mediisemestriale-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Introduceti mediisemestriale', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nr_matricol',
            'id_materie',
            'id_clasa',
            'an_scolar',
            //'media',
            //'sem',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
