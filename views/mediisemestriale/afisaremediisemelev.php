<?php
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediisemestrialeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mediile semestriale ale elevului : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev;

// retin numele si id-ul clasei selectate
// Yii::$app->session['nume_clasa'] = $nume_clasa;
// Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">

	<h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev) ?></h4>

	<br>

	<p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa-diriginte', 'id_clasa' => $id_clasa], ['class' => 'btn btn-primary']) ?>


    </p>
	<br> <br>

    <?= GridView::widget(['dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [['class' => 'yii\grid\SerialColumn'],
            ['label' => 'Numar matricol', 'content' => function ($data) {
                $nr_matricol = $data['nr_matricol'];
                // $clasa = $data->getListaclase()->one()->Listaclase;
                return $nr_matricol;
            }, 'format' => 'text'],
            ['label' => 'Materia', 'content' => function ($data) {
                $materia = $data['materia'];
                // $clasa = $data->getListaclase()->one()->Listaclase;
                return $materia;
            }, 'format' => 'text'],
            ['label' => 'Media semestriala', 'content' => function ($data) {
                $nota = $data['media'];
                // $clasa = $data->getListaclase()->one()->Listaclase;
                return $nota;
            }, 'format' => 'text'],
            ['label' => 'An scolar', 'content' => function ($data) {
                $anul = $data['an_scolar'];
                // $clasa = $data->getListaclase()->one()->Listaclase;
                return $anul;
            }, 'format' => 'text'],

            // 'email',
            // ['class' => 'yii\grid\ActionColumn',
            //    'template' => '{view} {update} {delete} ', ]

        ]]); ?>

    <br><br>

    <?php

    if (isset($_GET['materii_cu_note_insuficiente']) && count($_GET['materii_cu_note_insuficiente']) > 0) {
        echo 'Lista materiilor la care elevul nu are suficiente note pentru a i se incheia media semestriala :  <br><br>';
        $materii = $_GET['materii_cu_note_insuficiente'];

        for ($i = 0; $i < count($materii); $i++) {

            echo '<b>' . ($i + 1) . ')  ' . \app\models\Listamaterii::getNumeMaterie($materii[$i]) . '  ;  </b>';
            echo "</br>";
        }

    }
    echo "</br>";
    if (isset($_GET['materii_cu_module']) && count($_GET['materii_cu_module']) > 0) {
        echo 'Lista materiilor cu module la care se incheie doar media semestriala pe semestrul II :  <br><br>';
        $materii_module = $_GET['materii_cu_module'];

        for ($i = 0; $i < count($materii_module); $i++) {

            echo '<b>' . ($i + 1) . ')  ' . \app\models\Listamaterii::getNumeMaterie($materii_module[$i]) . '  ;  </b>';
            echo "</br>";
        }

    }
    ?>

</div>
