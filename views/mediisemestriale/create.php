<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mediisemestriale */

$this->title = 'Stabilesc media la purtare : ' . $nume_elev . '  ' . $prenume_elev;
$this->params['breadcrumbs'][] = ['label' => 'Medii semestriale', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mediisemestriale-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'nr_matricol' => $nr_matricol,
        'id_materie' => $id_materie,
        'id_clasa' => $id_clasa,
        'anul_scolar' => $anul_scolar,
        'nume_elev' => $nume_elev,
        'prenume_elev' => $prenume_elev,
        'materii_profesor' => $materii_profesor
    ]) ?>

</div>
