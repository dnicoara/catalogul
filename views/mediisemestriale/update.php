<?php

use app\models\Elevi;
use yii\helpers\Html;

$nume_elev = Elevi::getNumeElev($nr_matricol) . ' ' . Elevi::getPrenumeElev($nr_matricol);
/* @var $this yii\web\View */
/* @var $model app\models\Mediisemestriale */

$this->title = 'Editeaza media semestriala a elevului : ' . $nume_elev;
$this->params['breadcrumbs'][] = ['label' => 'Medii semestriale', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza';
?>
<div class="mediisemestriale-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'nr_matricol' => $nr_matricol,
        'id_materie' => $id_materie,
        'id_clasa' => $id_clasa,
    ]) ?>

</div>
