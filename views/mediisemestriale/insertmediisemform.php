<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$semestrul = \Yii::$app->params['semestrul'];
?>

<?php
$this->title = 'Introducere medii semestriale (medii examen diferenta) pentru elevii transferati  : ';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
if (Yii::$app->session->hasFlash('success')) {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error')) {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<br>

<div class="site-about">

    <div class="selectareElevTransferat-form">

        <?php $form = ActiveForm::begin([
            'action' => ['mediisemestriale/adaug-medii-sem-elevi-transferati'],
            'method' => 'get',
        ]);
        ?>

        <?php
        echo $form->field($model1, 'id')->widget(Select2::classname(), [
            'name' => 'Clasa',
            'data' => $materiile_clasei,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza materia...'],
            'pluginOptions' => [
                'allowClear' => true,
                //'multiple' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza materia');

        ?>

        <?= $form->field($model2, 'media')->widget(Select2::classname(), [
            'data' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
            'language' => 'ro',
            'options' => ['value' => 0, 'placeholder' => 'Selecteaza media semestriala ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Media semestriala'); ?>

        <?= $form->field($model2, 'sem')->widget(Select2::classname(), [
            'data' => $semestrul,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza semestrul ...', 'value' => 'I'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza semestrul'); ?>

        <?= $form->field($model2, 'nr_matricol')->hiddenInput(['value' => $nr_matricol])->label(false); ?>
        <?= $form->field($model2, 'id_clasa')->hiddenInput(['value' => $id_clasa])->label(false); ?>




        <div class="form-group">
            <?= Html::submitButton('Continua...', ['class' => 'btn btn-success']) ?><br><br>
            <?= Html::a('Revenire la pagina principala', ['admin/administrator-transfer-elevi'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
