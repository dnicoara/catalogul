<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Infosecretari */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Infosecretaris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infosecretari-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        $este_secretarul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'secretar' :
            false;
        ?>
        <?= Html::a('Revenire', [$este_secretarul_conectat ? 'infosecretari/cont-secretar' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fk_secretar',
            'sex',
            'studii',
            'telefon',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',
            'buletin_seria',
            'buletin_nr',
            'vechime',
            'data_actualizarii',
            'data_inregistrarii',
        ],
    ]) ?>

</div>
