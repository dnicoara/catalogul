<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InfosecretariSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Infosecretaris';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infosecretari-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Infosecretari', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fk_secretar',
            'sex',
            'studii',
            'telefon',
            // 'localitate',
            // 'strada',
            // 'numar',
            // 'bloc',
            // 'scara',
            // 'apartament',
            // 'judet',
            // 'buletin_seria',
            // 'buletin_nr',
            // 'vechime',
            // 'data_actualizarii',
            // 'data_inregistrarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
