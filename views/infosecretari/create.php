<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Infosecretari */

$this->title = 'Adauga secretar';
$this->params['breadcrumbs'][] = ['label' => 'Infosecretari', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infosecretari-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
