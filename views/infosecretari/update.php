<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Infosecretari */
if (isset(Yii::$app->session['user']->nume) && isset(Yii::$app->session['user']->prenume))
    $nume_secretar = Yii::$app->session['user']->nume . ' ' . Yii::$app->session['user']->prenume;
else
    $nume_secretar = '';

$this->title = 'Editare informatii secretar: ' . $nume_secretar;
$this->params['breadcrumbs'][] = ['label' => 'Infosecretari', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza';
?>
<div class="infosecretari-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
