<?php

use yii\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$nume = $prenume_director . ' ' . $nume_director;
$this->title = 'Cont director : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume;

?>
<div class="site-about">

    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>

    <div class="infodirectori-index">

        <h4><?= Html::encode($this->title . ' ' . $nume) ?></h4>

        <br>

        <p>
            <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>

        </p>


        <br><br>



        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'directori.email',
                'telefon',
                'studii',
                'buletin_seria',
                'buletin_nr',
                'localitate',
                'strada',
                'numar',
                'bloc',
                'scara',
                'apartament',
                'judet',
                'grad_didactic',
                'vechime',

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                ]
            ],

        ]); ?>
        <br><br>

        <?= Html::a('Modifica adresa mail', ['directori/schimba-adresa-mail-form'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Modifica parola', ['directori/schimba-parola-form'], ['class' => 'btn btn-success']) ?>


    </div>
