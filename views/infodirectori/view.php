<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Infodirectori */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Infodirectoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infodirectori-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;
        ?>
        <?= Html::a('Revenire', [$este_directorul_conectat ? 'infodirectori/cont-director' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'fk_director',
            'sex',
            'studii',
            'telefon',
            'localitate',
            'strada',
            'numar',
            'bloc',
            'scara',
            'apartament',
            'judet',
            'buletin_seria',
            'buletin_nr',
            'grad_didactic',
            'vechime',
            'data_actualizarii',
            'data_inregistrarii',
        ],
    ]) ?>

</div>
