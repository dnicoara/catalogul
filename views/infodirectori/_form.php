<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Infodirectori */
/* @var $form yii\widgets\ActiveForm */
$lista_judete = \Yii::$app->params['judete'];

?>

<div class="infodirectori-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sex')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'studii')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'telefon')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'localitate')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'strada')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'numar')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'bloc')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'scara')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'apartament')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?php //= $form->field($model, 'judet')->textInput(['maxlength' => true,'style' => 'width:400px']) ?>

    <?= $form->field($model, 'judet')->widget(Select2::classname(), [
        'data' => $lista_judete,
        'language' => 'ro',
        'options' => ['placeholder' => 'Selecteaza judetul ...'],
        //'disabled' => $model->isNewRecord ? false : true,
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '400px'
        ],
    ])->label('Selecteaza judetul'); ?>

    <?= $form->field($model, 'buletin_seria')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'buletin_nr')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'grad_didactic')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'vechime')->textInput(['style' => 'width:400px']) ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['infodirectori/cont-director'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
