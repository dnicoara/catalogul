<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Infodirectori */

$this->title = 'Adauga director';
$this->params['breadcrumbs'][] = ['label' => 'Infodirectori', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infodirectori-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
