<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\assets\MyAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;

require "ProtectiaDatelor.html";
/* @var $this \yii\web\View */
/* @var $content string */
MyAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

</head>

<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    $numele_institutiei = 'C.N."I.C.Brătianu"-Hațeg : Catalog digital';
    NavBar::begin([
        'brandLabel' => $numele_institutiei,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    ?>
    <?php
    if (isset(Yii::$app->session['numar_notificari_nerezolvate']))
        $nr_notificari_dirig_nerezolvate = Yii::$app->session['numar_notificari_nerezolvate'];
    else
        $nr_notificari_dirig_nerezolvate = 0;

    if (isset(Yii::$app->session['id_clasa']))
        $id_clasa = Yii::$app->session['id_clasa'];
    else
        $id_clasa = 0;

    ?>

    <?php
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-center'],
        'items' => [
            ['label' => 'Despre', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
        ],
    ]);

    $este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'profesor' :
                                        false;

    $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'diriginte' :
                                        false;

    $este_elevul_conectat = isset(Yii::$app->session['user']->role) ?
                                Yii::$app->session['user']->role == 'elev' :
                                false;

    $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'director' :
                                    false;

    $este_secretarul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'secretar' :
                                    false;

    $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'admin' :
                                        false;

    if (
        (!$este_profesorul_conectat) && 
        (!$este_directorul_conectat) && 
        (!$este_dirigintele_conectat) && 
        (!$este_administratorul_conectat) && 
        (!$este_elevul_conectat) && 
        (!$este_secretarul_conectat)
    ) {

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Conectare utilizatori',
                    'items' => [
                        ['label' => 'Conectare profesor ', 'url' => ['/site/login', 'user' => 'profesor']],
                        ['label' => 'Conectare director ', 'url' => ['/site/login', 'user' => 'director']],
                        ['label' => 'Conectare diriginte ', 'url' => ['/site/login', 'user' => 'diriginte']],
                        ['label' => 'Conectare secretar ', 'url' => ['/site/login', 'user' => 'secretar']],
                        '<li class="divider"></li>',
                        ['label' => 'Conectare elev/parinte ', 'url' => ['/site/login', 'user' => 'elev']],
                        '<li class="divider"></li>',
                        ['label' => 'Conectare administrator ', 'url' => ['/site/login', 'user' => 'administrator']],

                    ],
                ],
            ],
        ]);

    }

    if ($este_profesorul_conectat) {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Profesor (' . Yii::$app->session['user']->nume  . ')',
                    'items' => [
                        ['label' => 'Clasele profesorului ', 'url' => ['/listaclase/clase-profesor']],
                        '<li class="divider"></li>',
                        ['label' => 'Cont profesor ', 'url' => ['/infoprofesori/cont-profesor']],
                        '<li class="divider"></li>',
                        ['label' => 'Deconectare', 'url' => ['/site/logout']
                        ],
                    ],
                ],
            ],

        ]);
    }

    if ($este_directorul_conectat) {

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Director (' . Yii::$app->session['user']->nume . ')',
                    'items' => [
                        ['label' => 'Lista profesorilor scolii', 'url' => ['/profesori/index']],
                        '<li class="divider"></li>',
                        ['label' => 'Afisare clasa elevi ', 'url' => ['/elevi/selectare-clase', 'op' => 'infoelevi']],
                        ['label' => 'Afisare note elev ', 'url' => ['/directori/afisare-note-elev']],
                        ['label' => 'Situatia absentelor clasei', 'url' => ['/elevi/selectare-optiuni-absente', 'op' => 'absente']],
                        ['label' => 'Situatia absentelor elevului', 'url' => ['/directori/afisare-absente-elev']],
                        '<li class="divider"></li>',
                        ['label' => 'Cont director ', 'url' => ['/infodirectori/cont-director']],
                        '<li class="divider"></li>',
                        ['label' => 'Deconectare', 'url' => ['/site/logout']
                        ],
                    ],
                ],
            ],

        ]);
    }
    if ($este_secretarul_conectat) {

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Secretar (' . Yii::$app->session['user']->nume . ')',
                    'items' => [
                        ['label' => 'Afisare clasa elevi ', 'url' => ['/elevi/selectare-clase', 'op' => 'infoelevi']],
                        ['label' => 'Afisare elevi navetisti ', 'url' => ['/elevi/selectare-clase', 'op' => 'navetisti']],
                        ['label' => 'Afisare varsta elevi ', 'url' => ['/elevi/selectare-clase', 'op' => 'varstaelevi']],
                        '<li class="divider"></li>',
                        ['label' => 'Situatie scolara-semestriala', 'url' => ['/secretariat/statistici-situatia-scolara']],
                        ['label' => 'Afisarea elevilor corigenti ', 'url' => ['/elevi/selectare-clase-semestrul', 'op' => 'corigenti']],
                        ['label' => 'Elevi-note scazute la purtare ', 'url' => ['/elevi/selectare-clase-semestrul', 'op' => 'purtare']],
                        ['label' => 'Elevi-situatia absentelor', 'url' => ['/elevi/selectare-optiuni-absente', 'op' => 'absente']],
                        ['label' => 'Elevi-lista mediilor generale', 'url' => ['/elevi/selectare-clase', 'op' => 'medii_gen']],
                        ['label' => 'Elevi-lista medii clasa pe obiecte', 'url' => ['/secretariat/tabel-cu-medii-generale-elevi']],
                        '<li class="divider"></li>',
                        ['label' => 'Extrag date din arhiva', 'url' => ['/secretariat/afiseaza-date-din-arhiva', 'sursa' => 'arhiva']],
                        '<li class="divider"></li>',
                        ['label' => 'Cont secretar ', 'url' => ['/infosecretari/cont-secretar']],
                        '<li class="divider"></li>',
                        ['label' => 'Deconectare', 'url' => ['/site/logout']
                        ],
                    ],
                ],
            ],

        ]);
    }

    if ($este_dirigintele_conectat) {

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Diriginte (' . Yii::$app->session['user']->nume . ')',
                    'items' => [
                        //['label' => 'Vizualizare clasa elevi', 'url' => ['/diriginti/afisare-clasa']],
                        ['label' => 'Vizualizare clasa elevi', 'url' => ['/elevi/afisare-clasa-diriginte']],
                        ['label' => 'Adauga informatii despre elev', 'url' => ['infoelevi/diriginte-adauga-info-elevi']],
                        ['label' => 'Editeaza informatii despre elevi', 'url' => ['/infoelevi/index']],
                        ['label' => 'Adauga elev in clasa', 'url' => ['/elevi/adauga-elev-in-clasa']],

                        '<li class="divider"></li>',
                        ['label' => 'Calculare medii generale (pe obiecte)', 'url' => ['/diriginti/calculare-medii-generale-clasa-elevi']],
                        '<li class="divider"></li>',
                        ['label' => 'Absente clasa - numeric ', 'url' => ['/diriginti/statistici-absente-clasa']],
                        ['label' => 'Absente clasa - lista ', 'url' => ['/diriginti/statistici-absente']],
                        ['label' => 'Situatie scolara-statistici', 'url' => ['/diriginti/statistici-situatia-scolara']],
                        ['label' => 'Date elevi- statistici', 'url' => ['/infoelevi/statistici-date-elevi']],
                        ['label' => 'Inserare medii semestriale - manual', 'url' => ['/diriginti/insert-medii-semestriale-elev']],
                        '<li class="divider"></li>',
                        ['label' => 'Vizulizare notificari elevi (' . $nr_notificari_dirig_nerezolvate . ')', 'url' => ['/notificaridiriginte/index'], 'options' => ['style' => $nr_notificari_dirig_nerezolvate > 0 ? 'background-color: #F00;' : '']],
                        '<li class="divider"></li>',
                        ['label' => 'Trimite e-mail elevilor clasei', 'url' => ['/notificaridiriginte/trimit-email-elevi']],
                        '<li class="divider"></li>',
                        ['label' => 'Cont diriginte ', 'url' => ['/infodiriginti/cont-diriginte']],
                        '<li class="divider"></li>',
                        ['label' => 'Deconectare', 'url' => ['/site/logout']
                        ],
                    ],
                ],
            ],

        ]);
    }

    if ($este_elevul_conectat) {

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Elev (' . Yii::$app->session['user']->nume . ')',
                    'items' => [
                        ['label' => 'Situatia scolara ', 'url' => ['/elevi/situatia-scolara-elev']],
                        '<li class="divider"></li>',
                        ['label' => 'Posteaza mesaj pentru diriginte', 'url' => ['/notificaridiriginte/create']],
                        ['label' => 'Afiseaza postarile pentru diriginte', 'url' => ['/notificaridiriginte/afisez-notificarile-elevului']],
                        '<li class="divider"></li>',
                        ['label' => 'Cont elev', 'url' => ['/infoelevi/cont-elev']],
                        '<li class="divider"></li>',
                        ['label' => 'Deconectare', 'url' => ['/site/logout']
                        ],
                    ],
                ],
            ],

        ]);
    }

    if ($este_administratorul_conectat) {

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Administrator (' . Yii::$app->session['user']->nume . ')',
                    'items' => [
                        ['label' => 'Lista profesorilor', 'url' => ['/profesori/index']],
                        '<li class="divider"></li>',
                        ['label' => 'Meniul adauga... ', 'url' => ['/admin/administrator-adauga']],
                        '<li class="divider"></li>',
                        ['label' => 'Formare catedra profesor ', 'url' => ['/profesorclasa/selectare']],
                        ['label' => 'Formare clase elevi ', 'url' => ['elevclasa/selectare']],
                        ['label' => 'Sterge elevii din clasa ', 'url' => ['elevclasa/sterge-elevii-clasei']],
                        ['label' => 'Adaugare materii la clasa ', 'url' => ['materiiclasa/edit-materiiclase']],
                        ['label' => 'Meniul transferare elevi... ', 'url' => ['/admin/administrator-transfer-elevi']],

                        '<li class="divider"></li>',
                        ['label' => 'Meniul editare... ', 'url' => ['/admin/administrator-edit']],
                        ['label' => 'Setare e-mail initial elevi ', 'url' => ['/admin/set-mail-initial-elev']],
                        '<li class="divider"></li>',
                        ['label' => 'Migrare clase elevi', 'url' => ['/elevclasa/migrare-clasa']],
                        '<li class="divider"></li>',
                        ['label' => 'Cont administrator', 'url' => ['/admin/cont-administrator']],
                        '<li class="divider"></li>',
                        ['label' => 'Deconectare', 'url' => ['/site/logout']
                        ],
                    ],
                ],
            ],

        ]);
    }


    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="navbar navbar-inverse navbar-fixed-bottom">
    <div class="container">
        <p class="navbar-text pull-left">&copy; <?php echo $numele_institutiei; ?> <?= date('Y') ?></p>
    </div>
</footer>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
