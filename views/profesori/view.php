<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Profesori */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Profesori', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesori-view">

    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>

    <p>
        <?= Html::a('Editeaza inregistrarea', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge inregistrarea', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Sunteti sigur ca doriti stergerea inregistrarii ?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        ?>
        <?= Html::a('Revenire la pagina principala', [$este_administratorul_conectat ? 'admin/administrator-adauga' : 'profesori/index'], ['class' => 'btn btn-success']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nume',
            'prenume',
            'email:email',
            //'parola',
            'data_inregistrarii',
            'data_actualizarii',
        ],
    ]) ?>

</div>
