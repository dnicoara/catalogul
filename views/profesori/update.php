<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profesori */

$this->title = 'Editeaza lista profesorilor';
$this->params['breadcrumbs'][] = ['label' => 'Profesori', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profesori-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('actualizeaza_form', [
        'model' => $model,
    ]) ?>

</div>
