<?php

use app\models\Elevi;
use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalognoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
if ($semestrul == 'I' || $semestrul == 'II')
    $this->title = 'Mediile semestriale ale  elevilor clasei : ' . $nume_clasa;

if ($semestrul == 'MG')
    $this->title = 'Mediile anuale ale  elevilor clasei : ' . $nume_clasa;

$this->params['breadcrumbs'][] = $this->title;


?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['profesori/selectez-materia-profesorului'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa'], ['class' => 'btn btn-success']) ?>

    </p>
    <br>
    <?php if (isset($mesaj))
        echo '<span style="color:red "><h5>' . Html::encode($mesaj) . '</h5></span>';
    ?>
    <?php if (Yii::$app->request->get('mesaj'))
        echo '<span style="color:red "><h5>' . Html::encode(Yii::$app->request->get('mesaj')) . '</h5></span>';
    ?>
    <br><br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        'nr_matricol',
        'nume',
        'prenume',
        'materia',
        'nota',
        'sem',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            'nume',
            'prenume',
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Media',
                'content' => function ($data) {
                    $media = $data['media'];
                    return $media;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Semestrul',
                'attribute' => 'sem',
                'visible' => ($semestrul == 'I' || $semestrul == 'II'),
            ],
            //'sem'

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} {delete} ',
//            ]

        ],

    ]); ?>


</div>
