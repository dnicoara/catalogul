<?php

use exocet\BootstrapMD\widgets\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista profesorilor scolii';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesori-index">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>

    <p>
        <?= Html::a('Adauga profesor', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nume',
            'prenume',
            'email:email',
            ['class' => 'yii\grid\ActionColumn',
                'template' => /*'{create} {view}*/
                    '{info} {catedra} {update} {delete}',
                'buttons' => [
                    'info' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['infoprofesori/view', 'id' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, ['title' => Yii::t('app', 'Informatii despre profesor')]);
                    },
                    'catedra' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['directori/catedra-profesor', 'id_prof' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-calendar"></span>', $url, ['title' => Yii::t('app', 'Catedra profesorului')]);
                    },
                ]
            ],
        ],
    ]); ?>
</div>
