<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Afisare mediilor elevilor clasei la obiectul selectat';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>

    <div class="classProfesori-form">


        <?php $form = ActiveForm::begin([
            'action' => ['profesori/profesor-afisez-medii-clasa-elevi'],
            'method' => 'get',
        ]);
        ?>
        <?php

        echo $form->field($modelmaterii, 'id')->widget(Select2::classname(), [
            //'data' => ArrayHelper::map(CartiCustom::CartiDisponibile(), 'id_carte', 'titlu_carte'),
            'name' => 'profesorul',
            'data' => $materii_predate,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza materia ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza materia');

        ?>

        <?= $form->field($modelmedii, 'sem')->widget(Select2::classname(), [
            'data' => ['I' => 'Medii semestrul I', 'II' => 'Medii semestrul II', 'MG' => 'Medii anuale'],
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza optiunea ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px',
            ],
        ])->label('Selecteaza optiunea de afisare'); ?>


        <div class="form-group">
            <?= Html::submitButton('Afiseaza', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
