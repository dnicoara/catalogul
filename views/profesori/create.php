<?php



/* @var $this yii\web\View */
/* @var $model app\models\Profesori */

$this->title = 'Adauga profesor';
$this->params['breadcrumbs'][] = ['label' => 'Profesori', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesori-create">


    <?= $this->render('creaza_form', [
        'model' => $model,

    ]) ?>

</div>
