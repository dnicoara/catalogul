<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Profesori */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profesori-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nume')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'prenume')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'style' => 'width:400px']) ?>


    <?php
    $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
        Yii::$app->session['user']->role == 'director' :
        false;
    ?>
    <div class="form-group">
        <?= Html::submitButton('Actualizeaza profesor', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', [$este_directorul_conectat ? 'profesori/index' : 'admin/administrator-adauga'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
