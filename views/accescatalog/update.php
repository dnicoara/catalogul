<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Accescatalog */

$this->title = 'Update Accescatalog: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Accescatalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="accescatalog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
