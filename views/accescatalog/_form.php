<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Accescatalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accescatalog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'utilizator')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mesaj')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data_inregistrarii')->textInput() ?>

    <?= $form->field($model, 'data_actualizarii')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
