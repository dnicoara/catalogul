<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Accescatalog */

$this->title = 'Create Accescatalog';
$this->params['breadcrumbs'][] = ['label' => 'Accescatalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accescatalog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
