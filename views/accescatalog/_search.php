<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AccescatalogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accescatalog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ip') ?>

    <?= $form->field($model, 'utilizator') ?>

    <?= $form->field($model, 'mesaj') ?>

    <?php // echo $form->field($model, 'data_inregistrarii') ?>

    <?php // echo $form->field($model, 'data_actualizarii') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
