<?php
use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogabsenteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
switch ($optiunea) {
    case 1:
        if ($semestrul == 'An')
            $text = 'Lista mediilor generale pe obiecte';

        else
            $text = 'Lista mediilor semestriale pe obiecte (sem ' . $semestrul . ')';

        break;
    case 2:
        if ($semestrul == 'An') {
            $text = 'Lista mediilor generale anuale (elevii promovati)';
            $opt = 'anuale';
        }

        else {
            $text = 'Lista mediilor semestriale (sem ' . $semestrul . ' , elevii promovati) ';
            $opt = 'semestriale';
        }

        break;
    case 3:
        if ($semestrul == 'An')
            $text = 'Lista elevilor corigenti anual';
        else
            $text = 'Lista elevilor corigenti pe semestrul ' . $semestrul;
        break;
}

$this->title = ' ' . $text . ' - ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['diriginti/statistici-situatia-scolara'], ['class' => 'btn btn-primary']) ?>

    </p>
    <br><br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        'nume',
        'prenume',
        $optiunea == 1 || $optiunea == 3 ? 'materia' : 'col vida',
        $optiunea == 2 ? 'Media_semestriala' : 'media',


        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu

    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'showFooter' => TRUE,
        'layout' => '{items} {pager} {summary}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nr_matricol',
            [
                'label' => 'Nume elev',
                'content' => function ($data) {
                    $nume = $data['nume'];
                    return $nume;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Prenume elev',
                'content' => function ($data) {
                    $absenta = $data['prenume'];
                    return $absenta;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'visible' => ($materiavisible == 1),
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],
            [
                'label' => $semestrul == 'An' ? 'Media generala' : 'Media semestriala',
                'attribute' => 'media',
                'visible' => ($mediavisible == 1)
            ],
            [
                'label' => $semestrul == 'An' ? 'Media generala' : 'Media semestriala',
                'attribute' => 'Media_semestriala',
                'visible' => ($mediasemvisible == 1),
            ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

        ],

    ]); ?>
    <br><br>
    <?php
    if (isset($medii_pe_categorii)) {
        if ($semestrul == 'An')
            echo '<b>Mediile anuale generale pe categorii</b><br><br><br>';
        else
            echo '<b>Mediile semestriale pe categorii</b><br><br><br>';


        //echo'Numar elevi corigenti : '.$medii_pe_categorii[0][0].' din care fete : '.$medii_pe_categorii[0][1];
        //echo'<br><br>';
        echo 'Numar medii ' . $opt . ' intre [5-7) : ' . $medii_pe_categorii[1][0] . ' din care fete : ' . $medii_pe_categorii[1][1];
        echo '<br><br>';
        echo 'Numar medii ' . $opt . ' intre [7-9) : ' . $medii_pe_categorii[2][0] . ' din care fete : ' . $medii_pe_categorii[2][1];
        echo '<br><br>';
        echo 'Numar medii ' . $opt . ' intre [9-10] : ' . $medii_pe_categorii[3][0] . ' din care fete : ' . $medii_pe_categorii[3][1];
        echo '<br><br>';

    }

    ?>

</div>
