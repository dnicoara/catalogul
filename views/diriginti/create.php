<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Diriginti */

$this->title = 'Adauga diriginte';
$this->params['breadcrumbs'][] = ['label' => 'Diriginti', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diriginti-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'lista_claselor' => $lista_claselor

    ]) ?>

</div>
