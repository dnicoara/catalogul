<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogabsenteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Absentele clasei : ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . '  intre  ' . ' ' . $data1 . '  si  ' . ' ' . $data2) . ' ( semestrul  ' . $semestrul . ' )' ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['diriginti/statistici-absente'], ['class' => 'btn btn-primary']) ?>


    </p>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'showFooter' => ($optiunea != 4 && $optiunea != 1 ? TRUE : FALSE),
        'layout' => '{items} {pager} {summary}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Absenta',
                'visible' => $optiunea == 4 ? false : true,
                'content' => function ($data) {
                    $absenta = $data['absente'];
                    return $absenta;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Numar absente',
                'attribute' => 'motivata',
                'footer' => \app\models\Catalogabsente::getTotalAbsente($dataProvider),
            ],


//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

        ],

    ]); ?>

    <?php
    switch ($optiunea) {
        case 1:
            echo '<br><br>';
            echo '<b>Numarul total de absente = ' . $nr_total_abs;
            echo '<br>';
            echo 'Numar absente motivate = ' . $nr_abs_motivate;
            echo '<br>';
            echo 'Numar absente nemotivate = ' . $nr_abs_nemotivate;
            echo '</b>';

            break;
        case 2:
            echo '<br><br>';
            echo '<b>Numar absente nemotivate = ' . $nr_total_abs;
            echo '</b>';
            break;
        case 3:
            echo '<br><br>';
            echo '<b>Numar total absente = ' . $nr_total_abs;
            echo '</b>';
            break;
        case 4:
            echo '<br><br>';
            echo '<b>Numar total absente = ' . ($nr_abs_motivate + $nr_abs_nemotivate);
            echo '</b>';
            echo '<br><b>Numar absente motivate = ' . $nr_abs_motivate;
            echo '</b>';
            echo '<br><b>Numar absente nemotivate = ' . $nr_abs_nemotivate;

            break;


    }

    ?>
</div>
