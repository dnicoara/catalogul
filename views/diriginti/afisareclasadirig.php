<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CititoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$nume_diriginte=Yii::$app->session['user']->nume.' '.Yii::$app->session['user']->id;

$this->title = 'Elevii clasei : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_clasa.'  (diriginte: '.$nume_diriginte.')';

//retin numele si id-ul clasei selectate
Yii::$app->session['nume_clasa'] = $nume_clasa;
//Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>

    <h4><?= Html::encode($this->title . ' ' . $nume_clasa.'  (diriginte: '.$nume_diriginte.')') ?></h4>

    <br>

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>

    </p>
    <br><br>

    <?php if (isset($mesaj))
        echo '<span style="color:red "><h5>' . Html::encode($mesaj) . '</h5></span>';
    ?>
    <?php if (Yii::$app->request->get('mesaj'))
        echo '<span style="color:red "><h5>' . Html::encode(Yii::$app->request->get('mesaj')) . '</h5></span>';
    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol=$data['nr_matricol'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $nr_matricol;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Nume elev',
                'content' => function ($data) {
                    $nume=$data['nume'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $nume;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Prenume elev',
                'content' => function ($data) {
                    $prenume=$data['prenume'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $prenume;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Clasa',
                'content' => function ($data) {
                    $clasa=$data['Clasa'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $clasa;
                },
                'format' => 'text'
            ],
            //'email',


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}  {delete}      {note1}   {note2}         {calcularemediisemestriale1}     {calcularemediisemestriale2}
                               {afisaremediisemestriale1}   {afisaremediisemestriale2}         {absente1}    {absente2}         {purtare1}      {purtare1}    {medii_gen}',
                'buttons' => [
                    'view' => function ($url, $model, $id) {

                        $url = Yii::$app->urlManager->createUrl(['elevi/afisare-informatii-elev', 'id' => $id]); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Afisare elev ')]);

                    },
                    'update' => function ($url, $model, $id) {

                        $url = Yii::$app->urlManager->createUrl(['elevi/editare-informatii-elev', 'id' => $id]); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Editare date elev')]);

                    },

                    'note1' => function ($url, $model, $id) {

                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-note-elev-pe-semestru', 'id_elev' => $id, 'sem' => 'I']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['title' => Yii::t('app', 'Afisare note semestrul I')]);

                    },
                    'note2' => function ($url, $model, $id) {

                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-note-elev-pe-semestru', 'id_elev' => $id, 'sem' => 'II']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['title' => Yii::t('app', 'Afisare note semestrul II')]);

                    },
                    'calcularemediisemestriale1' => function ($url, $model, $id) {


                        $url = Yii::$app->urlManager->createUrl(['catalognote/medii-semestriale', 'id_elev' => $id, 'sem' => 'I']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, ['title' => Yii::t('app', 'Calculare medii semestriale  semestrul I')]);

                    },
                    'calcularemediisemestriale2' => function ($url, $model, $id) {


                        $url = Yii::$app->urlManager->createUrl(['catalognote/medii-semestriale', 'id_elev' => $id, 'sem' => 'II']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, ['title' => Yii::t('app', 'Calculare medii semestriale semestrul II')]);

                    },
                    'afisaremediisemestriale1' => function ($url, $model, $id) {


                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-medii-semestriale-elev', 'id_elev' => $id, 'sem' => 'I']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-share"></span>', $url, ['title' => Yii::t('app', 'Afisare medii semestriale  semestrul I')]);

                    },
                    'afisaremediisemestriale2' => function ($url, $model, $id) {


                        $url = Yii::$app->urlManager->createUrl(['catalognote/afiseaza-medii-semestriale-elev', 'id_elev' => $id, 'sem' => 'II']); // your own url generation logic

                        return Html::a('<span class="glyphicon glyphicon-share"></span>', $url, ['title' => Yii::t('app', 'Afisare medii semestriale semestrul II')]);

                    },
                    'absente1' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalogabsente/absente-elev', 'id_elev' => $id, 'sem' => 'I']);
                        return Html::a('<span class="glyphicon glyphicon-dashboard"></span>', $url, ['title' => Yii::t('app', 'Afisare/motivare absente elev , semestrul I')]);
                    },
                     'absente2' => function ($url, $model, $id) {
                         $url = Yii::$app->urlManager->createUrl(['catalogabsente/absente-elev', 'id_elev' => $id, 'sem' => 'II']);
                         return Html::a('<span class="glyphicon glyphicon-dashboard"></span>', $url, ['title' => Yii::t('app', 'Afisare/motivare absente elev , semestrul II')]);
                     },
                    'purtare1' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalognote/set-nota-purtare', 'id_elev' => $id, 'sem' => 'I']);
                        return Html::a('<span class="glyphicon glyphicon-comment"></span>', $url, ['title' => Yii::t('app', 'Stabilire nota purtare , semestrul I')]);
                    },
                    'purtare2' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['catalognote/set-nota-purtare', 'id_elev' => $id, 'sem' => 'II']);
                        return Html::a('<span class="glyphicon glyphicon-comment"></span>', $url, ['title' => Yii::t('app', 'Stabilire nota purtare , semestrul II')]);
                    },
                    'medii_gen' => function ($url, $model, $id) {
                        $url = Yii::$app->urlManager->createUrl(['diriginti/calculare-medii-generale-elev', 'id_elev' => $id]);
                        return Html::a('<span class="glyphicon glyphicon-cog"></span>', $url, ['title' => Yii::t('app', 'Calculeaza mediile generale anuale pe materii')]);
                    }

                ]

            ]

        ],

    ]); ?>


</div>
