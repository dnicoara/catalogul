<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Diriginti */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Diriginti', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diriginti-view">

    <h4><?= Html::encode($this->title) ?></h4>
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Yii::$app->session->getFlash('success');
        echo '</div>';
    }
    if (Yii::$app->session->hasFlash('error')) {
        echo '<div class="alert alert-danger" role="alert">';
        echo Yii::$app->session->getFlash('error');
        echo '</div>';
    }
    ?>
    <br>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Revenire pagina principala', ['diriginti/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_clasa',
            'nume',
            'prenume',
            'email:email',
            //'parola',
            'data_inregistrarii',
            'data_actualizarii',
        ],
    ]) ?>

</div>
