<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mediigenerale */
/* @var $form yii\widgets\ActiveForm */
?>
<h4><?= Html::encode('Introduceti mediile semestriale ale elevului') ?></h4>
<br>

<div class="mediigenerale-form">


    <?php $form = ActiveForm::begin([
        'action' => ['/diriginti/insert-medii-semestriale-elev-form'],
        'method' => 'get',
    ]);
    ?>

    <?php
    for ($i = 0; $i < count($id_materii); $i++) {
        //$model=new \app\models\MediisemForm();
        echo $form->field($model1, 'mediisem[' . $i . ']')->textInput(['maxlength' => true, 'style' => 'width:100px', 'id' => $nume_materii[$i]])->label($nume_materii[$i]);
        //echo Html::input('number', $id_materii[$i],null,['min'=>1, 'max'=>10]);
        //echo Html::textInput($id_materii[$i], '',['max'=>2,'style'=>'width:10%;']);
        //echo str_repeat('&nbsp;', 5);
        //echo Html::label($nume_materii[$i], 'materia', ['class' => 'control-label']);
        //echo'<br>';
    }
    ?>
    <?php echo $form->field($model, 'hidden')->hiddenInput(['value' => 'formular-complet'])->label(false); ?>
    <?php echo $form->field($model, 'hidden1')->hiddenInput(['value' => $nr_matricol])->label(false); ?>
    <?php echo $form->field($model, 'hidden2')->hiddenInput(['value' => $semestrul])->label(false); ?>


    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
