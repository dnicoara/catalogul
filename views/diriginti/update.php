<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Diriginti */

$this->title = 'Editeaza date diriginte ';
$this->params['breadcrumbs'][] = ['label' => 'Diriginti', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editeaza date diriginte';
?>
<div class="diriginti-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'lista_claselor' => $lista_claselor

    ]) ?>

</div>
