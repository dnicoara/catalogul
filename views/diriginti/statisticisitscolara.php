<?php
use app\models\Profesori;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<?php
$this->title = 'Situatia scolara - statistici : clasa ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title;

//obtin valorile parametrilor globali definiti in config/params.php
$data = \Yii::$app->params['data'];
//$semestrul = \Yii::$app->params['semestrul'];
$semestrul = array(
    'I' => 'Situatia pe semestrul I',
    'II' => 'Situatia pe semestrul II',
    'An' => 'Situatia anuala'
)
?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>
    <br>
    <div class="classDiriginti-form">
        <?php $form = ActiveForm::begin([
            'action' => ['diriginti/statistici-situatia-scolara'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model, 'sem')->widget(Select2::classname(), [
            'data' => $semestrul,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza optiunea ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '400px',
            ],
        ])->label('Selecteaza optiunea de afisare'); ?>

        <?php
        //Child level 1

        //        echo $form->field($model, 'media')->widget(DepDrop::classname(), [
        //        'options' => ['placeholder' => 'Selecteaza optiunea ...'],
        //        'type' => DepDrop::TYPE_SELECT2,
        //        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        //        'pluginOptions' => [
        //        'depends' => ['mediisemestriale-sem'],
        //        'url' => Url::to(['/mediisemestriale/returneazaoptiunile']),
        //        'loadingText' => 'Incarca optiunile ...',
        //        ]
        //        ]);

        ?>

        <?= $form->field($model, 'media')->radioList(
            ['1' => 'Lista mediilor elevilor , pe obiecte',
                '2' => 'Lista mediilor elevilor promovati',
                '3' => 'Lista corigentilor'],
            ['labelOptions' => ['style' => 'display:block']]

        )->label(' '); ?>
        <br>
        <div class="form-group">
            <?= Html::submitButton('Afiseaza', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
