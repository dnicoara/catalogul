<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DirigintiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista dirigintilor';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diriginti-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?php
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        ?>
        <?= Html::a('Adauga diriginte', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la pagina principala', [$este_administratorul_conectat ? 'admin/administrator-edit' : 'site/index'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_clasa',
            'nume',
            'prenume',
            'email:email',
            //'parola',
            //'data_inregistrarii',
            //'data_actualizarii',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
