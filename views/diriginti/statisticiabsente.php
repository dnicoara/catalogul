<?php
use app\models\Profesori;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Statistici absente clasa';
$this->params['breadcrumbs'][] = $this->title;
$semestrul = \Yii::$app->params['semestrul'];
$luna = \Yii::$app->params['luna'];

//obtin valorile parametrilor globali definiti in config/params.php
$data = \Yii::$app->params['data'];
?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>

    <div class="claseProfesori-form">


        <?php $form = ActiveForm::begin([
            'action' => ['diriginti/statistici-absente'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model, 'data1')->widget(DatePicker::classname(), [
            'options' => ['value' => $data, 'placeholder' => 'Selecteaza data initiala ...', 'style' => 'width:300px;'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ],
        ])->label('Data initiala') ?>

        <?= $form->field($model, 'data2')->widget(DatePicker::classname(), [
            'options' => ['value' => $data, 'placeholder' => 'Selecteaza data finala ...', 'style' => 'width:300px;'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ],
        ])->label('Data finala') ?>

        <?= $form->field($model, 'sem')->widget(Select2::classname(), [
            'data' => $semestrul,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza semestrul ...', 'value' => ($luna >= 2 && $luna <= 6) ? 'II' : 'I'],
            'disabled' => $model->isNewRecord ? false : true,
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '380px'
            ],
        ])->label('Selecteaza semestrul'); ?>


        <?= $form->field($model, 'absente')->radioList(
            ['1' => 'Lista total absente motivate/nemotivate',
                '2' => 'Lista total nemotivate',
                '3' => 'Lista total absente',
                '4' => 'Numar total absente'],
            ['labelOptions' => ['style' => 'display:block']]

        ); ?>


        <div class="form-group">
            <?= Html::submitButton('Afiseaza', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>

</div>
