<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$this->title = 'Introduceti noua adresa de e-mail';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="site-about">

    <div class="changeMail-form">

        <?php $form = ActiveForm::begin([
            'action' => ['diriginti/schimba-adresa-mail'],
            'method' => 'get',
        ]);

        ?>
        <?= $form->field($model, 'email')->textInput(['readonly' => true])->label('E-mailul curent') ?>
        <?= $form->field($model, 'noul_email')->input('email')->label('Introduceti noul e-mail') ?>


        <?= Html::activeHiddenInput($model, 'id_diriginte', ['value' => $model->id]) ?>

        <div class="form-group">
            <?= Html::submitButton('Schimba adresa e-mail', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        <?= Html::a('Revenire in cont diriginte', ['infodiriginti/cont-diriginte'], ['class' => 'btn btn-success']) ?>

    </div>

</div>
