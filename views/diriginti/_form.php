<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Diriginti */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diriginti-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_clasa')->widget(Select2::classname(), [
        'data' => $lista_claselor,
        'language' => 'ro',
        //'disabled' => $model->isNewRecord ? false : true,
        'options' => ['placeholder' => 'Selecteaza clasa ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => 300
        ],
    ])->label('Selecteaza clasa'); ?>

    <?= $form->field($model, 'nume')->textInput(['maxlength' => true, 'style' => 'width:300px']) ?>

    <?= $form->field($model, 'prenume')->textInput(['maxlength' => true, 'style' => 'width:300px']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'style' => 'width:300px']) ?>

    <div class="form-group">
        <?= Html::submitButton('Salveaza', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Renunta', ['diriginti/index'], ['class' => 'btn btn-danger']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
