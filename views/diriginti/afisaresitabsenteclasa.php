<?php

use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Infoelevi */
/* @var $dataProvider yii\data\ActiveDataProvider */
switch ($optiunea) {
    case 'I':
        $text = 'Situatia pe semestrul I';
        break;
    case 'II':
        $text = 'Situatia pe semestrul II';
        break;
    case 'An':
        $text = 'Situatia anuala ';
        break;
}
$this->title = 'Absentele elevilor clasei : ' . $nume_clasa . ' - ' . $text;
$this->params['breadcrumbs'][] = 'Absente elevi - statistici';

?>
<div class="clasa-absente">

    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Revenire la selectii', ['diriginti/statistici-absente-clasa'], ['class' => 'btn btn-primary']) ?>

    </p>
    <br>
    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        'motivate',
        'nemotivate',
        'total',
        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>
    <h4><?= Html::encode($this->title) ?><br>
        <h4><?= Html::encode('Perioada selectata : ' . $data1 . ' - ' . $data2) ?><br><br>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items} {pager} {summary}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'motivate',
                        'value' => 'motivate'
                    ],
                    [
                        'attribute' => 'nemotivate',
                        'value' => 'nemotivate'
                    ],

                    [
                        'attribute' => 'total',
                        'value' => 'total'
                    ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} ',
//            ]

                ],

            ]); ?>


</div>
