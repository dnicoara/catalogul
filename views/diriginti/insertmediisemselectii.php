<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php

$this->title = 'Inserare medii semestriale ';
$this->params['breadcrumbs'][] = $this->title;
$semestrul = \Yii::$app->params['semestrul'];
$luna = \Yii::$app->params['luna'];

?>

<?php
if (Yii::$app->session->hasFlash('success') && Yii::$app->session->getFlash('success') != '') {
    echo '<div class="alert alert-success" role="alert">';
    echo Yii::$app->session->getFlash('success');
    echo '</div>';
}
if (Yii::$app->session->hasFlash('error') && Yii::$app->session->getFlash('error') != '') {
    echo '<div class="alert alert-danger" role="alert">';
    echo Yii::$app->session->getFlash('error');
    echo '</div>';
}
?>
<h4><?= Html::encode('Puteti introduce mediile semestriale ale elevilor , daca acestea nu au fost calculate cu ajutorul aplicatiei') ?></h4>
<br>
<div class="site-about">

    <div class="selectareoptiuni-form">


        <?php $form = ActiveForm::begin([
            'action' => ['insert-medii-semestriale-elev'],
            'method' => 'get',
        ]);
        ?>
        <?= $form->field($model, 'nr_matricol')->widget(Select2::classname(), [
            'data' => $lista_elevi_clasa,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza elevul ...'],
            //'options' => count($materii_profesor) === 1 ? ['value' => $materii_profesor[key($materii_profesor)]] : ['placeholder' => 'Selecteaza materia ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '400px'
            ],
        ])->label('Selecteaza elevul'); ?>


        <?= $form->field($model, 'sem')->widget(Select2::classname(), [
            'data' => $semestrul,
            'language' => 'ro',
            'options' => ['placeholder' => 'Selecteaza semestrul ...', 'value' => 'I'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '400px'
            ],
        ])->label('Selecteaza semestrul'); ?>

        <div class="form-group">
            <?= Html::submitButton('Continua...', ['class' => 'btn btn-success']) ?><br><br>
            <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
