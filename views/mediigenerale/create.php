<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mediigenerale */

$this->title = 'Create Mediigenerale';
$this->params['breadcrumbs'][] = ['label' => 'Mediigenerales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mediigenerale-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
