<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mediigenerale */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mediigenerale-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nr_matricol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_materie')->textInput() ?>

    <?= $form->field($model, 'id_clasa')->textInput() ?>

    <?= $form->field($model, 'an_scolar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'media')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
