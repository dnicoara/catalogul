<?php

use app\models\Elevi;
use app\models\Listamaterii;
use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediigeneraleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$nume_clasa = \app\models\Listaclase::getNumeClasa($id_clasa);
$this->title = 'Mediile generale ale elevilor clasei : ' . $nume_clasa;
$this->params['breadcrumbs'][] = $this->title . ' ';

//if (Yii::$app->session->hasFlash('success'))
Yii::$app->session->setFlash('success', '');
Yii::$app->session->setFlash('error', '');

$este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'profesor' :
                                        false;

?>

<div class="clasa-index">

    <br>
    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a($este_profesorul_conectat ? 'Lista elevilor clasei' : 'Lista elevilor clasei', [$este_profesorul_conectat ? 'elevi/afisare-clasa' /*'listaclase/clase-profesor'*/ : 'elevi/afisare-clasa-diriginte', 'id_clasa' => $id_clasa], ['class' => 'btn btn-primary']) ?>
    </p>
    <br><br>

    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        'nr_matricol',
        'nume',
        'prenume',
        'materia',
        'media',
        'an_scolar',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Nume',
                'content' => function ($data) {
                    $nume = $data['nume'];
                    return $nume;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Prenume',
                'content' => function ($data) {
                    $prenume = $data['prenume'];
                    return $prenume;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia = $data['materia'];
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Media generala',
                'content' => function ($data) {
                    $nota = $data['media'];
                    return $nota;
                },
                'format' => 'text'
            ],
            [
                'label' => 'An scolar',
                'content' => function ($data) {
                    $anul = $data['an_scolar'];
                    return $anul;
                },
                'format' => 'text'
            ],


//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} {delete} ',
//            ]

        ],


    ]); ?>

    </br>
    <?php
    $medii_sem_lipsa = Yii::$app->session['elevi_fara_ambele_medii_sem'];

    echo "Lista elevilor si materiile la care nu sunt ambele medii semestriale incheiate , deci nu s-a putut incheia media generala </br></br>";

    for ($i = 0; $i < count($medii_sem_lipsa); $i++) {
        $k = 0;
        foreach ($medii_sem_lipsa[$i] as $value) {
            if ($k == 0)
                echo '<b>' . ($i + 1) . ') ' . Elevi::getNumeElev($value) . '  ' . \app\models\Elevi::getPrenumeElev($value) . '  :  </b>';
            else
                echo Listamaterii::getNumeMaterie($value) . ' ; ';
            $k++;
        }

        echo "</br>";
    }

    ?>


</div>
