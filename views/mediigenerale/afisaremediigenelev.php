<?php

use kartik\export\ExportMenu;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediigeneraleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mediile generale ale elevului : ';
$this->params['breadcrumbs'][] = $this->title . ' ' . $nume_elev . ' ' . $prenume_elev;

//retin numele si id-ul clasei selectate
//Yii::$app->session['nume_clasa'] = $nume_clasa;
//Yii::$app->session['id_clasa'] = $id_clasa;

?>
<div class="clasa-index">

    <h4><?= Html::encode($this->title . ' ' . $nume_elev . ' ' . $prenume_elev) ?></h4>
    <br>
    <p>
        <?= Html::a('Revenire la pagina principala', ['site/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Lista elevilor clasei', ['elevi/afisare-clasa-diriginte', 'id_clasa' => $id_clasa], ['class' => 'btn btn-primary']) ?>

    </p>
    <br><br>

    <?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        'nr_matricol',
        'materia',
        'media',
        'an_scolar',

        ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
            return '#';
        }],
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Exporta',
            'class' => 'btn btn-default',
        ],
    ]);
    // Renders a export dropdown menu
    ?>
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Numar matricol',
                'content' => function ($data) {
                    $nr_matricol = $data['nr_matricol'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $nr_matricol;
                },
                'format' => 'text'
            ],
            [
                'label' => 'Materia',
                'content' => function ($data) {
                    $materia = $data['materia'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $materia;
                },
                'format' => 'text'
            ],

            [
                'label' => 'Media generala',
                'content' => function ($data) {
                    $nota = $data['media'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $nota;
                },
                'format' => 'text'
            ],
            [
                'label' => 'An scolar',
                'content' => function ($data) {
                    $anul = $data['an_scolar'];
                    //$clasa = $data->getListaclase()->one()->Listaclase;
                    return $anul;
                },
                'format' => 'text'
            ],

            //'email',

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} {delete} ',
//            ]

        ],


    ]); ?>

    <br><br>

    <?php

    if (isset($_GET['materii_cu_medii_sem_neincheiate']) && count($_GET['materii_cu_medii_sem_neincheiate']) > 0) {
        echo 'Lista materiilor la care elevul nu are ambele medii semestriale incheiate :  <br><br>';
        $materii = $_GET['materii_cu_medii_sem_neincheiate'];

        for ($i = 0; $i < count($materii); $i++) {

            echo '<b>' . ($i + 1) . ')  ' . \app\models\Listamaterii::getNumeMaterie($materii[$i]) . '  ;  </b>';
            echo "</br>";
        }

    }
    ?>


</div>
