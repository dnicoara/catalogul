<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mediigenerale */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mediigenerales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mediigenerale-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a('Editeaza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Sterge', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nr_matricol',
            'id_materie',
            'id_clasa',
            'an_scolar',
            'media',
        ],
    ]) ?>

</div>
