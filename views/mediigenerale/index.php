<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediigeneraleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mediigenerales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mediigenerale-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mediigenerale', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nr_matricol',
            'id_materie',
            'id_clasa',
            'an_scolar',
            //'media',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
