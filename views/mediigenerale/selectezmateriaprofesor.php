<?php
use app\models\Profesori;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$anul_scolar = \Yii::$app->params['anul_scolar'];

?>

<?php
$this->title = 'Selectare  materie predata de profesorul : ' . $nume_profesor . ' ' . $prenume_profesor;
$this->params['breadcrumbs'][] = $this->title;

?>
<?php if (isset($mesaj)) {
    echo '<div class="alert alert-success" role="alert">';
    echo Html::encode($mesaj);
    echo '</div>';
} else {
    if (Yii::$app->request->get('mesaj')) {
        echo '<div class="alert alert-success" role="alert">';
        echo Html::encode(Yii::$app->request->get('mesaj'));
        echo '</div>';
    }
}

if (isset($_GET['id_elev']))
    $id_elev = $_GET['id_elev'];
else
    $id_elev = "";
?>

<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>

    <div class="mediiGenProfesor-form">


        <?php $form = ActiveForm::begin([
            'action' => ['mediigenerale/calculare-medie-generala-elev-la-materie', 'id_elev' => $id_elev],
            'method' => 'get',
        ]);
        ?>

        <?php

        echo $form->field($modelmaterii, 'id')->widget(Select2::classname(), [
            //'data' => ArrayHelper::map(CartiCustom::CartiDisponibile(), 'id_carte', 'titlu_carte'),
            'name' => 'profesorul',
            'data' => $materii_predate,
            'language' => 'ro',
            'size' => 'md',
            'options' => ['placeholder' => 'Selecteaza materia ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'width' => '300px'
            ],
        ])->label('Selecteaza materia');

        ?>

        <div class="form-group">
            <?= Html::submitButton('Calculez media generala la materia selectata', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Iesire', ['elevi/afisare-clasa'], ['class' => 'btn btn-primary']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
