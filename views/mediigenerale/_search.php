<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MediigeneraleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mediigenerale-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nr_matricol') ?>

    <?= $form->field($model, 'id_materie') ?>

    <?= $form->field($model, 'id_clasa') ?>

    <?= $form->field($model, 'an_scolar') ?>

    <?php // echo $form->field($model, 'media') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
