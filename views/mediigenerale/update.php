<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mediigenerale */

$this->title = 'Update Mediigenerale: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Mediigenerales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mediigenerale-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
