<?php
namespace tests\unit\models;

use app\models\Admin;





class AdminTest extends \Codeception\Test\Unit
{
    /**
     * Administrator id.
     * @var integer
     */
    const id = 1;
    
    /**
     * Admin email address.
     * @var string
     */
    const email = 'nicadialex@yahoo.com';
    
    /**
     * Cont de e-mail inexistent.
     * @var string
     */
    const email_inexistent = 'inexistent@admin.com';
    
    /**
     * Numele administratorului.
     * @var string
     */
    const nume = 'Nicoara';
    
    /**
     * Prenumele administratorului.
     * @var string
     */
    const prenume = 'Daniel';
    
    /**
     *
     * @var \UnitTester
     */
    public $tester;
    
    protected function _before()
    {
    }
    
    /**
     * Test pentru Admin::selectInfoadministrator
     */
    public function testSelectInfoAdministrator()
    {
        $numarIntregistrari = Admin::selectInfoadministrator(self::email)->getTotalCount();
        $this->assertTrue($numarIntregistrari === 1);
        
        $inregistrari = Admin::selectInfoadministrator(self::email)->getModels();
        $this->assertTrue($inregistrari[0]->id === self::id);
        $this->assertTrue($inregistrari[0]->nume === self::nume);
        $this->assertTrue($inregistrari[0]->prenume === self::prenume);
    }
    
    /**
     * Test pentru Admin::selectInfoadministrator
     */
    public function testSelectInfoAdministratorCuEmailInexistentInBazaDeDate()
    {
        $numarIntregistrari = Admin::selectInfoadministrator(self::email_inexistent)->getTotalCount();
        $this->assertEquals($numarIntregistrari, 0);
    }
    
    /**
     * Test pentru Admin::cautaAdminDupaEmail
     */
    public function testCautaAdminDupaEmail()
    {
        $this->assertTrue(Admin::cautaAdminDupaEmail(self::email) == self::id);
    }
    
    /**
     * Test pentru Admin::cautaAdminDupaEmail
     */
    public function testCautaAdminDupaEmailCuEmailInexistentInBazaDeDate()
    {
        $this->assertEmpty(Admin::cautaAdminDupaEmail(self::email_inexistent));
    }
    
    /**
     * Test pentru Admin::ValidareEmail
     */
    public function testValidareEmailDiriginte()
    {
        $id_diriginte = 2;
        
        // Nume tabela incepe cu litera mica
        $this->assertEquals($id_diriginte, Admin::ValidareEmail('nicadialex@yahoo.com', 'diriginti'));
        
        // Nume tabela incepe cu litera mare
        $this->assertEquals($id_diriginte, Admin::ValidareEmail('nicadialex@yahoo.com', 'Diriginti'));
    }
    
    /**
     * Test pentru Admin::ValidareEmail
     */
    public function testValidareEmailElev()
    {
        $id_elev = 1;
        
        // Nume tabela incepe cu litera mica
        $this->assertEquals($id_elev, Admin::ValidareEmail('1/AD1@yahoo.com', 'elevi'));
        
        // Nume tabela incepe cu litera mare
        $this->assertEquals($id_elev, Admin::ValidareEmail('1/AD1@yahoo.com', 'Elevi'));
    }
    
    /**
     * Test pentru Admin::ValidareEmail
     */
    public function testValidareEmailProfesori()
    {
        $id_profesor = 41;
        
        // Nume tabela incepe cu litera mica
        $this->assertEquals($id_profesor, Admin::ValidareEmail('nicadialex@yahoo.com', 'profesori'));
        
        // Nume tabela incepe cu litera mare
        $this->assertEquals($id_profesor, Admin::ValidareEmail('nicadialex@yahoo.com', 'Profesori'));
    }
    
    /**
     * Test pentru Admin::ValidareEmail
     */
    public function testValidareEmailDirectori()
    {
        $id_director = 1;
        
        // Nume tabela incepe cu litera mica
        $this->assertEquals($id_director, Admin::ValidareEmail('badea.rodica@gmail.com', 'directori'));
        
        // Nume tabela incepe cu litera mare
        $this->assertEquals($id_director, Admin::ValidareEmail('badea.rodica@gmail.com', 'Directori'));
    }
    
    /**
     * Test pentru Admin::ValidareEmail
     */
    public function testValidareEmailAdmin()
    {
        $id_admin = 1;
        
        // Nume tabela incepe cu litera mica
        $this->assertEquals($id_admin, Admin::ValidareEmail('nicadialex@yahoo.com', 'admin'));
        
        // Nume tabela incepe cu litera mare
        $this->assertEquals($id_admin, Admin::ValidareEmail('nicadialex@yahoo.com', 'Admin'));
    }
    
    /**
     * Test pentru Admin::ValidareEmail
     */
    public function testValidareEmailSecretariat()
    {
        $id_secretariat = 2;
        
        // Nume tabela incepe cu litera mica
        $this->assertEquals($id_secretariat, Admin::ValidareEmail('nicadialex@yahoo.com', 'secretariat'));
        
        // Nume tabela incepe cu litera mare
        $this->assertEquals($id_secretariat, Admin::ValidareEmail('nicadialex@yahoo.com', 'Secretariat'));
    }
    
    /**
     * Test pentru Admin::ValidareEmail
     */
    public function testValidareEmailInexistent()
    {
        $this->assertNull(Admin::ValidareEmail(self::email_inexistent, 'Diriginti'));
    }
    
    /**
     * Test pentru Admin::getNume.
     */
    public function testGetNume()
    {
        $nume_administrator = 'Nume administrator';
        
        $admin = new Admin();
        $admin->setNume($nume_administrator);
        
        $this->assertEquals($nume_administrator, $admin->getNume());
    }
    
    /**
     * Test pentru Admin::getPrenume.
     */
    public function testGetPrenume()
    {
        $prenume_administrator = 'Prenume administrator';
        
        $admin = new Admin();
        $admin->setPrenume($prenume_administrator);
        
        $this->assertEquals($prenume_administrator, $admin->getPrenume());
    }
    
    /**
     * Test pentru Admin::getMail.
     */
    public function testGetMail()
    {
        $email_administrator = 'email@administrator.com';
        
        $admin = new Admin();
        $admin->mail_administrator = $email_administrator;
        
        $this->assertEquals($email_administrator, $admin->getMail());
    }
    
    /**
     * Test pentru Admin::getIdAdministrator.
     */
    public function testGetIdAdministrator()
    {
        $id_administrator = 1;
        
        $admin = new Admin();
        $admin->setIdAdministrator($id_administrator);
        
        $this->assertEquals($id_administrator, $admin->getIdAdministrator());
    }
    
    public function testGetParola()
    {
        $admin = new Admin();
        $admin->parola_criptata = 'parola';
        
        $parola = $admin->getParola();
        
        $this->assertEquals('parola', $parola);
    }
    
    /**
     *Test pentru Admin::login
     */
    public function testLogin()
    {
        $adresa_email = 'nicadialex@yahoo.com';
        $admin = new Admin();
        $admin->email = $adresa_email;
        $admin->parola = 'admin';
        
        $administrator = $admin->login();
        
        $this->assertEquals(1, $admin->getIdAdministrator());
        $this->assertEquals(self::nume, $admin->getNume());
        $this->assertEquals(self::prenume, $admin->getPrenume());
    }
    
    /**
     *Test pentru Admin::login
     */
    public function testLoginWithWrongCredentials()
    {
        $admin = new Admin();
        $admin->email = self::email_inexistent;
        $admin->parola = 'admin';
        
        $administrator = $admin->login();
        
        $this->assertNull($administrator);
    }
    
    protected function _after()
    {
    }
}