// <?php

use app\models\Mediigenerale;

class MediigeneraleTest extends \Codeception\Test\Unit {
    protected function _before() {
        
    }
    
    /**
     * Test method Mediigenerale::CalculezMediaGeneralaElevMaterie
     */
//     public function testCalculezMediaGeneralaElevMaterieCuTotiParametriiNull() {
//         // Arrange
//         $numar_matricol = null;
//         $id_clasa = null;
//         $an_scolar = null;
//         $id_materie = null;
        
//         // Act
//         $media_generala = Mediigenerale::CalculezMediaGeneralaElevMaterie($numar_matricol, $id_clasa, $an_scolar, $id_materie);
        
//         // Assert
//         $this->assertTrue($media_generala == null);
//     }
    
    /**
     * Test method Mediigenerale::CalculezMediaGeneralaElevMaterie
     */
    public function testCalculezMediaGeneralaElevMaterie() {
        // Arrange
        $numar_matricol = '29/AD1';
        $id_clasa = 2;
        $an_scolar = '2017-2018';
        $id_materie = 4;
        
        // Act
        $media_generala = Mediigenerale::CalculezMediaGeneralaElevMaterie($numar_matricol, $id_clasa, $an_scolar, $id_materie);
        
        // Assert
        $this->assertFalse(empty($media_generala));
        $this->assertTrue($media_generala[0]['nr_matricol'] == $numar_matricol);
        $this->assertTrue($media_generala[0]['id_materie'] == $id_materie);
        $this->assertTrue($media_generala[0]['id_clasa'] == $id_clasa);
        $this->assertTrue($media_generala[0]['an_scolar'] == $an_scolar);
        $this->assertTrue($media_generala[0]['mediagenerala'] == 6.5);
    }
    
    protected function _after() {
        
    }
}