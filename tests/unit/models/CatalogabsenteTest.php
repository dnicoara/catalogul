<?php
namespace tests\unit\models;

use app\models\Catalogabsente;
use Yii;
use yii\data\SqlDataProvider;



/**
 *
 * @author dan.nicoara
 *
 */
class CatalogabsenteTest extends \Codeception\Test\Unit
{
    protected function _before()
    {
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElev
     */
    public function testGetAbsenteElevCuParametriiNull()
    {
        // Arrange
        $numar_matricol = null;
        $id_clasa = null;
        $semestrul = null;
        $anul_scolar = null;
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElev($numar_matricol, $id_clasa, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElev
     */
    public function testGetAbsenteElevCuNumarMatricolSetatSiRestulParametrilorNull()
    {
        // Arrange
        $numar_matricol = '1/AD1';
        $id_clasa = null;
        $semestrul = null;
        $anul_scolar = null;
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElev($numar_matricol, $id_clasa, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElev
     */
    public function testGetAbsenteElevCuNumarMatricolSiIdClasaSetatiSiRestulParametrilorNull()
    {
        // Arrange
        $numar_matricol = '1/AD1';
        $id_clasa = 1;
        $semestrul = null;
        $anul_scolar = null;
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElev($numar_matricol, $id_clasa, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElev
     */
    public function testGetAbsenteElevCuAnulScolarNullSiRestulParametrilorSetati()
    {
        // Arrange
        $numar_matricol = '1/AD1';
        $id_clasa = 1;
        $semestrul = 'I';
        $anul_scolar = null;
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElev($numar_matricol, $id_clasa, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElev
     */
    public function testGetAbsenteElevFaraAbsente()
    {
        // Arrange
        $numar_matricol = '33/AD1';
        $id_clasa = 2;
        $semestrul = 1;
        $anul_scolar = '2017-2018';
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElev($numar_matricol, $id_clasa, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElev
     */
    public function testGetAbsenteElevCuAbsente()
    {
        // Arrange
        $numar_matricol = '1/AD1';
        $id_clasa = 1;
        $semestrul = 'I';
        $anul_scolar = '2017-2018';
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElev($numar_matricol, $id_clasa, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        $inregistrare_absenta = $data_provider->getModels()[0];
        
        // Assert
        $this->assertTrue($numar_absente == 1);
        $this->assertTrue($inregistrare_absenta['nr_matricol'] == $numar_matricol);
        $this->assertTrue($inregistrare_absenta['id_clasa'] == $id_clasa);
        $this->assertTrue($inregistrare_absenta['motivata'] == 'Nemotivata');
        $this->assertTrue($inregistrare_absenta['materia'] == 'Chimie');
        $this->assertTrue($inregistrare_absenta['id_materie'] == '6');
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElevLaMateriaProfesorului
     *
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Parametrul nu e un array
     */
    public function testGetAbsenteElevLaMateriaProfesoruluiCuTotiParametriiNull()
    {
        // Arrange
        $numar_matricol = null;
        $id_clasa = null;
        $semestrul = null;
        $anul_scolar = null;
        $materii_profesor = null;
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElevLaMateriaProfesorului($numar_matricol, $id_clasa, $materii_profesor, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElevLaMateriaProfesorului
     *
     */
    public function testGetAbsenteElevLaMateriaProfesoruluiCuTotiParametriiNullMaiPutinMateriiProfesorCareEsteUnArrayFaraCheie()
    {
        // Arrange
        $numar_matricol = null;
        $id_clasa = null;
        $semestrul = null;
        $anul_scolar = null;
        $materii_profesor = [6, 5, 4];
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElevLaMateriaProfesorului($numar_matricol, $id_clasa, $materii_profesor, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElevLaMateriaProfesorului
     *
     */
    public function testGetAbsenteElevLaMateriaProfesoruluiCuTotiParametriiNullMaiPutinMateriiProfesor()
    {
        // Arrange
        $numar_matricol = null;
        $id_clasa = null;
        $semestrul = null;
        $anul_scolar = null;
        $materii_profesor = [6 => 6, 5 => 5, 4 => 4];
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElevLaMateriaProfesorului($numar_matricol, $id_clasa, $materii_profesor, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElevLaMateriaProfesorului
     *
     */
    public function testGetAbsenteElevLaMateriaProfesoruluiCuTotiParametriiNullMaiPutinMateriiProfesorSiAnulScolar()
    {
        // Arrange
        $numar_matricol = null;
        $id_clasa = null;
        $semestrul = null;
        $anul_scolar = '2017-2018';
        $materii_profesor = [6 => 6, 5 => 5, 4 => 4];
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElevLaMateriaProfesorului($numar_matricol, $id_clasa, $materii_profesor, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElevLaMateriaProfesorului
     *
     */
    public function testGetAbsenteElevLaMateriaProfesoruluiCuTotiParametriiNullMaiPutinMateriiProfesorAnulScolarSiSemestrul()
    {
        // Arrange
        $numar_matricol = null;
        $id_clasa = null;
        $semestrul = 'I';
        $anul_scolar = '2017-2018';
        $materii_profesor = [6 => 6, 5 => 5, 4 => 4];
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElevLaMateriaProfesorului($numar_matricol, $id_clasa, $materii_profesor, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElevLaMateriaProfesorului
     *
     */
    public function testGetAbsenteElevLaMateriaProfesoruluiCuTotiParametriiSetatiMaiPutinNumarMatricol()
    {
        // Arrange
        $numar_matricol = null;
        $id_clasa = 1;
        $semestrul = 'I';
        $anul_scolar = '2017-2018';
        $materii_profesor = [6 => 6, 5 => 5, 4 => 4];
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElevLaMateriaProfesorului($numar_matricol, $id_clasa, $materii_profesor, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElevLaMateriaProfesorului
     */
    public function testGetAbsenteElevLaMateriaProfesoruluiFataAbsente()
    {
        // Arrange
        $numar_matricol = '2/AD1';
        $id_clasa = 1;
        $semestrul = 'I';
        $anul_scolar = '2017-2018';
        $materii_profesor = [6 => 6, 5 => 5, 4 => 4];
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElevLaMateriaProfesorului($numar_matricol, $id_clasa, $materii_profesor, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        
        // Assert
        $this->assertTrue($numar_absente == 0);
    }
    
    /**
     * Test method Catalogabsente::getAbsenteElevLaMateriaProfesorului
     */
    public function testGetAbsenteElevLaMateriaProfesoruluiCuAbsente()
    {
        // Arrange
        $numar_matricol = '1/AD1';
        $id_clasa = 1;
        $semestrul = 'I';
        $anul_scolar = '2017-2018';
        $materii_profesor = [6 => 6, 5 => 5, 4 => 4];
        
        // Act
        $data_provider = Catalogabsente::getAbsenteElevLaMateriaProfesorului($numar_matricol, $id_clasa, $materii_profesor, $semestrul, $anul_scolar);
        $numar_absente = $data_provider->getCount();
        $inregistrare_absenta = $data_provider->getModels()[0];
        
        // Assert
        $this->assertTrue($numar_absente == 1);
        $this->assertTrue($inregistrare_absenta['nr_matricol'] == $numar_matricol);
        $this->assertTrue($inregistrare_absenta['id_clasa'] == $id_clasa);
        $this->assertTrue($inregistrare_absenta['motivata'] == 'Nemotivata');
        $this->assertTrue($inregistrare_absenta['materia'] == 'Chimie');
        $this->assertTrue($inregistrare_absenta['id_materie'] == '6');
    }
    
    /**
     * Test method Catalogabsente::getTotalAbsente
     */
    public function testGetTotalAbsente()
    {
        // Arrange
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM secretariat')->queryScalar();
        
        $provider = new SqlDataProvider([
            'sql' => 'SELECT * FROM secretariat',
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        // Act
        $numar_secretari = Catalogabsente::getTotalAbsente($provider);
        
        // Assert
        $this->assertTrue($numar_secretari == 2);
    }
    
    protected function _after()
    {
    }
}

