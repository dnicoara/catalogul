<?php
namespace app\models;

use app\models\custom\Utils;
use Yii;
use yii\base\Model;
/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *          
 */
class LoginForm extends Model
{

    public $username;

    public $password;

    public $rememberMe = true;

    private $_user = false;

    private $tip_utilizator;

    /**
     *
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [
                [
                    'username',
                    //'password'

                ],
                'required', 'message' => 'E-mailul nu poate fi gol'
            ],
            [
                [
                    'password'

                ],
                'required', 'message' => 'Parola nu poate fi goala'
            ],
            // rememberMe must be a boolean value
            [
                'rememberMe',
                'boolean'
            ],
            // password is validated by validatePassword()
            [
                'password',
                'validatePassword'
            ]
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute
     *            the attribute currently being validated
     * @param array $params
     *            the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (! $this->hasErrors()) {
            $user = $this->getUser($this->tip_utilizator);
            if (! $user || ! $user->validatePassword($this->password)) {
                $this->addError($attribute, 'Utilizator sau parola incorecte.');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser($tip_utilizator)
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username, $tip_utilizator);
        }

        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login($tip_utilizator)
    {
        //var_dump($tip_utilizator);die("hhffdddhh");
        $this->tip_utilizator = $tip_utilizator;
        if ($this->validate()) {
            $this->_user = $this->getUser($tip_utilizator);
            Yii::$app->session->open();

            switch ($this->_user->role) {
                case 'profesor':
                    Yii::$app->session['user'] = $this->_user;
                    break;
                case 'diriginte':
                    Yii::$app->session['user'] = $this->_user;
                    break;
                case 'elev':
                    Yii::$app->session['user'] = $this->_user;
                    break;
                case 'admin':
                    Yii::$app->session['user'] = $this->_user;
                    break;
                case 'director':
                    Yii::$app->session['user'] = $this->_user;
                    break;
                case 'secretar':
                    Yii::$app->session['user'] = $this->_user;
                    break;
            }

            $mesaj = 'Conectare';
            $tip_utilizator = isset(Yii::$app->session['user']) ? Yii::$app->session['user']->role : 'Necunoscut';
            Utils::inregistrare_utilizator_catalog_digital(Yii::$app->session['user']->username, $tip_utilizator, $mesaj);
            return true;
        }
        return false;
    }
}
