<?php
namespace app\models\custom;

use app\models\Accescatalog;
use Yii;
use app\models\Constante;

/**
 *
 * @author dan.nicoara
 *        
 */
class Utils
{
    /**
     * Trimite e-mail.
     * 
     * @param unknown $email
     * @param unknown $link_site
     * @param unknown $parola
     */
    public static function trimite_email($email, $link_site, $parola)
    {
        $sablon_mail = new Constante();
        $mailcontent = $sablon_mail->getSablonMailPentruParolaUitata($email, $parola, $link_site);
        
        // trimit e-mail
        Yii::$app->mailer->compose()
        ->setFrom($sablon_mail->getEmailHost())
        ->setTo($email)
        ->setSubject($sablon_mail->getNumeProiect() . ' - noua parola')
        ->setTextBody('Plain text content')
        ->setHtmlBody($mailcontent)
        ->send();
    }
    
    /**
     * Trimite e-mail cu noua parola.
     * 
     * @param unknown $email_admin
     * @param unknown $nume_admin
     * @param unknown $prenume_admin
     * @param unknown $link_site
     * @param unknown $parola
     */
    public static function trimite_email_parola_noua($email, $nume, $prenume, $link_site, $parola)
    {
        $sablon_mail = new Constante();
        
        $mailcontent = $sablon_mail->getSablonMailPentruParolaModificata($nume, $prenume, $email, $parola, $link_site);
        
        // trimit e-mail
        Yii::$app->mailer->compose()
        ->setFrom($sablon_mail->getEmailHost())
        ->setTo($email)
        ->setSubject('Parola noua - ' . $sablon_mail->getNumeProiect())
        ->setTextBody('Plain text content')
        ->setHtmlBody($mailcontent)
        ->send();
    }

    /**
     * Trimite e-mail cu noul email.
     * @param $email
     * @param $nume
     * @param $prenume
     * @param $link_site
     *
     */
    public static function trimite_email_cu_noul_email($email, $nume, $prenume, $link_site)
    {
        $sablon_mail = new Constante();

        $mailcontent = $sablon_mail->getSablonMailPentruEmailulModificat($nume, $prenume, $email, $link_site);

        // trimit e-mail
        Yii::$app->mailer->compose()
            ->setFrom($sablon_mail->getEmailHost())
            ->setTo($email)
            ->setSubject('Schimbare email - ' . $sablon_mail->getNumeProiect())
            ->setTextBody('Plain text content')
            ->setHtmlBody($mailcontent)
            ->send();
    }

    /**
     * @param $ip
     * @param $user
     * @param $data
     * @param $mesaj
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * inseram date despre utilizatorul care se logheaza sau se delogheaza
     */
    public static function inregistrare_utilizator_catalog_digital($user, $user_type, $mesaj)
    {
        $accescatalog = new Accescatalog();

        $ip = Yii::$app->getRequest()->getUserIP();  //obtin ip-ul utilizatorului

        $accescatalog->ip = $ip;
        $accescatalog->utilizator = $user;
        $accescatalog->tip_utilizator = $user_type;
        $accescatalog->mesaj = $mesaj;

        if ($accescatalog->insert())
            return true;
        else
            return false;

    }


}

