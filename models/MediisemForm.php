<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class MediisemForm extends Model
{

    public $mediisem;

    /**
     *
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [
                [
                    'mediisem',

                ],
                'integer',
                'min' => 1,
                'max' => 10
            ],

        ];
    }


}
