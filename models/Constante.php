<?php

namespace app\models;

/**
* Contine constante folosite in celelalte clase.
*/
class Constante
{    
    /**
    * @var string Numele proiectului.
    */
    const numele_proiectului = 'catalogul-digital';

    /**
     * @var string Adresa de email a hostingului.
     */
    const email_host = 'dnicoara57@gmail.com';
    
    /**
    * @var Date Data curenta.
    */
    private $time;
    
    /**
     * Constructor.
     */
    public function __construct(){
    	$this->time = new \DateTime('now', new \DateTimeZone('Europe/Athens'));
    }
    
    /**
     * Destructor.
     */
    public function __destruct(){
    	$this->time = null;
    }

    /**
     * Returneaza adresa de email a hostingului.
     *
     * @return string
     */
    public function getEmailHost()
    {
        return self::email_host;
    }

    /**
     * Returneaza un sablon de e-mail folosit pentru a trimite o noua parola utilizatorului.
     *
     * @param string $email - E-mailul la care se va trimite parola.
     * @param string $parola - Reprezinta noua parola generata de sistem.
     * @param string $link_site - Link catre website.
     *
     * @return string Sablonul de mail.
     */
    public function getSablonMailPentruParolaUitata($email, $parola, $link_site)
    {

        $continut_sablon = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
							</head>
							<body>
								Bun&#259 ziua , <br/><br/>
								Astazi ' . date('d-m-Y', $this->time->getTimestamp()) . ' parola dumneavoastra a fost resetata .<br/>
								Contul poate fi accesat la adresa : ' . $link_site . '<br/>
								Pentru a va loga folositi: <br/><br/>
									Adresa de mail : ' . $email . '<br/>
									Parola         : ' . $parola . '<br/>
								Veti putea schimba parola accesad contul dumneavoastra.<br/><br/>
								Va multumim ,<br/>' . $this->getNumeProiect() . '<br/>
								www.' . $this->getNumeProiect() . '.ro<br/><br/><br/>
							</body>
						</html>';
        return $continut_sablon;
    }
    
    /**
     * Returneaza numele proiectului.
     *
     * @return string
     */
    public function getNumeProiect()
    {
        return self::numele_proiectului;
    }
    
    /**
     * Returneaza un sablon de e-mail folosit pentru a trimite parola modificata de catre utilizatorului.
     * 
     * @param string $nume Numele utilizatorului.
     * @param string $prenume Prenmele utilizatorului. 
     * @param string $email Emailul utilizatorului.
     * @param string $parola Parola utilizatorului.
     * @param string $link_site Link catre site.
     * 
     * @return string Sablon e-mail.
     */
    public function getSablonMailPentruParolaModificata($nume, $prenume, $email, $parola, $link_site) {
    	$continut_sablon = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<head>
									<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								</head>
								<body>
									Bun&#259 ziua , ' . $prenume . ' ' . $nume . ' ,<br/><br/>
									Astazi ' . date('d-m-Y', $this->time->getTimestamp()) . ' ati modificat parola de logare la situl catalog-digital .<br/>
									Contul poate fi accesat la adresa : ' . $link_site . '<br/>
									Pentru a va loga folositi : <br/><br/>
										Adresa de mail : ' . $email . '<br/>
										Parola         : ' . $parola . '<br/><br/>
									Va multumim pentru interesul dumneavoastra,<br/>' . $this->getNumeProiect() . '<br/>
									www.catalogul-digital.ro<br/><br/><br/>
								</body>
							</html>';
    	return $continut_sablon;
    }

    /**
     * @param $nume
     * @param $prenume
     * @param $email
     * @param $link_site
     * @return string
     * @return string Sablon e-mail.
     */
    public function getSablonMailPentruEmailulModificat($nume, $prenume, $email, $link_site)
    {
        $continut_sablon = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<head>
									<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								</head>
								<body>
									Bun&#259 ziua , ' . $prenume . ' ' . $nume . ' ,<br/><br/>
									Astazi ' . date('d-m-Y', $this->time->getTimestamp()) . ' ati modificat e-mailul de logare la situl catalog-digital .<br/>
									Noua adresa de e-mail este : ' . $email . '<br>
									Pentru a activa noua adresa de e-mail da click pe link-ul urmator : <a href="' . $link_site . '">Click aici</a><br/><br/>

									Va multumim pentru interesul dumneavoastra,<br/>' . $this->getNumeProiect() . '<br/>
									www.catalogul-digital.ro<br/><br/><br/>
								</body>
							</html>';
        return $continut_sablon;
    }

    /**
     * @param $notificarea
     * @return string
     * Returneaza un sablon de e-mail folosit pentru a trimite notificari elevilor selectati de diriginte.
     */
    public function getSablonMailPentruNotificareElevi($notificarea)
    {
        $continut_sablon = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<head>
									<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								</head>
								<body>
									Bun&#259 ziua , <br/><br/>
									' . $notificarea . ' .<br/><br/>

									Va multumesc ,<br/>' . $this->getNumeProiect() . '<br/>
									www.catalogul-digital.ro<br/><br/><br/>
								</body>
							</html>';
        return $continut_sablon;
    }

    /**
     *
     * functia genereaza o parola aleatorie
     *
     * @param int $length - Lungiea parolei.
     *
     * @return string - Parola generata.
     */
    public function generatePassword($length = 8)
    {
        $password = "";
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
        $maxlength = strlen($possible);
        if ($length > $maxlength) {
            $length = $maxlength;
        }

        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
            if (!strstr($password, $char)) { //daca nu gaseste caracterul in $password il adauga
                $password .= $char;
                $i++;
            }

        }

        return $password;
    }
}
