<?php

namespace app\models\autoGenerated;

/**
 * This is the ActiveQuery class for [[Catalogabsente]].
 *
 * @see Catalogabsente
 */
class CatalogabsenteGeneratedQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Catalogabsente[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Catalogabsente|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
