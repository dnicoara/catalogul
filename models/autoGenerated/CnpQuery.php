<?php

namespace app\models\autoGenerated;

/**
 * This is the ActiveQuery class for [[Cnp]].
 *
 * @see Cnp
 */
class CnpQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Cnp[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Cnp|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
