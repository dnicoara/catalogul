<?php

namespace app\models\autoGenerated;

/**
 * This is the ActiveQuery class for [[InfodirigintiGenerated]].
 *
 * @see InfodirigintiGenerated
 */
class InfodirigintiGeneratedQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return InfodirigintiGenerated[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return InfodirigintiGenerated|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
