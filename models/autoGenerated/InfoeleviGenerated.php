<?php

namespace app\models\autoGenerated;

use Yii;

/**
 * This is the model class for table "infoelevi".
 *
 * @property integer $id
 * @property string $nr_matricol
 * @property integer $scutit_la
 * @property string $telefon
 * @property string $tata
 * @property string $mama
 * @property string $ocupatie_mama
 * @property string $ocupatie_tata
 * @property string $religie
 * @property string $naveta
 * @property integer $nr_frati
 * @property string $localitate
 * @property string $strada
 * @property string $numar
 * @property string $bloc
 * @property string $scara
 * @property string $apartament
 * @property string $judet
 * @property string $buletin_seria
 * @property string $buletin_nr
 * @property string $cod_num
 * @property string $data_actualizarii
 * @property string $data_inregistrarii
 */
class InfoeleviGenerated extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'infoelevi';
    }

    /**
     * @inheritdoc
     * @return InfoeleviGeneratedQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InfoeleviGeneratedQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nr_matricol' => 'Nr Matricol',
            'scutit_la' => 'Scutit La',
            'telefon' => 'Telefon',
            'tata' => 'Tata',
            'mama' => 'Mama',
            'ocupatie_mama' => 'Ocupatie Mama',
            'ocupatie_tata' => 'Ocupatie Tata',
            'religie' => 'Religie',
            'naveta' => 'Naveta',
            'nr_frati' => 'Nr Frati',
            'localitate' => 'Localitate',
            'strada' => 'Strada',
            'numar' => 'Numar',
            'bloc' => 'Bloc',
            'scara' => 'Scara',
            'apartament' => 'Apartament',
            'judet' => 'Judet',
            'buletin_seria' => 'Buletin Seria',
            'buletin_nr' => 'Buletin Nr',
            'cod_num' => 'Cod Num',
            'data_actualizarii' => 'Data Actualizarii',
            'data_inregistrarii' => 'Data Inregistrarii',
        ];
    }
}
