<?php

namespace app\models\autoGenerated;

/**
 * This is the ActiveQuery class for [[Materiiclasa]].
 *
 * @see Materiiclasa
 */
class MateriiclasaGeneratedQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Materiiclasa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Materiiclasa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
