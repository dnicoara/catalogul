<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class User
{
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($email, $tip_utilizator)
    {

        $utilizator = '';
        $user = new User();
        switch ($tip_utilizator) {
            case 'profesor':
                    $utilizator = Profesori::getProfesorDupaEmail($email);
                    $user->role = 'profesor';
                break;
            
                case 'diriginte':
                    $utilizator = Diriginti::getDiriginteDupaEmail($email);
                    $user->role = 'diriginte';
                    Yii::$app->session['id_clasa'] = Diriginti::getIdClasa($utilizator['id']);
                    Yii::$app->session['numar_notificari_nerezolvate'] = Notificaridiriginte::getNrNotificariNerezolvate(Yii::$app->session['id_clasa']);
                    break;

                case 'director':
                    $utilizator = Directori::getDirectorDupaEmail($email);
                    $user->role = 'director';
                break;

                case 'administrator':
                    $utilizator = Admin::getAdminDupaEmail($email);
                    $user->role = 'admin';
                break;

                case 'elev':
                    $utilizator = Elevi::getElevDupaEmail($email);
                    $user->role = 'elev';
                    $user->nr_matricol = $utilizator['nr_matricol'];
                break;

                case 'secretar':
                    $utilizator = Secretariat::getSecretarDupaEmail($email);
                    $user->role = 'secretar';
                break;
        }
        if ($utilizator != null) {
            $user->id = $utilizator['id'];
            $user->username = $utilizator['email'];
            $user->password = $utilizator['parola'];
            $user->nume = $utilizator['nume'];
            $user->prenume = $utilizator['prenume'];  

            return $user;
        }

        return null;
    }

    /**
     * Validates password
     *
     * @param string $password
     *            password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //var_dump($this->password);
        // var_dump(md5($password));
        //die();
        return $this->password === md5($password);
    }
}
