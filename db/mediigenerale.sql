-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 08 Noi 2017 la 10:41
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `catalogul`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mediigenerale`
--

CREATE TABLE IF NOT EXISTS `mediigenerale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(5) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `media` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Salvarea datelor din tabel `mediigenerale`
--

INSERT INTO `mediigenerale` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `media`) VALUES
(1, 'IZ29', 4, 2, '2017-2018', 7.5),
(2, 'IZ29', 10, 2, '2017-2018', 8.62),
(3, 'IZ28', 2, 2, '2017-2018', 8.5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
