-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Noi 2017 la 22:07
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `catalogul`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `absente`
--

CREATE TABLE IF NOT EXISTS `absente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `nr_tot_abs` int(4) NOT NULL,
  `nr_abs_mot` int(4) NOT NULL,
  `sem` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `parola` varchar(15) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Salvarea datelor din tabel `admin`
--

INSERT INTO `admin` (`id`, `nume`, `prenume`, `email`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Nicoara', 'Daniel', 'nicadialex@yahoo.com', 'admin', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Nicoara', 'Adriana', 'dalex997@hotmail.com', 'adi12345', '2015-07-19 22:00:00', '2015-12-02 22:08:29');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `adresa`
--

CREATE TABLE IF NOT EXISTS `adresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) NOT NULL,
  `nr` varchar(8) NOT NULL,
  `bl` varchar(8) NOT NULL,
  `sc` varchar(4) NOT NULL,
  `ap` int(4) NOT NULL,
  `judetul` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `catalogabsente`
--

CREATE TABLE IF NOT EXISTS `catalogabsente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` char(10) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` char(9) NOT NULL,
  `absente` date NOT NULL,
  `motivata` tinyint(1) NOT NULL,
  `sem` char(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Salvarea datelor din tabel `catalogabsente`
--

INSERT INTO `catalogabsente` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `absente`, `motivata`, `sem`) VALUES
(1, '33/AD1', 1, 2, '2017-2018', '2017-10-04', 0, 'I'),
(2, '34/AD1', 17, 2, '2017-2018', '2017-10-10', 1, 'I'),
(3, '32/AD1', 3, 2, '2017-2018', '2017-11-07', 1, 'I'),
(4, '31/AD1', 4, 2, '2017-2018', '2017-11-09', 0, 'I'),
(5, '29/AD1', 5, 2, '2017-2018', '2017-10-04', 1, 'I'),
(6, '30/AD1', 1, 2, '2017-2018', '2017-10-05', 0, 'I'),
(7, '30/AD1', 10, 2, '2017-2018', '2017-09-21', 1, 'I'),
(8, '31/AD1', 4, 2, '2017-2018', '2017-10-14', 0, 'I'),
(9, '34/AD1', 10, 2, '2017-2018', '2017-10-02', 0, 'I'),
(10, '33/AD1', 10, 2, '2017-2018', '2017-10-08', 0, 'I'),
(11, '29/AD1', 4, 2, '2017-2018', '2017-11-02', 1, 'I'),
(12, '30/AD1', 10, 2, '2017-2018', '2017-10-24', 1, 'I'),
(13, '34/AD1', 4, 2, '2017-2018', '2017-10-24', 1, 'I'),
(14, '35/AD1', 4, 2, '2017-2018', '2017-10-24', 0, 'I'),
(15, '35/AD1', 4, 2, '2017-2018', '2017-10-24', 0, 'I'),
(16, '29/AD1', 4, 2, '2017-2018', '2017-10-24', 0, 'I'),
(17, '29/AD1', 4, 2, '2017-2018', '2017-11-12', 0, 'I'),
(18, '29/AD1', 4, 2, '2017-2018', '2017-11-13', 0, 'I'),
(21, '32/AD1', 10, 2, '2017-2018', '2017-11-13', 1, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `catalognote`
--

CREATE TABLE IF NOT EXISTS `catalognote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` char(10) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `data` date NOT NULL,
  `an_scolar` char(9) NOT NULL,
  `nota` int(2) NOT NULL,
  `teza` int(1) NOT NULL,
  `sem` char(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Salvarea datelor din tabel `catalognote`
--

INSERT INTO `catalognote` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `data`, `an_scolar`, `nota`, `teza`, `sem`) VALUES
(1, '30/AD1', 1, 1, '2017-09-22', '2017-2018', 10, 0, 'I'),
(2, '33/AD1', 2, 2, '2017-10-02', '2017-2018', 10, 0, 'I'),
(3, '32/AD1', 3, 2, '2017-09-25', '2017-2018', 9, 0, 'I'),
(4, '31/AD1', 4, 2, '2017-10-10', '2017-2018', 8, 0, 'I'),
(5, '29/AD1', 4, 2, '2017-10-03', '2017-2018', 9, 0, 'I'),
(6, '34/AD1', 1, 2, '2017-10-12', '2017-2018', 9, 0, 'I'),
(28, '32/AD1', 4, 2, '2017-10-03', '2017-2018', 9, 0, 'I'),
(27, '29/AD1', 4, 2, '2017-10-09', '2017-2018', 8, 0, 'I'),
(26, '34/AD1', 10, 2, '2017-10-19', '2017-2018', 7, 0, 'I'),
(25, '31/AD1', 4, 2, '2017-10-19', '2017-2018', 8, 0, 'I'),
(24, '33/AD1', 10, 2, '2017-10-19', '2017-2018', 9, 0, 'I'),
(23, '30/AD1', 10, 1, '2017-10-19', '2017-2018', 10, 0, 'I'),
(22, '29/AD1', 5, 2, '2017-10-19', '2017-2018', 10, 0, 'I'),
(29, '29/AD1', 10, 2, '2017-10-24', '2017-2018', 6, 1, 'I'),
(30, '32/AD1', 3, 2, '2017-10-03', '2017-2018', 9, 1, 'I'),
(31, '32/AD1', 3, 2, '2017-10-02', '2017-2018', 8, 0, 'I'),
(32, '31/AD1', 4, 2, '2017-10-04', '2017-2018', 7, 0, 'I'),
(33, '32/AD1', 4, 2, '2017-10-05', '2017-2018', 5, 0, 'I'),
(34, '29/AD1', 4, 2, '2017-10-10', '2017-2018', 7, 0, 'I'),
(35, '31/AD1', 4, 2, '2017-10-19', '2017-2018', 9, 1, 'I'),
(36, '29/AD1', 10, 2, '2017-11-07', '2017-2018', 8, 0, 'I'),
(37, '29/AD1', 10, 2, '2017-11-03', '2017-2018', 10, 0, 'I'),
(38, '34/AD1', 10, 2, '2017-11-13', '2017-2018', 7, 0, 'I'),
(39, '29/AD1', 4, 2, '2017-11-26', '2017-2018', 6, 0, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `directori`
--

CREATE TABLE IF NOT EXISTS `directori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Salvarea datelor din tabel `directori`
--

INSERT INTO `directori` (`id`, `nume`, `prenume`, `email`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Badea', 'Rodica', 'badea.rodica@gmail.com', '3d4e992d8d8a7d848724aa26ed7f4176', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Sbuchea', 'Liliana', 'lili72_ro@yahoo.com', '3d4e992d8d8a7d848724aa26ed7f4176', '2015-07-19 22:00:00', '2015-12-02 22:08:29');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `diriginti`
--

CREATE TABLE IF NOT EXISTS `diriginti` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_clasa` int(3) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Salvarea datelor din tabel `diriginti`
--

INSERT INTO `diriginti` (`id`, `id_clasa`, `nume`, `prenume`, `email`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 1, 'Indrecan', 'Mihaela', 'indrecanmihaela@yahoo.com', '581df6ac548560cc89c14ba06c9393a5', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 2, 'Nicoara', 'Daniel', 'nicadialex@yahoo.com', '581df6ac548560cc89c14ba06c9393a5', '2015-07-19 22:00:00', '2015-12-02 22:08:29'),
(3, 3, 'Floroni', 'Loredana', 'loredana_floroni@yahoo.com', '581df6ac548560cc89c14ba06c9393a5', '2017-11-19 16:55:08', '2017-11-19 18:55:08'),
(4, 4, 'Lupulescu', 'Carmen', 'carmenvioletalupulescu@yahoo.com', '581df6ac548560cc89c14ba06c9393a5', '2017-11-19 17:11:48', '2017-11-19 20:07:47'),
(5, 5, 'Muntean', 'Marius', 'munteanpmarius@yahoo.com', '581df6ac548560cc89c14ba06c9393a5', '2017-11-27 14:47:13', '2017-11-19 20:07:47'),
(6, 6, 'Paraian', 'Ana-Maria', 'anamariaparaian@yahoo.com', '581df6ac548560cc89c14ba06c9393a5', '2017-11-27 14:48:55', '2017-11-27 00:00:00'),
(7, 7, 'Bota', 'Cristian', 'cris77_b@yahoo.com', '581df6ac548560cc89c14ba06c9393a5', '2017-11-27 15:16:57', '2017-11-27 00:00:00'),
(8, 8, 'Ghida', 'Romeo', 'adyy_ghyyda@yahoo.com', '581df6ac548560cc89c14ba06c9393a5', '2017-11-27 15:17:49', '2017-11-27 00:00:00');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `elevclasa`
--

CREATE TABLE IF NOT EXISTS `elevclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` char(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=210 ;

--
-- Salvarea datelor din tabel `elevclasa`
--

INSERT INTO `elevclasa` (`id`, `nr_matricol`, `id_clasa`, `an_scolar`) VALUES
(1, '1/AD1', 1, '2017-2018'),
(2, '2/AD1', 1, '2017-2018'),
(3, '3/AD1', 1, '2017-2018'),
(4, '4/AD1', 1, '2017-2018'),
(5, '5/AD1', 1, '2017-2018'),
(6, '6/AD1', 1, '2017-2018'),
(7, '7/AD1', 1, '2017-2018'),
(8, '8/AD1', 1, '2017-2018'),
(9, '9/AD1', 1, '2017-2018'),
(10, '10/AD1', 1, '2017-2018'),
(12, '12/AD1', 1, '2017-2018'),
(11, '11/AD1', 1, '2017-2018'),
(13, '13/AD1', 1, '2017-2018'),
(14, '14/AD1', 1, '2017-2018'),
(15, '15/AD1', 1, '2017-2018'),
(16, '16/AD1', 1, '2017-2018'),
(17, '17/AD1', 1, '2017-2018'),
(18, '18/AD1', 1, '2017-2018'),
(19, '19/AD1', 1, '2017-2018'),
(20, '20/AD1', 1, '2017-2018'),
(21, '21/AD1', 1, '2017-2018'),
(22, '22/AD1', 1, '2017-2018'),
(23, '23/AD1', 1, '2017-2018'),
(24, '24/AD1', 1, '2017-2018'),
(25, '25/AD1', 1, '2017-2018'),
(26, '26/AD1', 1, '2017-2018'),
(27, '27/AD1', 1, '2017-2018'),
(28, '28/AD1', 1, '2017-2018'),
(29, '29/AD1', 2, '2017-2018'),
(30, '30/AD1', 2, '2017-2018'),
(31, '31/AD1', 2, '2017-2018'),
(32, '32/AD1', 2, '2017-2018'),
(33, '33/AD1', 2, '2017-2018'),
(34, '34/AD1', 2, '2017-2018'),
(35, '35/AD1', 2, '2017-2018'),
(36, '36/AD1', 2, '2017-2018'),
(37, '37/AD1', 2, '2017-2018'),
(38, '38/AD1', 2, '2017-2018'),
(39, '39/AD1', 2, '2017-2018'),
(40, '40/AD1', 2, '2017-2018'),
(41, '41/AD1', 2, '2017-2018'),
(42, '42/AD1', 2, '2017-2018'),
(43, '43/AD1', 2, '2017-2018'),
(44, '44/Ad1', 2, '2017-2018'),
(45, '45/AD1', 2, '2017-2018'),
(46, '46/AD1', 2, '2017-2018'),
(47, '47/AD1', 2, '2017-2018'),
(48, '48/AD1', 2, '2017-2018'),
(49, '49/AD1', 2, '2017-2018'),
(50, '50/AD1', 2, '2017-2018'),
(51, '51/AD1', 2, '2017-2018'),
(52, '52/AD1', 2, '2017-2018'),
(53, '53/AD1', 2, '2017-2018'),
(54, '54/AD1', 2, '2017-2018'),
(55, '55/AD1', 2, '2017-2018'),
(56, '56/AD1', 2, '2017-2018'),
(57, '57/AD1', 2, '2017-2018'),
(58, '58/AD1', 3, '2017-2018'),
(59, '59/AD1', 3, '2017-2018'),
(60, '60/AD1', 3, '2017-2018'),
(61, '61/AD1', 3, '2017-2018'),
(62, '62/AD1', 3, '2017-2018'),
(63, '63/AD1', 3, '2017-2018'),
(64, '64/AD1', 3, '2017-2018'),
(65, '65/AD1', 3, '2017-2018'),
(66, '66/AD1', 3, '2017-2018'),
(67, '67/AD1', 3, '2017-2018'),
(68, '68/AD1', 3, '2017-2018'),
(69, '69/AD1', 3, '2017-2018'),
(70, '70/AD1', 3, '2017-2018'),
(71, '71/AD1', 3, '2017-2018'),
(72, '72/AD1', 3, '2017-2018'),
(73, '73/AD1', 3, '2017-2018'),
(74, '74/AD1', 3, '2017-2018'),
(75, '75/AD1', 3, '2017-2018'),
(76, '76/AD1', 3, '2017-2018'),
(77, '77/AD1', 3, '2017-2018'),
(78, '78/AD1', 3, '2017-2018'),
(79, '79/AD1', 3, '2017-2018'),
(80, '80/AD1', 3, '2017-2018'),
(81, '81/AD1', 3, '2017-2018'),
(82, '82/AD1', 3, '2017-2018'),
(83, '83/AD1', 3, '2017-2018'),
(84, '84/AD1', 3, '2017-2018'),
(85, '85/AD1', 4, '2017-2018'),
(86, '86/AD1', 4, '2017-2018'),
(87, '87/AD1', 4, '2017-2018'),
(88, '88/AD1', 4, '2017-2018'),
(89, '89/AD1', 4, '2017-2018'),
(90, '90/AD1', 4, '2017-2018'),
(91, '91/AD1', 4, '2017-2018'),
(92, '92/AD1', 4, '2017-2018'),
(93, '93/AD1', 4, '2017-2018'),
(94, '94/AD1', 4, '2017-2018'),
(95, '95/AD1', 4, '2017-2018'),
(96, '96/AD1', 4, '2017-2018'),
(97, '97/AD1', 4, '2017-2018'),
(98, '98/AD1', 4, '2017-2018'),
(99, '99/AD1', 4, '2017-2018'),
(100, '100/AD1', 4, '2017-2018'),
(101, '101/AD1', 4, '2017-2018'),
(102, '102/AD1', 4, '2017-2018'),
(103, '103/AD1', 4, '2017-2018'),
(104, '104/AD1', 4, '2017-2018'),
(105, '105/AD1', 4, '2017-2018'),
(106, '106/AD1', 4, '2017-2018'),
(107, '107/AD1', 4, '2017-2018'),
(108, '108/AD1', 4, '2017-2018'),
(109, '109/AD1', 4, '2017-2018'),
(110, '110/AD1', 4, '2017-2018'),
(111, '111/AD1', 4, '2017-2018'),
(112, '112/AD1', 4, '2017-2018'),
(113, '113/AD1', 4, '2017-2018'),
(114, '114/AD2', 5, '2017-2018'),
(115, '115/AD2', 5, '2017-2018'),
(116, '116/AD2', 5, '2017-2018'),
(117, '117/AD2', 5, '2017-2018'),
(118, '118/AD2', 5, '2017-2018'),
(119, '119/AD2', 5, '2017-2018'),
(120, '120/AD2', 5, '2017-2018'),
(121, '121/AD2', 5, '2017-2018'),
(122, '122/AD2', 5, '2017-2018'),
(123, '123/AD2', 5, '2017-2018'),
(124, '124/AD2', 5, '2017-2018'),
(125, '125/AD2', 5, '2017-2018'),
(126, '126/AD2', 5, '2017-2018'),
(127, '127/AD2', 5, '2017-2018'),
(128, '128/AD2', 5, '2017-2018'),
(129, '129/AD2', 5, '2017-2018'),
(130, '130/AD2', 5, '2017-2018'),
(131, '131/AD2', 5, '2017-2018'),
(132, '132/AD2', 5, '2017-2018'),
(133, '133/AD2', 5, '2017-2018'),
(134, '134/AD2', 5, '2017-2018'),
(135, '135/AD2', 5, '2017-2018'),
(136, '136/AD2', 5, '2017-2018'),
(137, '137/AD2', 5, '2017-2018'),
(138, '138/AD2', 5, '2017-2018'),
(139, '139/AD2', 5, '2017-2018'),
(140, '140/AD2', 5, '2017-2018'),
(141, '141/AD2', 5, '2017-2018'),
(142, '142/AD2', 6, '2017-2018'),
(143, '143/AD2', 6, '2017-2018'),
(144, '144/AD2', 6, '2017-2018'),
(145, '145/AD2', 6, '2017-2018'),
(146, '146/AD2', 6, '2017-2018'),
(147, '147/AD2', 6, '2017-2018'),
(148, '148/AD2', 6, '2017-2018'),
(149, '149/AD2', 6, '2017-2018'),
(150, '150/AD2', 6, '2017-2018'),
(151, '151/AD2', 6, '2017-2018'),
(152, '152/AD2', 6, '2017-2018'),
(153, '153/AD2', 6, '2017-2018'),
(154, '154/AD2', 6, '2017-2018'),
(155, '155/AD2', 6, '2017-2018'),
(156, '156/AD2', 6, '2017-2018'),
(157, '157/AD2', 6, '2017-2018'),
(158, '158/AD2', 6, '2017-2018'),
(159, '159/AD2', 6, '2017-2018'),
(160, '160/AD2', 6, '2017-2018'),
(161, '161/AD2', 6, '2017-2018'),
(162, '162/AD2', 6, '2017-2018'),
(163, '163/AD2', 6, '2017-2018'),
(164, '164/AD2', 6, '2017-2018'),
(165, '165/AD2', 6, '2017-2018'),
(166, '166/AD2', 6, '2017-2018'),
(167, '167/AD2', 6, '2017-2018'),
(168, '168/AD2', 6, '2017-2018'),
(169, '169/AD2', 7, '2017-2018'),
(170, '170/AD2', 7, '2017-2018'),
(171, '171/AD2', 7, '2017-2018'),
(172, '172/AD2', 7, '2017-2018'),
(173, '173/AD2', 7, '2017-2018'),
(174, '174/AD2', 7, '2017-2018'),
(175, '175/AD2', 7, '2017-2018'),
(176, '176/AD2', 7, '2017-2018'),
(177, '177/AD2', 7, '2017-2018'),
(178, '178/AD2', 7, '2017-2018'),
(179, '179/AD2', 7, '2017-2018'),
(180, '180/AD2', 7, '2017-2018'),
(181, '181/AD2', 7, '2017-2018'),
(182, '182/AD2', 7, '2017-2018'),
(183, '183/AD2', 7, '2017-2018'),
(184, '184/AD2', 7, '2017-2018'),
(185, '185/AD2', 7, '2017-2018'),
(186, '186/AD2', 7, '2017-2018'),
(187, '187/AD2', 7, '2017-2018'),
(188, '188/AD2', 7, '2017-2018'),
(189, '189/AD2', 8, '2017-2018'),
(190, '190/AD2', 8, '2017-2018'),
(191, '191/AD2', 8, '2017-2018'),
(192, '192/AD2', 8, '2017-2018'),
(193, '193/AD2', 8, '2017-2018'),
(194, '194/AD2', 8, '2017-2018'),
(195, '195/AD2', 8, '2017-2018'),
(196, '196/AD2', 8, '2017-2018'),
(197, '197/AD2', 8, '2017-2018'),
(198, '198/AD2', 8, '2017-2018'),
(199, '199/AD2', 8, '2017-2018'),
(200, '200/AD2', 8, '2017-2018'),
(201, '201/AD2', 8, '2017-2018'),
(202, '202/AD2', 8, '2017-2018'),
(203, '203/AD2', 8, '2017-2018'),
(204, '204/AD2', 8, '2017-2018'),
(205, '205/AD2', 8, '2017-2018'),
(206, '206/AD2', 8, '2017-2018'),
(207, '207/AD2', 8, '2017-2018'),
(208, '208/AD2', 8, '2017-2018'),
(209, '209/AD2', 8, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `elevi`
--

CREATE TABLE IF NOT EXISTS `elevi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `sex` char(1) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=210 ;

--
-- Salvarea datelor din tabel `elevi`
--

INSERT INTO `elevi` (`id`, `nr_matricol`, `nume`, `prenume`, `email`, `sex`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, '1/AD1', 'Baluse', 'Adrian-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-23 22:00:00', '2017-11-20 17:45:29'),
(2, '2/AD1', 'Batuca', 'Dragos-Sebastian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-26 22:00:00', '2017-11-12 14:59:42'),
(3, '3/AD1', 'Butoi', 'Rebeca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-07-26 22:00:00', '2017-11-12 15:02:28'),
(4, '4/AD1', 'Costache', 'Mihai-Mihnea', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-31 22:00:00', '2015-08-01 00:00:00'),
(5, '5/AD1', 'Crai', 'Sorinela-Lorena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-08-13 13:26:59', '2015-08-13 15:26:59'),
(6, '6/AD1', 'Cristescu', 'Iulia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-08-20 19:54:44', '2017-11-12 14:43:31'),
(7, '7/AD1', 'David', 'Simonel-Olimpiu', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-12 13:21:00', '2017-11-15 00:00:00'),
(8, '8/AD1', 'Dragoi', 'Andrei-Laurentiu', 'angelica@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-18 21:28:46', '2017-11-20 19:34:26'),
(9, '9/AD1', 'Filan', 'Paul-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-18 22:15:06', '2017-11-20 21:03:43'),
(10, '10/AD1', 'Fuca', 'Razvzn-Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-19 13:45:03', '2017-11-19 19:46:50'),
(11, '11/AD1', 'Fulea', 'Stefania-Yasmine', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:45:45', NULL),
(12, '12/AD1', 'Gardean', 'Alexandru-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:45:45', NULL),
(13, '13/AD1', 'Gherghina', 'Darius-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:49:45', NULL),
(14, '14/AD1', 'Ghita', 'Georgiana-Miriam', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:49:45', NULL),
(15, '15/AD1', 'Harangus', 'Robert-Adrian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:52:06', NULL),
(16, '16/AD1', 'Iovaneasa', 'Flavia-Giorgiana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:52:06', NULL),
(17, '17/AD1', 'Josan', 'Gelu-Samuel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:55:00', NULL),
(18, '18/AD1', 'Mezinesc', 'Adelina-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:55:00', NULL),
(19, '19/AD1', 'Mistau', 'Traian-Razvan', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:57:01', NULL),
(20, '20/AD1', 'Muntean', 'David-Mirel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:57:01', NULL),
(21, '21/AD1', 'Murgoi', 'Ionela-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:58:31', NULL),
(22, '22/AD1', 'Nadasan', 'Victor', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:58:31', NULL),
(23, '23/AD1', 'Nedelea', 'Igori', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:00:23', NULL),
(24, '24/AD1', 'Olarescu', 'Sergiu-Robert', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:00:23', NULL),
(25, '25/AD1', 'Onescu', 'Andeea-Ancuta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:02:19', NULL),
(26, '26/AD1', 'Oparlescu', 'Dina-Lidia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:02:19', NULL),
(27, '27/AD1', 'Popescu', 'Sergiu-Catalin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', NULL),
(28, '28/AD1', 'Tirea', 'Delia-Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', NULL),
(29, '29/AD1', 'Ciuca', 'Luis Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(30, '30/AD1', 'Crisan', 'Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(31, '31/AD1', 'Danciu', 'Andrea', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(32, '32/AD1', 'Dragotesc', 'Antonela Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(33, '33/AD1', 'Dumitresc', ' Carina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(34, '34/AD1', 'Filan', 'Ioana-Karina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(35, '35/AD1', 'Ganea', 'Lavinia Denisa', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(36, '36/AD1', 'Gherman', 'Razvan Petru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(37, '37/AD1', 'Goia', 'Robert Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(38, '38/AD1', 'Hoalga', 'Maria-Cristina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(39, '39/AD1', 'Istrate', 'Briana Alesia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(40, '40/AD1', 'Leja', 'Ana Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(41, '41/AD1', 'Mara', ' Andreas Emanuel', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(42, '42/AD1', 'Mituca', 'Bianca Antonia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(43, '43/AD1', 'Niculesc', 'Larissa Madalina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(44, '44/AD1', 'Pascal', 'Vasile Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(45, '45/AD1', 'Petrescu', ' Ruxandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(46, '46/AD1', 'Popa', ' Marc Antonio', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(47, '47/AD1', 'Priala', ' Alin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(48, '48/AD1', 'Priala', ' David', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(49, '49/AD1', 'Romanesc', ' Denis Raul', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(50, '50/AD1', 'Scorobete', ' Filip Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(51, '51/AD1', 'Stefoni', ' Denisa Nicoleta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(52, '52/AD1', 'Tabirgic', 'Alexandra Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(53, '53/AD1', 'Ungur', 'Natalia-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(54, '54/AD1', 'Vitionesc', 'Denisa Naomi', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(55, '55/AD1', 'Vitionescu', 'Denisa Octavia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(56, '56/AD1', 'Voda', ' Adela Doinita', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(57, '57/AD1', 'Zanon', ' Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(58, '58/AD1', 'Alb', 'Simina-Magdalena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:05:23', NULL),
(59, '59/AD1', 'Alboni', 'Andrei-Sebastian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:05:23', NULL),
(60, '60/AD1', 'Andrei', 'Daniel-Sebastian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:07:38', NULL),
(61, '61/AD1', 'Arimescu', 'Darius-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:07:38', NULL),
(62, '62/AD1', 'Bojin', 'Andrada-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:08:11', NULL),
(63, '63/AD1', 'Botoni', 'Letisia-Alexandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:11:29', NULL),
(64, '64/AD1', 'Bucur', 'Murgoi-Rares-Danut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:11:29', NULL),
(65, '65/AD1', 'Cretu', 'Giorgiana-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:13:39', NULL),
(66, '66/AD1', 'Damian', 'Emanuel-David', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:13:39', NULL),
(67, '67/AD1', 'Gheorma', 'Carmen-Ariana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:15:36', NULL),
(68, '68/AD1', 'Hibais', 'Alina-Dorina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:15:36', NULL),
(69, '69/AD1', 'Manincea', 'Izabela-Alexandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:17:52', NULL),
(70, '70/AD1', 'Martin', 'Emanuela-Monica', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:17:52', NULL),
(71, '71/AD1', 'Merian', 'Constantin-Darius', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:19:46', NULL),
(72, '72/AD1', 'Micsoni', 'Raul-Ciprian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:19:46', NULL),
(73, '73/AD1', 'Muraru', 'Mihai-Lucian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:21:36', NULL),
(74, '74/AD1', 'Onica', 'Izabela-Lorena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:21:36', NULL),
(75, '75/AD1', 'Orasan', 'Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:23:33', NULL),
(76, '76/AD1', 'Paulescu', 'Robert-Alin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:23:33', NULL),
(77, '77/AD1', 'Pelin', 'Rares', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:25:01', NULL),
(78, '78/AD1', 'Sabau', 'Raul-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:25:01', NULL),
(79, '79/AD1', 'Salasan', 'Daniel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:26:32', NULL),
(80, '80/AD1', 'Sauca', 'Darius-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:26:32', NULL),
(81, '81/AD1', 'Terlea', 'Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:28:13', NULL),
(82, '82/AD1', 'Todoni', 'Eugenia-Raluca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:28:13', NULL),
(83, '83/AD1', 'Todoran', 'Ariana-Maria-Irina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:30:15', NULL),
(84, '84/AD1', 'Voian', 'Iosua-Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:30:15', NULL),
(85, '85/AD1', 'Avieritei', 'Daniela-Florentina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:49:30', NULL),
(86, '86/AD1', 'Avramescu', 'Bianca-Rebeca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:49:30', NULL),
(87, '87/AD1', 'Beuca', 'Leia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:13:11', NULL),
(88, '88/AD1', 'Beuca', 'Lorena-Timeia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:13:11', NULL),
(89, '89/AD1', 'Bolovaneanu', 'Lorena-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:14:48', NULL),
(90, '90/AD1', 'Burtoi', 'Simina-Paraschiva', 'e', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:14:48', NULL),
(91, '91/AD1', 'Dinisoni', 'Florina-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:16:28', NULL),
(92, '92/AD1', 'Filip', 'Maria-Irina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:16:28', NULL),
(93, '93/AD1', 'Gardean', 'Flavius-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:18:39', NULL),
(94, '94/AD1', 'Gardean', 'Alexandru-Silvestru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:18:39', NULL),
(95, '95/AD1', 'Haidaciuc', 'Flavia-Miriam', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:20:48', NULL),
(96, '96/AD1', 'Hrezdac', 'Andreea-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:20:48', NULL),
(97, '97/AD1', 'Iancu', 'Marian-Stelian', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:22:32', NULL),
(98, '98/AD1', 'Iosivoni', 'Roxana-Alexandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:22:32', NULL),
(99, '99/AD1', 'Jurj', 'Marius-Gabriel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:24:20', NULL),
(100, '100/AD1', 'Lut', 'Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:24:20', NULL),
(101, '101/AD1', 'Mardan', 'Elena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:26:41', NULL),
(102, '102/AD1', 'Meleru', 'Amalia-Nicola', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:26:41', NULL),
(103, '103/AD1', 'Munteanu', 'Eduard-Adrian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:28:15', NULL),
(104, '104/AD1', 'Neacsu', 'Simina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:28:15', NULL),
(105, '105/AD1', 'Onita', 'Lorena-Simona', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:30:17', NULL),
(106, '106/AD1', 'Opric', 'Raul-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:30:17', NULL),
(107, '107/AD1', 'Pusdrea', 'Andrei-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:31:56', NULL),
(108, '108/AD1', 'Pusca', 'Bianca-Geanina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:31:56', NULL),
(109, '109/AD1', 'Sucioni', 'Daniel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:33:14', NULL),
(110, '110/AD1', 'Sucioni', 'Gabriel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:33:14', NULL),
(111, '111/AD1', 'Stefoni', 'Petronela-Eugenia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:34:54', NULL),
(112, '112/AD1', 'Toncea', 'Ludovica-Mariana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:34:54', NULL),
(113, '113/AD1', 'Virvoniu', 'Daniel-Iosif', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:35:49', NULL),
(114, '114/AD2', 'Baicu', 'Andereea-Raluca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:12:29', NULL),
(115, '115/AD2', 'Blaga', 'Iulian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:12:29', NULL),
(116, '116/AD2', 'Bogdan', 'Georgiana-Loredana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:13:51', NULL),
(117, '117/AD2', 'Ciura', 'Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:13:51', NULL),
(118, '118/AD2', 'Cociuba', 'Christa-Chtara', '', '', '', '2017-11-27 14:15:35', NULL),
(119, '119/AD2', 'Condurache', 'Iemima-Lorena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:15:35', NULL),
(120, '120/AD2', 'Danut', 'Ariana-Amelia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:16:52', NULL),
(121, '121/AD2', 'Grecea', 'Vladut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:16:52', NULL),
(122, '122/AD2', 'Iacoboni', 'Ioana-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:18:05', NULL),
(123, '123/AD2', 'Ionescu', 'Bianca-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:18:05', NULL),
(124, '124/AD2', 'Iovanescu', 'Rebeca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:19:16', NULL),
(125, '125/AD2', 'Lapadus', 'Denisa-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:19:16', NULL),
(126, '126/AD2', 'Macra', 'Denisa-Alexandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:20:20', NULL),
(127, '127/AD2', 'Mihai', 'Ioana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:20:20', NULL),
(128, '128/AD2', 'Mihaescu', 'Raul', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:21:42', NULL),
(129, '129/AD2', 'Muntoi', 'Cosmin-Octavian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:21:42', NULL),
(130, '130/AD2', 'Neiconi', 'Raul-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:23:01', NULL),
(131, '131/AD2', 'Nistor', 'Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:23:01', NULL),
(132, '132/AD2', 'Onica', 'Silviu-Darian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:24:26', NULL),
(133, '133/AD2', 'Opreanu', 'Eugen-Gabriel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:24:26', NULL),
(134, '134/AD2', 'Racatean', 'Andrei-Ionatan', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:29:42', NULL),
(135, '135/AD2', 'Resedea', 'Ramona-Alexandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:29:42', NULL),
(136, '136/AD2', 'Strejeru', 'Diana-Amalia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:31:05', NULL),
(137, '137/AD2', 'Timis', 'Bogdan-Vasile', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:31:05', NULL),
(138, '138/AD2', 'Tip', 'Naomi-Paula', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:32:12', NULL),
(139, '139/AD2', 'Vasioni', 'Miruna', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:32:12', NULL),
(140, '140/AD2', 'Vitonesc', 'Ariana-Silvana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:33:40', NULL),
(141, '141/AD2', 'Zaharie', 'Iosif-Ioana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:33:40', NULL),
(142, '142/AD2', 'Balinda', 'Roxana-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:51:33', NULL),
(143, '143/AD2', 'Baloi', 'Cosmin-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:51:33', NULL),
(144, '144/AD2', 'Bernat', 'Andeeas', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:53:04', NULL),
(145, '145/AD2', 'Boiani', 'Cristina-Rebeca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:53:04', NULL),
(146, '146/AD2', 'Bota', 'Georgiana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:54:22', NULL),
(147, '147/AD2', 'Budau', 'Denisa', 'Raluca', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:54:22', NULL),
(148, '148/AD2', 'Caluser', 'Denis-Sorin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:55:38', NULL),
(149, '149/AD2', 'Costa', 'Cosmin-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:55:38', NULL),
(150, '150/AD2', 'Danciu', 'Flavius-Vladut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:56:56', NULL),
(151, '151/AD2', 'Dramnescu', 'Roxana-Gabriele', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:56:56', NULL),
(152, '152/AD2', 'Huniadi', 'Robert-Florian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:58:17', NULL),
(153, '153/AD2', 'Igna', 'Sabrina-Cosmina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:58:17', NULL),
(154, '154/AD2', 'Ipate', 'Ana-Maria-Izabele', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:59:42', NULL),
(155, '155/AD2', 'Jura', 'Antonio-Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 14:59:42', NULL),
(156, '156/AD2', 'Mailat', 'Alina-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:01:03', NULL),
(157, '157/AD2', 'Marc', 'Alin-Daniel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:01:03', NULL),
(158, '158/AD2', 'Marton', 'Alina-Nicoleta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:02:31', NULL),
(159, '159/AD2', 'Mihaiesc', 'Rahela-Madalina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:02:31', NULL),
(160, '160/AD2', 'Moldovan', 'Ioana-Brandusa-Jeni', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:05:21', NULL),
(161, '161/AD2', 'Oichia', 'Alessandra-Simona', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:05:21', NULL),
(162, '162/AD2', 'Podean', 'Adela-Marcela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:06:45', NULL),
(163, '163/AD2', 'Salasan', 'Aurelian-Petrisor', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:06:45', NULL),
(164, '164/AD2', 'Salasan', 'Izvorel-Octavian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:08:49', NULL),
(165, '165/AD2', 'Scafaru', 'Anamaria-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:08:49', NULL),
(166, '166/AD2', 'Stoica', 'Anamaria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:10:31', NULL),
(167, '167/AD2', 'Tamas', 'Ioan-Gabriel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:10:31', NULL),
(168, '168/AD2', 'Virvesc', 'Denis-Ioan', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:11:03', NULL),
(169, '169/AD2', 'Ardelean', 'Darius-Cristian', 'elev@yahoo.com', 'M', '4', '2017-11-27 15:19:50', NULL),
(170, '170/AD2', 'Baiesc', 'Bianca-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:19:50', NULL),
(171, '171/AD2', 'Butnaru', 'Roxana-Ana-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:21:04', NULL),
(172, '172/AD2', 'Buzduga', 'Rebeca-Diana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:21:04', NULL),
(173, '173/AD2', 'Ciocoiu', 'Ionela-Diana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:22:08', NULL),
(174, '174/AD2', 'Ciolea', 'Elena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:22:08', NULL),
(175, '175/AD2', 'Fercal', 'Cristian-Miron', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:23:28', NULL),
(176, '176/AD2', 'Halmagi', 'David-Daniel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:23:28', NULL),
(177, '177/AD2', 'Iubas', 'Ruben-Flavian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:24:42', NULL),
(178, '178/AD2', 'Merian', 'Elena-Dinora', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:24:42', NULL),
(179, '179/AD2', 'Molnar', 'Emanuel-Roberto', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:26:02', NULL),
(180, '180/AD2', 'Moraru', 'Alexandra-Dorina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:26:02', NULL),
(181, '181/AD2', 'Obrejan', 'Dalia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:27:02', NULL),
(182, '182/AD2', 'Ocos', 'Dana-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:27:02', NULL),
(183, '183/AD2', 'Oparlescu', 'Daniel-Emanuel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:28:10', NULL),
(184, '184/AD2', 'Oproni', 'Roxana-Adina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:28:10', NULL),
(185, '185/AD2', 'Pirv', 'Beatrice-Laura', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:29:23', NULL),
(186, '186/AD2', 'Ungur', 'Laurentiu-Marius', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:29:23', NULL),
(187, '187/AD2', 'Ursic', 'Simona-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:30:34', NULL),
(188, '188/AD2', 'Vrabel', 'Axel-Aleodor', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:30:34', NULL),
(189, '189/AD2', 'Bodescu', 'Iosif', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:37:44', NULL),
(190, '190/AD2', 'Capraru', 'Andreea-Cristina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:37:44', NULL),
(191, '191/AD2', 'Ciobanasu', 'Ioan-Eusebio', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:39:06', NULL),
(192, '192/AD2', 'Ciolacu', 'Mihai-Daniel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:39:06', NULL),
(193, '193/AD2', 'Coman', 'Emanuela-Daniela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:41:17', NULL),
(194, '194/AD2', 'Coroescu', 'Alin-Dumitru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:41:17', NULL),
(195, '195/AD2', 'Craciunesc', 'Iosif-Emanuel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:42:31', NULL),
(196, '196/AD2', 'Crisan', 'Emanuel-Mircea', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:42:31', NULL),
(197, '197/AD2', 'Davidoni', 'Emanuel-Iosif', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:43:50', NULL),
(198, '198/AD2', 'Danila', 'Raluca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:43:50', NULL),
(199, '199/AD2', 'Florescu', 'Robert-Raul', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:45:01', NULL),
(200, '200/AD2', 'Fona', 'Lucian-Octavian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:45:01', NULL),
(201, '201/AD2', 'Legea', 'Luiza-Iuliana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:46:12', NULL),
(202, '202/AD2', 'Niculesc', 'Alina-Claudia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:46:12', NULL),
(203, '203/AD2', 'Opaic', 'Emil-Beniamin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:47:48', NULL),
(204, '204/AD2', 'Pascu', 'Natanael-Viorel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:47:48', NULL),
(205, '205/AD2', 'Stanila', 'Daniel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:48:57', NULL),
(206, '206/AD2', 'Stelian', 'Adina-Malina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:48:57', NULL),
(207, '207/AD2', 'Stoica', 'Denisa-Andrada', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:50:13', NULL),
(208, '208/AD2', 'Voinea', 'Darius-Iulian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:50:13', NULL),
(209, '209/AD2', 'Vulcu', 'Iulia-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-27 15:50:47', NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `elevi2`
--

CREATE TABLE IF NOT EXISTS `elevi2` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `sex` char(1) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Salvarea datelor din tabel `elevi2`
--

INSERT INTO `elevi2` (`id`, `nr_matricol`, `nume`, `prenume`, `email`, `sex`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, '1/AD1', 'Baluse', 'Adrian-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-23 22:00:00', '2017-11-20 17:45:29'),
(2, '2/AD1', 'Batuca', 'Dragos-Sebastian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-26 22:00:00', '2017-11-12 14:59:42'),
(3, '3/AD1', 'Butoi', 'Rebeca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-07-26 22:00:00', '2017-11-12 15:02:28'),
(4, '4/AD4', 'Costache', 'Mihai-Mihnea', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-31 22:00:00', '2015-08-01 00:00:00'),
(5, '5/AD1', 'Crai', 'Sorinela-Lorena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-08-13 13:26:59', '2015-08-13 15:26:59'),
(6, '6/AD1', 'Cristescu', 'Iulia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-08-20 19:54:44', '2017-11-12 14:43:31'),
(7, '7/AD1', 'David', 'Simonel-Olimpiu', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-12 13:21:00', '2017-11-15 00:00:00'),
(8, '8/AD1', 'Dragoi', 'Andrei-Laurentiu', 'angelica@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-18 21:28:46', '2017-11-20 19:34:26'),
(9, '9/AD1', 'Filan', 'Paul-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-18 22:15:06', '2017-11-20 21:03:43'),
(10, '10/AD1', 'Fuca', 'Razvzn-Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-19 13:45:03', '2017-11-19 19:46:50'),
(11, '11/AD1', 'Fulea', 'Stefania-Yasmine', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:45:45', NULL),
(12, '12/AD1', 'Gardean', 'Alexandru-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:45:45', NULL),
(13, '13/AD1', 'Gherghina', 'Darius-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:49:45', NULL),
(14, '14/AD1', 'Ghita', 'Georgiana-Miriam', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:49:45', NULL),
(15, '15/AD1', 'Harangus', 'Robert-Adrian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:52:06', NULL),
(16, '16/AD1', 'Iovaneasa', 'Flavia-Giorgiana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:52:06', NULL),
(17, '17/AD1', 'Josan', 'Gelu-Samuel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:55:00', NULL),
(18, '18/AD1', 'Mezinesc', 'Adelina-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:55:00', NULL),
(19, '19/AD1', 'Mistau', 'Traian-Razvan', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:57:01', NULL),
(20, '20/AD1', 'Muntean', 'David-Mirel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:57:01', NULL),
(21, '21/AD1', 'Murgoi', 'Ionela-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:58:31', NULL),
(22, '22/AD1', 'Nadasan', 'Victor', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:58:31', NULL),
(23, '23/AD!', 'Nedelea', 'Igori', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:00:23', NULL),
(24, '24/AD1', 'Olarescu', 'Sergiu-Robert', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:00:23', NULL),
(25, '25/AD1', 'Onescu', 'Andeea-Ancuta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:02:19', NULL),
(26, '26/AD1', 'Oparlescu', 'Dina-Lidia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:02:19', NULL),
(27, '27/AD1', 'Popescu', 'Sergiu-Catalin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', NULL),
(28, '28/AD!', 'Tirea', 'Delia-Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', NULL),
(29, '29/AD1', 'Ciuca', 'Luis Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '30/AD1', 'Crisan', 'Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '31/AD1', 'Danciu', 'Andrea', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '32/AD1', 'Dragotesc', 'Antonela Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '33/AD3', 'Dumitresc', ' Carina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '34/AD1', 'Filan', 'Ioana-Karina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '35/AD1', 'Ganea', 'Lavinia Denisa', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '36/AD1', 'Gherman', 'Razvan Petru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '37/AD1', 'Goia', 'Robert Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '38/AD1', 'Hoalga', 'Maria-Cristina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '39/AD1', 'Istrate', 'Briana Alesia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '40/AD1', 'Leja', 'Ana Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '41/AD1', 'Mara', ' Andreas Emanuel', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '42/AD1', 'Mituca', 'Bianca Antonia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '43/AD1', 'Niculesc', 'Larissa Madalina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '44/Ad1', 'Pascal', 'Vasile Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '45/AD1', 'Petrescu', ' Ruxandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '46/AD1', 'Popa', ' Marc Antonio', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, '47/AD1', 'Priala', ' Alin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, '48/AD1', 'Priala', ' David', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, '49/AD1', 'Romanesc', ' Denis Raul', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, '50/AD1', 'Scorobete', ' Filip Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, '51/AD1', 'Stefoni', ' Denisa Nicoleta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, '52/AD1', 'Tabirgic', 'Alexandra Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, '53/AD1', 'Ungur', 'Natalia-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, '54/AD1', 'Vitionesc', 'Denisa Naomi', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, '55/AD1', 'Vitionescu', 'Denisa Octavia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, '56/AD1', 'Voda', ' Adela Doinita', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, '57/AD1', 'Zanon', ' Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infodirectori`
--

CREATE TABLE IF NOT EXISTS `infodirectori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_director` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infodirectori`
--

INSERT INTO `infodirectori` (`id`, `fk_director`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infodiriginti`
--

CREATE TABLE IF NOT EXISTS `infodiriginti` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_diriginte` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infodiriginti`
--

INSERT INTO `infodiriginti` (`id`, `fk_diriginte`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', '', '5', 'Hunedoara', 'HD', '688830', 'I', 0, '2017-11-15 13:37:15', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(3, 3, 'M', 'politehnica', '072549396', 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', 0, '2015-08-21 21:24:38', '2015-08-21 19:24:38'),
(4, 8, 'F', 'postliceale', '0745678908', 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', 0, '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 7, 'M', 'Universitare', '', 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', 0, '2015-10-11 21:37:53', '2015-10-11 19:37:53');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infoelevi`
--

CREATE TABLE IF NOT EXISTS `infoelevi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `tata` varchar(20) NOT NULL,
  `mama` varchar(20) NOT NULL,
  `ocupatie_mama` varchar(20) DEFAULT NULL,
  `ocupatie_tata` varchar(20) DEFAULT NULL,
  `religie` varchar(15) NOT NULL,
  `naveta` varchar(2) NOT NULL DEFAULT 'nu',
  `nr_frati` int(2) DEFAULT '0',
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `cod_num` varchar(13) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=256 ;

--
-- Salvarea datelor din tabel `infoelevi`
--

INSERT INTO `infoelevi` (`id`, `nr_matricol`, `telefon`, `tata`, `mama`, `ocupatie_mama`, `ocupatie_tata`, `religie`, `naveta`, `nr_frati`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `cod_num`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, '1/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(2, '2/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(3, '3/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(4, '4/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(5, '5/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(6, '6/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(7, '7/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(8, '8/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(9, '9/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(10, '10/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(11, '11/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(12, '12/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(13, '13/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(14, '14/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(15, '15/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(16, '16/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(17, '17/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(18, '18/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(19, '19/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(20, '20/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(21, '21/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(22, '22/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(23, '23/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(24, '24/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(25, '25/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(26, '26/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(27, '27/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(28, '28/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(29, '29/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(30, '30/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(31, '31/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(32, '32/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(33, '33/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(34, '34/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(35, '35/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(36, '36/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(37, '37/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(38, '38/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(39, '39/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(40, '40/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(41, '41/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(42, '42/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(43, '43/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(44, '44/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(45, '45/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(46, '46/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(47, '47/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(48, '48/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(49, '49/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(50, '50/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(51, '51/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(52, '52/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(53, '53/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(54, '54/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(55, '55/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(56, '56/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(57, '57/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(58, '58/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(59, '59/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(60, '60/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(61, '61/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(62, '62/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(63, '63/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(64, '64/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(65, '65/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(66, '66/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(67, '67/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(68, '68/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(69, '69/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(70, '70/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(71, '71/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(72, '72/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(73, '73/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(74, '74/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(75, '75/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(76, '76/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(77, '77/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(78, '78/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(79, '79/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(80, '80/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(81, '81/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(82, '82/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(83, '83/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(84, '84/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(85, '85/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(86, '86/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(87, '87/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(88, '88/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(89, '89/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(90, '90/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(91, '91/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(92, '92/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(93, '93/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(94, '94/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(95, '95/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(96, '96/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(97, '97/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(98, '98/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(99, '99/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(100, '100/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(101, '101/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(102, '102/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(103, '103/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(104, '104/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(105, '105/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(106, '106/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(107, '107/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(108, '108/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(109, '109/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(110, '110/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(111, '111/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(112, '112/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(113, '113/AD1', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(114, '114/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(115, '115/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(116, '116/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(117, '117/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(118, '118/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(119, '119/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(120, '120/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(121, '121/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(122, '122/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(123, '123/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(124, '124/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(125, '125/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(126, '126/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(127, '127/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(128, '128/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(129, '129/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(130, '130/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(131, '131/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(132, '132/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(133, '133/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(134, '134/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(135, '135/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(136, '136/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(137, '137/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(138, '138/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(139, '139/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(140, '140/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(141, '141/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(142, '142/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(143, '143/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(144, '144/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(145, '145/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(146, '146/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(147, '147/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(148, '148/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(149, '149/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(150, '150/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(151, '151/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(152, '152/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(153, '153/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(154, '154/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(155, '155/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(156, '156/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(157, '157/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(158, '158/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(159, '159/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(160, '160/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(161, '161/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(162, '162/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(163, '163/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(164, '164/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(165, '165/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(166, '166/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(167, '167/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(168, '168/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(169, '169/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(170, '170/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(171, '171/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(172, '172/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(173, '173/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(174, '174/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(175, '175/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(176, '176/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(177, '177/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(178, '178/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(179, '179/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(180, '180/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(181, '181/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(182, '182/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(183, '183/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(184, '184/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(185, '185/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(186, '186/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(187, '187/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(188, '188/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(189, '189/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(190, '190/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(191, '191/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(192, '192/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(193, '193/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(194, '194/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(195, '195/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(196, '196/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(197, '197/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(198, '198/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(199, '199/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(200, '200/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(201, '201/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(202, '202/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(203, '203/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(204, '204/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(205, '205/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(206, '206/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(207, '207/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(208, '208/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33'),
(209, '209/AD2', NULL, '', '', NULL, NULL, '', 'nu', 0, '', NULL, '', NULL, NULL, NULL, '', NULL, NULL, '', NULL, '2017-11-27 15:57:33');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infoprofesori`
--

CREATE TABLE IF NOT EXISTS `infoprofesori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_profesor` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infoprofesori`
--

INSERT INTO `infoprofesori` (`id`, `fk_profesor`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(3, 3, 'M', 'politehnica', '072549396', 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', 0, '2015-08-21 21:24:38', '2015-08-21 19:24:38'),
(4, 8, 'F', 'postliceale', '0745678908', 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', 0, '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 7, 'M', 'Universitare', '', 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', 0, '2015-10-11 21:37:53', '2015-10-11 19:37:53');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `listaclase`
--

CREATE TABLE IF NOT EXISTS `listaclase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Clasa` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Salvarea datelor din tabel `listaclase`
--

INSERT INTO `listaclase` (`id`, `Clasa`) VALUES
(1, 'IX-A'),
(2, 'IX-B'),
(3, 'IX-C'),
(4, 'IX-D'),
(5, 'IX-E'),
(6, 'IX-F'),
(7, 'IX-G'),
(8, 'X-A'),
(9, 'X-B'),
(10, 'X-C'),
(11, 'X-D'),
(12, 'X-E'),
(13, 'X-F'),
(14, 'X-G'),
(15, 'XI-A'),
(16, 'XI-B'),
(17, 'XI-C'),
(18, 'XI-D'),
(19, 'XI-E'),
(20, 'XI-F'),
(21, 'XI-G'),
(22, 'XII-A'),
(23, 'XII-B'),
(24, 'XII-C'),
(25, 'XII-D'),
(26, 'XII-E'),
(27, 'XII-F'),
(28, 'XII-G'),
(29, 'V'),
(30, 'VI'),
(31, 'VII'),
(32, 'VIII'),
(33, 'IX-H');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `listamaterii`
--

CREATE TABLE IF NOT EXISTS `listamaterii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia` char(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Salvarea datelor din tabel `listamaterii`
--

INSERT INTO `listamaterii` (`id`, `materia`) VALUES
(1, 'Engleza'),
(2, 'Franceza'),
(3, 'Romana'),
(4, 'Matematica'),
(5, 'Fizica'),
(6, 'Chimie'),
(7, 'Biologie'),
(8, 'Istorie'),
(9, 'Geografie'),
(10, 'Informatica'),
(11, 'Economie'),
(12, 'Filosofie'),
(13, 'Religie'),
(14, 'Logica'),
(15, 'Sport'),
(16, 'Desen'),
(17, 'Economie aplicata'),
(18, 'Tic'),
(19, 'Educatie artistica'),
(20, 'Educatie vizuala'),
(21, 'Educatie antreprenoriala'),
(22, 'M2'),
(23, 'Chimie optional'),
(24, 'Purtare');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `materiiclasa`
--

CREATE TABLE IF NOT EXISTS `materiiclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Salvarea datelor din tabel `materiiclasa`
--

INSERT INTO `materiiclasa` (`id`, `id_materie`, `id_clasa`, `an_scolar`) VALUES
(1, 1, 2, '2017-2018'),
(2, 2, 2, '2017-2018'),
(3, 3, 2, '2017-2018'),
(4, 4, 2, '2017-2018'),
(5, 5, 2, '2017-2018'),
(6, 6, 2, '2017-2018'),
(7, 7, 2, '2017-2018'),
(8, 8, 2, '2017-2018'),
(9, 9, 2, '2017-2018'),
(10, 10, 2, '2017-2018'),
(11, 1, 1, '2017-2018'),
(12, 2, 1, '2017-2018'),
(13, 3, 1, '2017-2018'),
(14, 4, 1, '2017-2018'),
(15, 5, 1, '2017-2018'),
(16, 6, 1, '2017-2018'),
(17, 7, 1, '2017-2018'),
(18, 8, 1, '2017-2018'),
(19, 9, 1, '2017-2018'),
(20, 10, 1, '2017-2018'),
(21, 11, 1, '2017-2018'),
(22, 12, 1, '2017-2018'),
(23, 13, 1, '2017-2018'),
(24, 14, 1, '2017-2018'),
(25, 15, 1, '2017-2018'),
(26, 16, 1, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mediigenerale`
--

CREATE TABLE IF NOT EXISTS `mediigenerale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `media` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Salvarea datelor din tabel `mediigenerale`
--

INSERT INTO `mediigenerale` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `media`) VALUES
(6, '32/AD1', 4, 2, '2017-2018', 6),
(5, '31/AD1', 4, 2, '2017-2018', 7.5),
(4, '29/AD1', 4, 2, '2017-2018', 8.5);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mediisemestriale`
--

CREATE TABLE IF NOT EXISTS `mediisemestriale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `media` float NOT NULL DEFAULT '0',
  `sem` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Salvarea datelor din tabel `mediisemestriale`
--

INSERT INTO `mediisemestriale` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `media`, `sem`) VALUES
(1, '29/AD1', 4, 2, '2017-2018', 8, 'I'),
(2, '31/AD1', 4, 2, '2017-2018', 8, 'I'),
(3, '32/AD1', 4, 2, '2017-2018', 7, 'I'),
(4, '29/AD1', 4, 2, '2017-2018', 9, 'II'),
(5, '31/AD1', 4, 2, '2017-2018', 7, 'II'),
(6, '32/AD1', 4, 2, '2017-2018', 5, 'II'),
(7, '29/AD1', 24, 2, '2017-2018', 9, 'I'),
(8, '30/AD1', 24, 2, '2017-2018', 10, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `profesorclasa`
--

CREATE TABLE IF NOT EXISTS `profesorclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profesor` int(2) NOT NULL,
  `id_clasa` int(2) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Salvarea datelor din tabel `profesorclasa`
--

INSERT INTO `profesorclasa` (`id`, `id_profesor`, `id_clasa`, `id_materie`, `an_scolar`) VALUES
(1, 1, 8, 21, '2017-2018'),
(2, 1, 9, 21, '2017-2018'),
(3, 1, 10, 21, '2017-2018'),
(4, 1, 11, 21, '2017-2018'),
(5, 1, 12, 21, '2017-2018'),
(6, 1, 13, 21, '2017-2018'),
(7, 1, 14, 21, '2017-2018'),
(8, 1, 15, 11, '2017-2018'),
(9, 1, 16, 11, '2017-2018'),
(10, 1, 17, 11, '2017-2018'),
(11, 1, 18, 11, '2017-2018'),
(12, 1, 20, 11, '2017-2018'),
(13, 1, 21, 11, '2017-2018'),
(14, 1, 27, 11, '2017-2018'),
(15, 1, 28, 11, '2017-2018'),
(16, 1, 27, 22, '2017-2018'),
(17, 1, 28, 22, '2017-2018'),
(18, 1, 27, 17, '2017-2018'),
(19, 4, 2, 7, '2017-2018'),
(20, 4, 3, 7, '2017-2018'),
(21, 4, 5, 7, '2017-2018'),
(22, 4, 6, 7, '2017-2018'),
(23, 4, 7, 7, '2017-2018'),
(24, 4, 6, 6, '2017-2018'),
(25, 4, 33, 6, '2017-2018'),
(26, 4, 12, 7, '2017-2018'),
(27, 4, 13, 7, '2017-2018'),
(28, 4, 14, 7, '2017-2018'),
(29, 4, 31, 7, '2017-2018'),
(30, 4, 14, 6, '2017-2018'),
(31, 4, 25, 6, '2017-2018'),
(32, 4, 26, 6, '2017-2018'),
(33, 4, 26, 23, '2017-2018'),
(34, 5, 29, 20, '2017-2018'),
(35, 5, 32, 3, '2017-2018'),
(36, 5, 10, 3, '2017-2018'),
(37, 5, 13, 3, '2017-2018'),
(38, 5, 19, 3, '2017-2018'),
(39, 5, 21, 3, '2017-2018'),
(40, 5, 18, 19, '2017-2018'),
(41, 5, 19, 19, '2017-2018'),
(42, 5, 25, 19, '2017-2018'),
(43, 5, 26, 19, '2017-2018'),
(44, 41, 2, 4, '2017-2018'),
(45, 41, 3, 4, '2017-2018'),
(46, 41, 16, 4, '2017-2018'),
(47, 41, 22, 4, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `profesori`
--

CREATE TABLE IF NOT EXISTS `profesori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Salvarea datelor din tabel `profesori`
--

INSERT INTO `profesori` (`id`, `nume`, `prenume`, `email`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Adam', 'Costel', 'adam.costel@yahoo.com', '793741d54b00253006453742ad4ed534', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Alimpesc', 'Mihaela', 'vasilonimihaela@yahoo.com', '793741d54b00253006453742ad4ed534', '2015-07-19 22:00:00', '2015-12-02 22:08:29'),
(3, 'Ardelean', 'Marcel', 'amarcel29@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-10-20 20:35:44', '2017-10-20 23:35:44'),
(4, 'Avramoni', 'Adina', 'adina@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:48:39', '2017-11-27 22:08:44'),
(5, 'Bacter', 'Gabriela', 'gabi_bacter@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:48:39', '2017-11-27 22:08:44'),
(6, 'Badea', 'Rodica', 'badea.rodica@gmail.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:49:56', '2017-11-27 22:08:44'),
(7, 'Badiu', 'Ioan', 'badiu@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:49:56', '2017-11-27 22:08:44'),
(8, 'Basaraba', 'Gabriela', 'basarabag@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:51:14', '2017-11-27 22:08:44'),
(9, 'Bolosin', 'Maria', 'maria76_ro@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:51:14', '2017-11-27 22:08:44'),
(10, 'Bota', 'Cristian', 'criss77_b@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:52:00', '2017-11-27 22:08:44'),
(11, 'Bratu', 'Victor', 'victordebratu@gmail.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:53:24', '2017-11-27 22:08:44'),
(12, 'Bratu', 'Alina', 'alinabratu25@gmail.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:53:24', '2017-11-27 22:08:44'),
(13, 'Bunea', 'Teodora', 'dorabunea@yahoo.fr', '793741d54b00253006453742ad4ed534', '2017-11-23 11:54:39', '2017-11-27 22:08:44'),
(14, 'Cioara', 'Dana', 'danasafeinvest@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:54:39', '2017-11-27 22:08:44'),
(15, 'Cosa', 'Rodica', 'rodica_cosa@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:56:00', '2017-11-27 22:08:44'),
(16, 'Craciun', 'Raul', 'raul@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:56:00', '2017-11-27 22:08:44'),
(17, 'Damian', 'Adrian', 'adi_damian2008@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:57:36', '2017-11-27 22:08:44'),
(18, 'DeValerio', 'Marilena', 'marilena@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:57:36', '2017-11-27 22:08:44'),
(19, 'Filimon', 'Costel', 'cristi_filimon@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:58:41', '2017-11-27 22:08:44'),
(20, 'Florea', 'Neluta', 'neluta@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:58:41', '2017-11-27 22:08:44'),
(21, 'Florea', 'Anisoara', 'moisoni@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 11:59:07', '2017-11-27 22:08:44'),
(22, 'Florescu', 'Ioana', 'ioana.flrsc@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:02:46', '2017-11-27 22:08:44'),
(23, 'Floroni', 'Loredana', 'loredana_floroni@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:02:46', '2017-11-27 22:08:44'),
(24, 'Ghida', 'Romeo', 'adyy_ghyyda@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:03:53', '2017-11-27 22:08:44'),
(25, 'Ghiura', 'Anca', 'ghiura@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:03:53', '2017-11-27 22:08:44'),
(26, 'Ilioni', 'Manuela', 'ilionim@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:05:07', '2017-11-27 22:08:44'),
(27, 'Indrecan', 'Mihaela', 'indrecanmihaela@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:05:07', '2017-11-27 22:08:44'),
(28, 'Itul', 'Flaviu', 'itu_flaviu@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:06:09', '2017-11-27 22:08:44'),
(29, 'Iuga', 'Gabriel', 'gabiiuga2006@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:06:09', '2017-11-27 22:08:44'),
(30, 'Iuga', 'Aniela', 'iugaaniela@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:07:16', '2017-11-27 22:08:44'),
(31, 'Lupulescu', 'Carmen', 'carmenvioletalupulescu@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:07:16', '2017-11-27 22:08:44'),
(32, 'Marian', 'Marius', 'atisbuck@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:08:09', '2017-11-27 22:08:44'),
(33, 'Moga', 'Mirela', 'mirellamarcu@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:08:09', '2017-11-27 22:08:44'),
(34, 'Muntean', 'Cristina', 'cristimunt@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:09:23', '2017-11-27 22:08:44'),
(35, 'Muntean', 'Marius', 'munteanpmarius@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:09:23', '2017-11-27 22:08:44'),
(36, 'Muntean', 'Diana', 'dianastefoni@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:10:18', '2017-11-27 22:08:44'),
(37, 'Muntean', 'Iuliana', 'iulim04@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:10:18', '2017-11-27 22:08:44'),
(38, 'Neiconi', 'Liliana', 'neiconilil@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:11:24', '2017-11-27 22:08:44'),
(39, 'Neiconi', 'Cornelia', 'cornelianeiconi@yahoo.co.uk', '793741d54b00253006453742ad4ed534', '2017-11-23 15:11:24', '2017-11-27 22:08:44'),
(40, 'Nicoara', 'Adriana', 'dnicoara57@gmail.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:12:28', '2017-11-27 22:08:44'),
(41, 'Nicoara', 'Daniel', 'nicadialex@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:12:28', '2017-11-27 22:08:44'),
(42, 'Paraian', 'Ana-Maria', 'anamariaparaian@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:13:26', '2017-11-27 22:08:44'),
(43, 'Pascotescu', 'Camelia', 'cameliapascotescu@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:13:26', '2017-11-27 22:08:44'),
(44, 'Popa', 'Alina', 'popaalina@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:14:46', '2017-11-27 22:08:44'),
(45, 'Rosian', 'Lucian', 'rossian_lucian@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:14:46', '2017-11-27 22:08:44'),
(46, 'Sandor', 'Iozefina', 'sandori.prof@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:15:51', '2017-11-27 22:08:44'),
(47, 'Sbuchea', 'Liliana', 'lili72_ro@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:15:51', '2017-11-27 22:08:44'),
(48, 'Scorobete', 'Raluca', 'scorobete@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:16:58', '2017-11-27 22:08:44'),
(49, 'Sintimbrean', 'Simona', 'simonaoprinesc@gmail.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:16:58', '2017-11-27 22:08:44'),
(50, 'Socaci', 'Mihaela', 'socaci.mihaela@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:17:56', '2017-11-27 22:08:44'),
(51, 'Stanculesc', 'Diana', 'diana_stanculesc@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:17:56', '2017-11-27 22:08:44'),
(52, 'Stanculesc', 'Paul', 'paulst_s@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:18:56', '2017-11-27 22:08:44'),
(53, 'Stoica', 'Alina', 'sarmi69@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:18:56', '2017-11-27 22:08:44'),
(54, 'Susan', 'Daniela', 'susan@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:19:42', '2017-11-27 22:08:44'),
(55, 'Tamas', 'Bianca', 'bianca_tamas28@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:19:42', '2017-11-27 22:08:44'),
(56, 'Terlea', 'Violeta', 'terlea@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:20:46', '2017-11-27 22:08:44'),
(57, 'Tigaranu', 'Nicoleta', 'nicoletatigaranu@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:20:46', '2017-11-27 22:08:44'),
(58, 'Varhedi', 'Ionel', 'varhediionel@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:21:43', '2017-11-27 22:08:44'),
(59, 'Voda', 'Maria', 'voda@yahoo.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:21:43', '2017-11-27 22:08:44'),
(60, 'Zaharie', 'Iosif-Liziana', 'liziana.zaharie@gmail.com', '793741d54b00253006453742ad4ed534', '2017-11-23 15:22:17', '2017-11-27 22:08:44');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `sex` char(1) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Salvarea datelor din tabel `test`
--

INSERT INTO `test` (`id`, `nr_matricol`, `nume`, `prenume`, `email`, `sex`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(29, '29/AD1', 'Ciuca', 'Luis Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(30, '30/AD1', 'Crisan', 'Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(31, '31/AD1', 'Danciu', 'Andrea', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(32, '32/AD1', 'Dragotesc', 'Antonela Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(33, '33/AD3', 'Dumitresc', ' Carina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(34, '34/AD1', 'Filan', 'Ioana-Karina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(35, '35/AD1', 'Ganea', 'Lavinia Denisa', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(36, '36/AD1', 'Gherman', 'Razvan Petru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(37, '37/AD1', 'Goia', 'Robert Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(38, '38/AD1', 'Hoalga', 'Maria-Cristina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(39, '39/AD1', 'Istrate', 'Briana Alesia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(40, '40/AD1', 'Leja', 'Ana Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(41, '41/AD1', 'Mara', ' Andreas Emanuel', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(42, '42/AD1', 'Mituca', 'Bianca Antonia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(43, '43/AD1', 'Niculesc', 'Larissa Madalina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(44, '44/Ad1', 'Pascal', 'Vasile Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(45, '45/AD1', 'Petrescu', ' Ruxandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(46, '46/AD1', 'Popa', ' Marc Antonio', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(47, '47/AD1', 'Priala', ' Alin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(48, '48/AD1', 'Priala', ' David', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(49, '49/AD1', 'Romanesc', ' Denis Raul', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(50, '50/AD1', 'Scorobete', ' Filip Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(51, '51/AD1', 'Stefoni', ' Denisa Nicoleta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(52, '52/AD1', 'Tabirgic', 'Alexandra Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(53, '53/AD1', 'Ungur', 'Natalia-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(54, '54/AD1', 'Vitionesc', 'Denisa Naomi', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(55, '55/AD1', 'Vitionescu', 'Denisa Octavia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(56, '56/AD1', 'Voda', ' Adela Doinita', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(57, '57/AD1', 'Zanon', ' Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
