-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10 Noi 2017 la 21:32
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `catalogul`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `absente`
--

CREATE TABLE IF NOT EXISTS `absente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `nr_tot_abs` int(4) NOT NULL,
  `nr_abs_mot` int(4) NOT NULL,
  `sem` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nume_utilizator` varchar(20) NOT NULL,
  `parola` varchar(15) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Salvarea datelor din tabel `admin`
--

INSERT INTO `admin` (`id`, `nume`, `prenume`, `email`, `nume_utilizator`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Nicoara', 'Daniel', 'dnicoara57@gmail.com', 'Dan', 'dan12345', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Nicoara', 'Adriana', 'dalex997@hotmail.com', 'adissimo', 'adi12345', '2015-07-19 22:00:00', '2015-12-02 22:08:29');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `adresa`
--

CREATE TABLE IF NOT EXISTS `adresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) NOT NULL,
  `nr` varchar(8) NOT NULL,
  `bl` varchar(8) NOT NULL,
  `sc` varchar(4) NOT NULL,
  `ap` int(4) NOT NULL,
  `judetul` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `catalogabsente`
--

CREATE TABLE IF NOT EXISTS `catalogabsente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` char(5) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` char(9) NOT NULL,
  `absente` date NOT NULL,
  `motivata` tinyint(1) NOT NULL,
  `sem` char(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Salvarea datelor din tabel `catalogabsente`
--

INSERT INTO `catalogabsente` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `absente`, `motivata`, `sem`) VALUES
(1, 'IZ21', 1, 2, '2017-2018', '2017-10-04', 0, 'I'),
(2, 'IZ22', 2, 2, '2017-2018', '2017-10-10', 0, 'I'),
(3, 'IZ27', 3, 2, '2017-2018', '0000-00-00', 0, 'I'),
(4, 'IZ28', 4, 2, '2017-2018', '0000-00-00', 0, 'I'),
(5, 'IZ29', 5, 2, '2017-2018', '2017-10-04', 1, 'I'),
(6, 'IZ30', 1, 2, '2017-2018', '2017-10-05', 0, 'I'),
(7, 'IZ30', 10, 2, '2017-2018', '2017-09-21', 1, 'I'),
(8, 'IZ28', 4, 2, '2017-2018', '2017-10-14', 0, 'I'),
(9, 'IZ22', 10, 2, '2017-2018', '2017-10-02', 0, 'I'),
(10, 'IZ21', 10, 2, '2017-2018', '2017-10-08', 0, 'I'),
(11, 'IZ29', 4, 2, '2017-2018', '2017-11-02', 1, 'I'),
(12, 'IZ30', 10, 2, '2017-2018', '2017-10-24', 1, 'I'),
(13, 'IZ22', 4, 2, '2017-2018', '2017-10-24', 1, 'I'),
(14, 'IZ23', 4, 2, '2017-2018', '2017-10-24', 0, 'I'),
(15, 'IZ23', 4, 2, '2017-2018', '2017-10-24', 0, 'I'),
(16, 'IZ29', 4, 2, '2017-2018', '2017-10-24', 0, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `catalognote`
--

CREATE TABLE IF NOT EXISTS `catalognote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` char(5) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `data` date NOT NULL,
  `an_scolar` char(9) NOT NULL,
  `nota` float NOT NULL,
  `teza` int(1) NOT NULL,
  `sem` char(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Salvarea datelor din tabel `catalognote`
--

INSERT INTO `catalognote` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `data`, `an_scolar`, `nota`, `teza`, `sem`) VALUES
(1, 'IZ21', 1, 1, '2017-09-22', '2017-2018', 10, 0, 'I'),
(2, 'IZ22', 2, 2, '2017-10-02', '2017-2018', 10, 0, 'I'),
(3, 'IZ27', 3, 2, '2017-09-25', '2017-2018', 9, 0, 'I'),
(4, 'IZ28', 4, 2, '2017-10-10', '2017-2018', 8, 0, 'I'),
(5, 'IZ29', 4, 2, '2017-10-03', '2017-2018', 9, 0, 'I'),
(6, 'IZ30', 1, 2, '2017-10-12', '2017-2018', 9, 0, 'I'),
(28, 'IZ27', 4, 2, '2017-10-03', '2017-2018', 9, 0, 'I'),
(27, 'IZ29', 4, 2, '2017-10-09', '2017-2018', 8, 0, 'I'),
(26, 'IZ30', 10, 2, '2017-10-19', '2017-2018', 7, 0, 'I'),
(25, 'IZ28', 4, 2, '2017-10-19', '2017-2018', 8, 0, 'I'),
(24, 'IZ22', 10, 2, '2017-10-19', '2017-2018', 9, 0, 'I'),
(23, 'IZ21', 10, 1, '2017-10-19', '2017-2018', 10, 0, 'I'),
(22, 'IZ29', 5, 2, '2017-10-19', '2017-2018', 10, 0, 'I'),
(29, 'IZ29', 10, 2, '2017-10-24', '2017-2018', 6, 1, 'I'),
(30, 'IZ27', 3, 2, '2017-10-03', '2017-2018', 9, 1, 'I'),
(31, 'IZ27', 3, 2, '2017-10-02', '2017-2018', 8, 0, 'I'),
(32, 'IZ28', 4, 2, '2017-10-04', '2017-2018', 7, 0, 'I'),
(33, 'IZ27', 4, 2, '2017-10-05', '2017-2018', 5, 0, 'I'),
(34, 'IZ29', 4, 2, '2017-10-10', '2017-2018', 7, 0, 'I'),
(35, 'IZ28', 4, 2, '2017-10-19', '2017-2018', 9, 1, 'I'),
(36, 'IZ29', 10, 2, '2017-11-07', '2017-2018', 8, 0, 'I'),
(37, 'IZ29', 10, 2, '2017-11-03', '2017-2018', 10, 0, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `directori`
--

CREATE TABLE IF NOT EXISTS `directori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nume_utilizator` varchar(20) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Salvarea datelor din tabel `directori`
--

INSERT INTO `directori` (`id`, `nume`, `prenume`, `email`, `nume_utilizator`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Badea', 'Rodica', 'nicadialex@yahoo.com', 'Rodica', '0d801be376ab1ee7515702986ed03710', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Nicoara', 'Adriana', 'dalex997@hotmail.com', 'adissimo', 'adi12345', '2015-07-19 22:00:00', '2015-12-02 22:08:29');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `diriginti`
--

CREATE TABLE IF NOT EXISTS `diriginti` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_clasa` int(3) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nume_utilizator` varchar(20) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Salvarea datelor din tabel `diriginti`
--

INSERT INTO `diriginti` (`id`, `id_clasa`, `nume`, `prenume`, `email`, `nume_utilizator`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 2, 'Nicoara', 'Daniel', 'nicadialex@yahoo.com', 'Daniel', 'dan12345', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 1, 'Nicoara', 'Adriana', 'dalex997@hotmail.com', 'adissimo', 'adi12345', '2015-07-19 22:00:00', '2015-12-02 22:08:29');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `elevclasa`
--

CREATE TABLE IF NOT EXISTS `elevclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` char(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Salvarea datelor din tabel `elevclasa`
--

INSERT INTO `elevclasa` (`id`, `nr_matricol`, `id_clasa`, `an_scolar`) VALUES
(1, 'IZ29', 2, '2017-2018'),
(2, 'IZ30', 2, '2017-2018'),
(3, 'IZ27', 2, '2017-2018'),
(4, 'IZ22', 2, '2017-2018'),
(5, 'IZ23', 2, '2017-2018'),
(6, 'IZ21', 1, '2017-2018'),
(7, 'IZ28', 2, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `elevi`
--

CREATE TABLE IF NOT EXISTS `elevi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `sex` char(1) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Salvarea datelor din tabel `elevi`
--

INSERT INTO `elevi` (`id`, `nr_matricol`, `nume`, `prenume`, `email`, `sex`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'IZ21', 'Nicoara', 'Alexandra', 'nicadialex@yahoo.com', 'F', '0d801be376ab1ee7515702986ed03710', '2015-07-23 22:00:00', '2015-07-27 00:00:00'),
(2, 'IZ22', 'Nicoara', 'Adriana', 'adi@hotmail.com', 'F', 'c46335eb267e2e1cde5b017acb4cd799', '2015-07-26 22:00:00', '2015-07-27 00:00:00'),
(3, 'IZ27', 'Nicoara', 'Dan', 'dan.alexadru.nicoara@gmail.com', 'M', '9180b4da3f0c7e80975fad685f7f134e', '2015-07-26 22:00:00', '2015-07-27 00:00:00'),
(4, 'IZ28', 'Popescu', 'Radu', 'popescu@yahoo.com', 'M', 'b21afc54fb48d153c19101658f4a2a48', '2015-07-31 22:00:00', '2015-08-01 00:00:00'),
(5, 'IZ29', 'Munteanu ', 'Sergiu', 'sergiu.muntean@gmail.com', 'M', '9cae68addbd8ac69eb5ebf168d0d295e', '2015-08-13 13:26:59', '2015-08-13 15:26:59'),
(6, 'IZ30', 'Simon', 'Tiberiu', 'simon@yahoo.com', 'M', '5d2a7ba4090fdeabfd6176833522b0ab', '2015-08-20 19:54:44', '2015-08-20 21:54:44'),
(7, 'IZ23', 'Stanciu', 'Rebeca', 'stanciu@hotmail.com', 'F', 'eeeeeeeeeeeeee33333333333333', '2017-10-22 17:31:25', NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infodirectori`
--

CREATE TABLE IF NOT EXISTS `infodirectori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_director` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infodirectori`
--

INSERT INTO `infodirectori` (`id`, `fk_director`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infodiriginti`
--

CREATE TABLE IF NOT EXISTS `infodiriginti` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_diriginte` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infodiriginti`
--

INSERT INTO `infodiriginti` (`id`, `fk_diriginte`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(3, 3, 'M', 'politehnica', '072549396', 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', 0, '2015-08-21 21:24:38', '2015-08-21 19:24:38'),
(4, 8, 'F', 'postliceale', '0745678908', 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', 0, '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 7, 'M', 'Universitare', '', 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', 0, '2015-10-11 21:37:53', '2015-10-11 19:37:53');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infoelevi`
--

CREATE TABLE IF NOT EXISTS `infoelevi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `tata` varchar(20) NOT NULL,
  `mama` varchar(20) NOT NULL,
  `ocupatie_mama` varchar(20) DEFAULT NULL,
  `ocupatie_tata` varchar(20) DEFAULT NULL,
  `religie` varchar(15) NOT NULL,
  `naveta` varchar(2) NOT NULL DEFAULT 'nu',
  `nr_frati` int(2) NOT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `cod_num` varchar(13) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Salvarea datelor din tabel `infoelevi`
--

INSERT INTO `infoelevi` (`id`, `nr_matricol`, `telefon`, `tata`, `mama`, `ocupatie_mama`, `ocupatie_tata`, `religie`, `naveta`, `nr_frati`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `cod_num`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 'IZ21', '0722549396', '', '', 'profesor', 'profesor', '', '', 0, 'Hateg', 'Sarmizegetusa', '8', '19-B', 'A', '5', 'Hunedoara', 'HD', '688830', '1570521222030', '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 'IZ22', '07317668877', '', '', 'profesor', 'inginer', '', '', 0, 'Hateg', 'Sarmizegetusa', '8', '19-B', 'A', '5', 'Hunedoara', 'HD', '688831', '', '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(3, 'IZ27', '072549396', '', '', 'profesor', 'profesor', '', '', 0, 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', '2015-08-21 21:24:38', '2015-08-21 19:24:38'),
(4, 'IZ28', '0745678908', '', '', 'pensionar', 'pensionar', '', '', 0, 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 'IZ29', '0734565678', 'Andrei', 'Ana', 'profesor', 'inginer', '', '', 0, 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', '2015-10-11 21:37:53', '2015-10-11 19:37:53'),
(6, 'IZ30', '0734567893', 'Andru', 'Ana', 'asistent medical', 'muncitor', 'ortodoxa', 'nu', 2, 'Hateg', 'Sarmizegetusa', '5', '17-B', 'A', '3', 'Hunedoara', 'HD', '345678', '1570521222030', NULL, '2017-11-09 15:03:27'),
(7, 'IZ23', '0734567543', 'Marian', 'Maria', 'vanzator', 'constructor', 'ortodox', 'nu', 1, 'Paclisa', 'principala', '2', NULL, NULL, NULL, 'Hunedoara', 'HD', '235678', '1234565432345', NULL, '2017-11-09 15:03:27');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infoprofesori`
--

CREATE TABLE IF NOT EXISTS `infoprofesori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_profesor` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infoprofesori`
--

INSERT INTO `infoprofesori` (`id`, `fk_profesor`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(3, 3, 'M', 'politehnica', '072549396', 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', 0, '2015-08-21 21:24:38', '2015-08-21 19:24:38'),
(4, 8, 'F', 'postliceale', '0745678908', 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', 0, '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 7, 'M', 'Universitare', '', 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', 0, '2015-10-11 21:37:53', '2015-10-11 19:37:53');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `listaclase`
--

CREATE TABLE IF NOT EXISTS `listaclase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Clasa` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Salvarea datelor din tabel `listaclase`
--

INSERT INTO `listaclase` (`id`, `Clasa`) VALUES
(1, 'IX-A'),
(2, 'IX-B'),
(3, 'IX-C'),
(4, 'IX-D'),
(5, 'IX-E'),
(6, 'IX-F'),
(7, 'IX-G'),
(8, 'X-A'),
(9, 'X-B'),
(10, 'X-C'),
(11, 'X-D'),
(12, 'X-E'),
(13, 'X-F'),
(14, 'X-G'),
(15, 'XI-A'),
(16, 'XI-B'),
(17, 'XI-C'),
(18, 'XI-D'),
(19, 'XI-E'),
(20, 'XI-F'),
(21, 'XI-G'),
(22, 'XII-A'),
(25, 'XII-B'),
(26, 'XII-C'),
(27, 'XII-D'),
(28, 'XII-E'),
(29, 'XII-G'),
(30, 'V'),
(31, 'VI'),
(32, 'VII'),
(33, 'VIII');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `listamaterii`
--

CREATE TABLE IF NOT EXISTS `listamaterii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia` char(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Salvarea datelor din tabel `listamaterii`
--

INSERT INTO `listamaterii` (`id`, `materia`) VALUES
(1, 'Engleza'),
(2, 'Franceza'),
(3, 'Romana'),
(4, 'Matematica'),
(5, 'Fizica'),
(6, 'Chimie'),
(7, 'Biologie'),
(8, 'Istorie'),
(9, 'Geografie'),
(10, 'Informatica'),
(11, 'Economie'),
(12, 'Filosofie'),
(13, 'Religie'),
(14, 'Logica'),
(15, 'Sport'),
(16, 'Desen'),
(17, 'Purtare'),
(18, 'Tic');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `materiiclasa`
--

CREATE TABLE IF NOT EXISTS `materiiclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Salvarea datelor din tabel `materiiclasa`
--

INSERT INTO `materiiclasa` (`id`, `id_materie`, `id_clasa`, `an_scolar`) VALUES
(1, 1, 2, '2017-2018'),
(2, 2, 2, '2017-2018'),
(3, 3, 2, '2017-2018'),
(4, 4, 2, '2017-2018'),
(5, 5, 2, '2017-2018'),
(6, 6, 2, '2017-2018'),
(7, 7, 2, '2017-2018'),
(8, 8, 2, '2017-2018'),
(9, 9, 2, '2017-2018'),
(10, 10, 2, '2017-2018'),
(11, 1, 1, '2017-2018'),
(12, 2, 1, '2017-2018'),
(13, 3, 1, '2017-2018'),
(14, 4, 1, '2017-2018'),
(15, 5, 1, '2017-2018'),
(16, 6, 1, '2017-2018'),
(17, 7, 1, '2017-2018'),
(18, 8, 1, '2017-2018'),
(19, 9, 1, '2017-2018'),
(20, 10, 1, '2017-2018'),
(21, 11, 1, '2017-2018'),
(22, 12, 1, '2017-2018'),
(23, 13, 1, '2017-2018'),
(24, 14, 1, '2017-2018'),
(25, 15, 1, '2017-2018'),
(26, 16, 1, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mediigenerale`
--

CREATE TABLE IF NOT EXISTS `mediigenerale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(5) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `media` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Salvarea datelor din tabel `mediigenerale`
--

INSERT INTO `mediigenerale` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `media`) VALUES
(1, 'IZ29', 4, 2, '2017-2018', 7.5),
(2, 'IZ29', 10, 2, '2017-2018', 8.62),
(3, 'IZ28', 2, 2, '2017-2018', 8.5);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mediisemestriale`
--

CREATE TABLE IF NOT EXISTS `mediisemestriale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(5) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `media` float NOT NULL DEFAULT '0',
  `sem` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Salvarea datelor din tabel `mediisemestriale`
--

INSERT INTO `mediisemestriale` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `media`, `sem`) VALUES
(1, 'IZ29', 4, 2, '2017-2018', 8, 'I'),
(2, 'IZ29', 10, 2, '2017-2018', 8, 'I'),
(3, 'IZ28', 4, 2, '2017-2018', 8, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `profesorclasa`
--

CREATE TABLE IF NOT EXISTS `profesorclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profesor` int(2) NOT NULL,
  `id_clasa` int(2) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Salvarea datelor din tabel `profesorclasa`
--

INSERT INTO `profesorclasa` (`id`, `id_profesor`, `id_clasa`, `id_materie`, `an_scolar`) VALUES
(1, 1, 2, 4, '2017-2018'),
(2, 1, 3, 4, '2017-2018'),
(3, 1, 16, 4, '2017-2018'),
(4, 1, 22, 4, '2017-2018'),
(5, 1, 2, 10, '2017-2018'),
(6, 2, 2, 4, '2017-2018'),
(7, 3, 2, 9, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `profesori`
--

CREATE TABLE IF NOT EXISTS `profesori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `profesori`
--

INSERT INTO `profesori` (`id`, `nume`, `prenume`, `email`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Nicoara', 'Daniel', 'nicadialex@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Nicoara', 'Adriana', 'dalex997@hotmail.com', '0d801be376ab1ee7515702986ed03710', '2015-07-19 22:00:00', '2015-12-02 22:08:29'),
(3, 'Simon', 'Tibi', 'simon@yahoo.com', 'tibi', '2017-10-20 20:35:44', '2017-10-20 23:35:44');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
