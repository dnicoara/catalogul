-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26 Noi 2017 la 21:11
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `catalogul`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `absente`
--

CREATE TABLE IF NOT EXISTS `absente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `nr_tot_abs` int(4) NOT NULL,
  `nr_abs_mot` int(4) NOT NULL,
  `sem` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nume_utilizator` varchar(20) NOT NULL,
  `parola` varchar(15) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Salvarea datelor din tabel `admin`
--

INSERT INTO `admin` (`id`, `nume`, `prenume`, `email`, `nume_utilizator`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Nicoara', 'Daniel', 'dnicoara57@gmail.com', 'Dan', 'dan12345', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Nicoara', 'Adriana', 'dalex997@hotmail.com', 'adissimo', 'adi12345', '2015-07-19 22:00:00', '2015-12-02 22:08:29');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `adresa`
--

CREATE TABLE IF NOT EXISTS `adresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) NOT NULL,
  `nr` varchar(8) NOT NULL,
  `bl` varchar(8) NOT NULL,
  `sc` varchar(4) NOT NULL,
  `ap` int(4) NOT NULL,
  `judetul` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `catalogabsente`
--

CREATE TABLE IF NOT EXISTS `catalogabsente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` char(10) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` char(9) NOT NULL,
  `absente` date NOT NULL,
  `motivata` tinyint(1) NOT NULL,
  `sem` char(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Salvarea datelor din tabel `catalogabsente`
--

INSERT INTO `catalogabsente` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `absente`, `motivata`, `sem`) VALUES
(1, 'IZ21', 1, 2, '2017-2018', '2017-10-04', 0, 'I'),
(2, 'IZ22', 17, 2, '2017-2018', '2017-10-10', 1, 'I'),
(3, 'IZ27', 3, 2, '2017-2018', '2017-11-07', 1, 'I'),
(4, 'IZ28', 4, 2, '2017-2018', '2017-11-09', 0, 'I'),
(5, 'IZ29', 5, 2, '2017-2018', '2017-10-04', 1, 'I'),
(6, 'IZ30', 1, 2, '2017-2018', '2017-10-05', 0, 'I'),
(7, 'IZ30', 10, 2, '2017-2018', '2017-09-21', 1, 'I'),
(8, 'IZ28', 4, 2, '2017-2018', '2017-10-14', 0, 'I'),
(9, 'IZ22', 10, 2, '2017-2018', '2017-10-02', 0, 'I'),
(10, 'IZ21', 10, 2, '2017-2018', '2017-10-08', 0, 'I'),
(11, 'IZ29', 4, 2, '2017-2018', '2017-11-02', 1, 'I'),
(12, 'IZ30', 10, 2, '2017-2018', '2017-10-24', 1, 'I'),
(13, 'IZ22', 4, 2, '2017-2018', '2017-10-24', 1, 'I'),
(14, 'IZ23', 4, 2, '2017-2018', '2017-10-24', 0, 'I'),
(15, 'IZ23', 4, 2, '2017-2018', '2017-10-24', 0, 'I'),
(16, 'IZ29', 4, 2, '2017-2018', '2017-10-24', 0, 'I'),
(17, 'IZ29', 4, 2, '2017-2018', '2017-11-12', 0, 'I'),
(18, 'IZ29', 4, 2, '2017-2018', '2017-11-13', 0, 'I'),
(21, 'IZ27', 10, 2, '2017-2018', '2017-11-13', 1, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `catalognote`
--

CREATE TABLE IF NOT EXISTS `catalognote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` char(10) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `data` date NOT NULL,
  `an_scolar` char(9) NOT NULL,
  `nota` int(2) NOT NULL,
  `teza` int(1) NOT NULL,
  `sem` char(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Salvarea datelor din tabel `catalognote`
--

INSERT INTO `catalognote` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `data`, `an_scolar`, `nota`, `teza`, `sem`) VALUES
(1, '30/AD1', 1, 1, '2017-09-22', '2017-2018', 10, 0, 'I'),
(2, '33/AD1', 2, 2, '2017-10-02', '2017-2018', 10, 0, 'I'),
(3, '32/AD1', 3, 2, '2017-09-25', '2017-2018', 9, 0, 'I'),
(4, '31/AD1', 4, 2, '2017-10-10', '2017-2018', 8, 0, 'I'),
(5, '29/AD1', 4, 2, '2017-10-03', '2017-2018', 9, 0, 'I'),
(6, '34/AD1', 1, 2, '2017-10-12', '2017-2018', 9, 0, 'I'),
(28, '32/AD1', 4, 2, '2017-10-03', '2017-2018', 9, 0, 'I'),
(27, '29/AD1', 4, 2, '2017-10-09', '2017-2018', 8, 0, 'I'),
(26, '34/AD1', 10, 2, '2017-10-19', '2017-2018', 7, 0, 'I'),
(25, '31/AD1', 4, 2, '2017-10-19', '2017-2018', 8, 0, 'I'),
(24, '33/AD1', 10, 2, '2017-10-19', '2017-2018', 9, 0, 'I'),
(23, '30/AD1', 10, 1, '2017-10-19', '2017-2018', 10, 0, 'I'),
(22, '29/AD1', 5, 2, '2017-10-19', '2017-2018', 10, 0, 'I'),
(29, '29/AD1', 10, 2, '2017-10-24', '2017-2018', 6, 1, 'I'),
(30, '32/AD1', 3, 2, '2017-10-03', '2017-2018', 9, 1, 'I'),
(31, '32/AD1', 3, 2, '2017-10-02', '2017-2018', 8, 0, 'I'),
(32, '31/AD1', 4, 2, '2017-10-04', '2017-2018', 7, 0, 'I'),
(33, '32/AD1', 4, 2, '2017-10-05', '2017-2018', 5, 0, 'I'),
(34, '29/AD1', 4, 2, '2017-10-10', '2017-2018', 7, 0, 'I'),
(35, '31/AD1', 4, 2, '2017-10-19', '2017-2018', 9, 1, 'I'),
(36, '29/AD1', 10, 2, '2017-11-07', '2017-2018', 8, 0, 'I'),
(37, '29/AD1', 10, 2, '2017-11-03', '2017-2018', 10, 0, 'I'),
(38, '34/AD1', 10, 2, '2017-11-13', '2017-2018', 7, 0, 'I'),
(39, '29/AD1', 4, 2, '2017-11-26', '2017-2018', 6, 0, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `directori`
--

CREATE TABLE IF NOT EXISTS `directori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nume_utilizator` varchar(20) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Salvarea datelor din tabel `directori`
--

INSERT INTO `directori` (`id`, `nume`, `prenume`, `email`, `nume_utilizator`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Badea', 'Rodica', 'nicadialex@yahoo.com', 'Rodica', '0d801be376ab1ee7515702986ed03710', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Nicoara', 'Adriana', 'dalex997@hotmail.com', 'adissimo', 'adi12345', '2015-07-19 22:00:00', '2015-12-02 22:08:29');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `diriginti`
--

CREATE TABLE IF NOT EXISTS `diriginti` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_clasa` int(3) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Salvarea datelor din tabel `diriginti`
--

INSERT INTO `diriginti` (`id`, `id_clasa`, `nume`, `prenume`, `email`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 1, 'Indrecan', 'Mihaela', 'mihaela@yahoo.com', 'dan12345', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 2, 'Nicoara', 'Daniel', 'nicadialex@yahoo.com', 'dan12345', '2015-07-19 22:00:00', '2015-12-02 22:08:29'),
(3, 3, 'Floroni', 'Loredana', 'floroni@yahoo.com', 'floroni', '2017-11-19 16:55:08', '2017-11-19 18:55:08'),
(4, 4, 'Lupulescu', 'Carmen', 'lupulescu@yahoo.com', 'carmen', '2017-11-19 17:11:48', '2017-11-19 20:07:47');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `elevclasa`
--

CREATE TABLE IF NOT EXISTS `elevclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` char(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=114 ;

--
-- Salvarea datelor din tabel `elevclasa`
--

INSERT INTO `elevclasa` (`id`, `nr_matricol`, `id_clasa`, `an_scolar`) VALUES
(1, '1/AD1', 1, '2017-2018'),
(2, '2/AD1', 1, '2017-2018'),
(3, '3/AD1', 1, '2017-2018'),
(4, '4/AD1', 1, '2017-2018'),
(5, '5/AD1', 1, '2017-2018'),
(6, '6/AD1', 1, '2017-2018'),
(7, '7/AD1', 1, '2017-2018'),
(8, '8/AD1', 1, '2017-2018'),
(9, '9/AD1', 1, '2017-2018'),
(10, '10/AD1', 1, '2017-2018'),
(12, '12/AD1', 1, '2017-2018'),
(11, '11/AD1', 1, '2017-2018'),
(13, '13/AD1', 1, '2017-2018'),
(14, '14/AD1', 1, '2017-2018'),
(15, '15/AD1', 1, '2017-2018'),
(16, '16/AD1', 1, '2017-2018'),
(17, '17/AD1', 1, '2017-2018'),
(18, '18/AD1', 1, '2017-2018'),
(19, '19/AD1', 1, '2017-2018'),
(20, '20/AD1', 1, '2017-2018'),
(21, '21/AD1', 1, '2017-2018'),
(22, '22/AD1', 1, '2017-2018'),
(23, '23/AD1', 1, '2017-2018'),
(24, '24/AD1', 1, '2017-2018'),
(25, '25/AD1', 1, '2017-2018'),
(26, '26/AD1', 1, '2017-2018'),
(27, '27/AD1', 1, '2017-2018'),
(28, '28/AD1', 1, '2017-2018'),
(29, '29/AD1', 2, '2017-2018'),
(30, '30/AD1', 2, '2017-2018'),
(31, '31/AD1', 2, '2017-2018'),
(32, '32/AD1', 2, '2017-2018'),
(33, '33/AD1', 2, '2017-2018'),
(34, '34/AD1', 2, '2017-2018'),
(35, '35/AD1', 2, '2017-2018'),
(36, '36/AD1', 2, '2017-2018'),
(37, '37/AD1', 2, '2017-2018'),
(38, '38/AD1', 2, '2017-2018'),
(39, '39/AD1', 2, '2017-2018'),
(40, '40/AD1', 2, '2017-2018'),
(41, '41/AD1', 2, '2017-2018'),
(42, '42/AD1', 2, '2017-2018'),
(43, '43/AD1', 2, '2017-2018'),
(44, '44/Ad1', 2, '2017-2018'),
(45, '45/AD1', 2, '2017-2018'),
(46, '46/AD1', 2, '2017-2018'),
(47, '47/AD1', 2, '2017-2018'),
(48, '48/AD1', 2, '2017-2018'),
(49, '49/AD1', 2, '2017-2018'),
(50, '50/AD1', 2, '2017-2018'),
(51, '51/AD1', 2, '2017-2018'),
(52, '52/AD1', 2, '2017-2018'),
(53, '53/AD1', 2, '2017-2018'),
(54, '54/AD1', 2, '2017-2018'),
(55, '55/AD1', 2, '2017-2018'),
(56, '56/AD1', 2, '2017-2018'),
(57, '57/AD1', 2, '2017-2018'),
(58, '58/AD1', 3, '2017-2018'),
(59, '59/AD1', 3, '2017-2018'),
(60, '60/AD1', 3, '2017-2018'),
(61, '61/AD1', 3, '2017-2018'),
(62, '62/AD1', 3, '2017-2018'),
(63, '63/AD1', 3, '2017-2018'),
(64, '64/AD1', 3, '2017-2018'),
(65, '65/AD1', 3, '2017-2018'),
(66, '66/AD1', 3, '2017-2018'),
(67, '67/AD1', 3, '2017-2018'),
(68, '68/AD1', 3, '2017-2018'),
(69, '69/AD1', 3, '2017-2018'),
(70, '70/AD1', 3, '2017-2018'),
(71, '71/AD1', 3, '2017-2018'),
(72, '72/AD1', 3, '2017-2018'),
(73, '73/AD1', 3, '2017-2018'),
(74, '74/AD1', 3, '2017-2018'),
(75, '75/AD1', 3, '2017-2018'),
(76, '76/AD1', 3, '2017-2018'),
(77, '77/AD1', 3, '2017-2018'),
(78, '78/AD1', 3, '2017-2018'),
(79, '79/AD1', 3, '2017-2018'),
(80, '80/AD1', 3, '2017-2018'),
(81, '81/AD1', 3, '2017-2018'),
(82, '82/AD1', 3, '2017-2018'),
(83, '83/AD1', 3, '2017-2018'),
(84, '84/AD1', 3, '2017-2018'),
(85, '85/AD1', 4, '2017-2018'),
(86, '86/AD1', 4, '2017-2018'),
(87, '87/AD1', 4, '2017-2018'),
(88, '88/AD1', 4, '2017-2018'),
(89, '89/AD1', 4, '2017-2018'),
(90, '90/AD1', 4, '2017-2018'),
(91, '91/AD1', 4, '2017-2018'),
(92, '92/AD1', 4, '2017-2018'),
(93, '93/AD1', 4, '2017-2018'),
(94, '94/AD1', 4, '2017-2018'),
(95, '95/AD1', 4, '2017-2018'),
(96, '96/AD1', 4, '2017-2018'),
(97, '97/AD1', 4, '2017-2018'),
(98, '98/AD1', 4, '2017-2018'),
(99, '99/AD1', 4, '2017-2018'),
(100, '100/AD1', 4, '2017-2018'),
(101, '101/AD1', 4, '2017-2018'),
(102, '102/AD1', 4, '2017-2018'),
(103, '103/AD1', 4, '2017-2018'),
(104, '104/AD1', 4, '2017-2018'),
(105, '105/AD1', 4, '2017-2018'),
(106, '106/AD1', 4, '2017-2018'),
(107, '107/AD1', 4, '2017-2018'),
(108, '108/AD1', 4, '2017-2018'),
(109, '109/AD1', 4, '2017-2018'),
(110, '110/AD1', 4, '2017-2018'),
(111, '111/AD1', 4, '2017-2018'),
(112, '112/AD1', 4, '2017-2018'),
(113, '113/AD1', 4, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `elevi`
--

CREATE TABLE IF NOT EXISTS `elevi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `sex` char(1) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=114 ;

--
-- Salvarea datelor din tabel `elevi`
--

INSERT INTO `elevi` (`id`, `nr_matricol`, `nume`, `prenume`, `email`, `sex`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, '1/AD1', 'Baluse', 'Adrian-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-23 22:00:00', '2017-11-20 17:45:29'),
(2, '2/AD1', 'Batuca', 'Dragos-Sebastian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-26 22:00:00', '2017-11-12 14:59:42'),
(3, '3/AD1', 'Butoi', 'Rebeca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-07-26 22:00:00', '2017-11-12 15:02:28'),
(4, '4/AD1', 'Costache', 'Mihai-Mihnea', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-31 22:00:00', '2015-08-01 00:00:00'),
(5, '5/AD1', 'Crai', 'Sorinela-Lorena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-08-13 13:26:59', '2015-08-13 15:26:59'),
(6, '6/AD1', 'Cristescu', 'Iulia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-08-20 19:54:44', '2017-11-12 14:43:31'),
(7, '7/AD1', 'David', 'Simonel-Olimpiu', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-12 13:21:00', '2017-11-15 00:00:00'),
(8, '8/AD1', 'Dragoi', 'Andrei-Laurentiu', 'angelica@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-18 21:28:46', '2017-11-20 19:34:26'),
(9, '9/AD1', 'Filan', 'Paul-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-18 22:15:06', '2017-11-20 21:03:43'),
(10, '10/AD1', 'Fuca', 'Razvzn-Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-19 13:45:03', '2017-11-19 19:46:50'),
(11, '11/AD1', 'Fulea', 'Stefania-Yasmine', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:45:45', NULL),
(12, '12/AD1', 'Gardean', 'Alexandru-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:45:45', NULL),
(13, '13/AD1', 'Gherghina', 'Darius-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:49:45', NULL),
(14, '14/AD1', 'Ghita', 'Georgiana-Miriam', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:49:45', NULL),
(15, '15/AD1', 'Harangus', 'Robert-Adrian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:52:06', NULL),
(16, '16/AD1', 'Iovaneasa', 'Flavia-Giorgiana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:52:06', NULL),
(17, '17/AD1', 'Josan', 'Gelu-Samuel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:55:00', NULL),
(18, '18/AD1', 'Mezinesc', 'Adelina-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:55:00', NULL),
(19, '19/AD1', 'Mistau', 'Traian-Razvan', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:57:01', NULL),
(20, '20/AD1', 'Muntean', 'David-Mirel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:57:01', NULL),
(21, '21/AD1', 'Murgoi', 'Ionela-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:58:31', NULL),
(22, '22/AD1', 'Nadasan', 'Victor', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:58:31', NULL),
(23, '23/AD1', 'Nedelea', 'Igori', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:00:23', NULL),
(24, '24/AD1', 'Olarescu', 'Sergiu-Robert', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:00:23', NULL),
(25, '25/AD1', 'Onescu', 'Andeea-Ancuta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:02:19', NULL),
(26, '26/AD1', 'Oparlescu', 'Dina-Lidia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:02:19', NULL),
(27, '27/AD1', 'Popescu', 'Sergiu-Catalin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', NULL),
(28, '28/AD1', 'Tirea', 'Delia-Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', NULL),
(29, '29/AD1', 'Ciuca', 'Luis Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(30, '30/AD1', 'Crisan', 'Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(31, '31/AD1', 'Danciu', 'Andrea', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(32, '32/AD1', 'Dragotesc', 'Antonela Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(33, '33/AD1', 'Dumitresc', ' Carina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(34, '34/AD1', 'Filan', 'Ioana-Karina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(35, '35/AD1', 'Ganea', 'Lavinia Denisa', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(36, '36/AD1', 'Gherman', 'Razvan Petru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(37, '37/AD1', 'Goia', 'Robert Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(38, '38/AD1', 'Hoalga', 'Maria-Cristina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(39, '39/AD1', 'Istrate', 'Briana Alesia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(40, '40/AD1', 'Leja', 'Ana Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(41, '41/AD1', 'Mara', ' Andreas Emanuel', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(42, '42/AD1', 'Mituca', 'Bianca Antonia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(43, '43/AD1', 'Niculesc', 'Larissa Madalina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(44, '44/AD1', 'Pascal', 'Vasile Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(45, '45/AD1', 'Petrescu', ' Ruxandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(46, '46/AD1', 'Popa', ' Marc Antonio', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(47, '47/AD1', 'Priala', ' Alin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(48, '48/AD1', 'Priala', ' David', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(49, '49/AD1', 'Romanesc', ' Denis Raul', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(50, '50/AD1', 'Scorobete', ' Filip Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(51, '51/AD1', 'Stefoni', ' Denisa Nicoleta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(52, '52/AD1', 'Tabirgic', 'Alexandra Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(53, '53/AD1', 'Ungur', 'Natalia-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(54, '54/AD1', 'Vitionesc', 'Denisa Naomi', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(55, '55/AD1', 'Vitionescu', 'Denisa Octavia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(56, '56/AD1', 'Voda', ' Adela Doinita', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(57, '57/AD1', 'Zanon', ' Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(58, '58/AD1', 'Alb', 'Simina-Magdalena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:05:23', NULL),
(59, '59/AD1', 'Alboni', 'Andrei-Sebastian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:05:23', NULL),
(60, '60/AD1', 'Andrei', 'Daniel-Sebastian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:07:38', NULL),
(61, '61/AD1', 'Arimescu', 'Darius-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:07:38', NULL),
(62, '62/AD1', 'Bojin', 'Andrada-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:08:11', NULL),
(63, '63/AD1', 'Botoni', 'Letisia-Alexandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:11:29', NULL),
(64, '64/AD1', 'Bucur', 'Murgoi-Rares-Danut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:11:29', NULL),
(65, '65/AD1', 'Cretu', 'Giorgiana-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:13:39', NULL),
(66, '66/AD1', 'Damian', 'Emanuel-David', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:13:39', NULL),
(67, '67/AD1', 'Gheorma', 'Carmen-Ariana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:15:36', NULL),
(68, '68/AD1', 'Hibais', 'Alina-Dorina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:15:36', NULL),
(69, '69/AD1', 'Manincea', 'Izabela-Alexandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:17:52', NULL),
(70, '70/AD1', 'Martin', 'Emanuela-Monica', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:17:52', NULL),
(71, '71/AD1', 'Merian', 'Constantin-Darius', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:19:46', NULL),
(72, '72/AD1', 'Micsoni', 'Raul-Ciprian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:19:46', NULL),
(73, '73/AD1', 'Muraru', 'Mihai-Lucian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:21:36', NULL),
(74, '74/AD1', 'Onica', 'Izabela-Lorena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:21:36', NULL),
(75, '75/AD1', 'Orasan', 'Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:23:33', NULL),
(76, '76/AD1', 'Paulescu', 'Robert-Alin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:23:33', NULL),
(77, '77/AD1', 'Pelin', 'Rares', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:25:01', NULL),
(78, '78/AD1', 'Sabau', 'Raul-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:25:01', NULL),
(79, '79/AD1', 'Salasan', 'Daniel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:26:32', NULL),
(80, '80/AD1', 'Sauca', 'Darius-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:26:32', NULL),
(81, '81/AD1', 'Terlea', 'Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:28:13', NULL),
(82, '82/AD1', 'Todoni', 'Eugenia-Raluca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:28:13', NULL),
(83, '83/AD1', 'Todoran', 'Ariana-Maria-Irina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:30:15', NULL),
(84, '84/AD1', 'Voian', 'Iosua-Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:30:15', NULL),
(85, '85/AD1', 'Avieritei', 'Daniela-Florentina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:49:30', NULL),
(86, '86/AD1', 'Avramescu', 'Bianca-Rebeca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 20:49:30', NULL),
(87, '87/AD1', 'Beuca', 'Leia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:13:11', NULL),
(88, '88/AD1', 'Beuca', 'Lorena-Timeia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:13:11', NULL),
(89, '89/AD1', 'Bolovaneanu', 'Lorena-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:14:48', NULL),
(90, '90/AD1', 'Burtoi', 'Simina-Paraschiva', 'e', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:14:48', NULL),
(91, '91/AD1', 'Dinisoni', 'Florina-Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:16:28', NULL),
(92, '92/AD1', 'Filip', 'Maria-Irina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:16:28', NULL),
(93, '93/AD1', 'Gardean', 'Flavius-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:18:39', NULL),
(94, '94/AD1', 'Gardean', 'Alexandru-Silvestru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:18:39', NULL),
(95, '95/AD1', 'Haidaciuc', 'Flavia-Miriam', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:20:48', NULL),
(96, '96/AD1', 'Hrezdac', 'Andreea-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:20:48', NULL),
(97, '97/AD1', 'Iancu', 'Marian-Stelian', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:22:32', NULL),
(98, '98/AD1', 'Iosivoni', 'Roxana-Alexandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:22:32', NULL),
(99, '99/AD1', 'Jurj', 'Marius-Gabriel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:24:20', NULL),
(100, '100/AD1', 'Lut', 'Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:24:20', NULL),
(101, '101/AD1', 'Mardan', 'Elena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:26:41', NULL),
(102, '102/AD1', 'Meleru', 'Amalia-Nicola', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:26:41', NULL),
(103, '103/AD1', 'Munteanu', 'Eduard-Adrian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:28:15', NULL),
(104, '104/AD1', 'Neacsu', 'Simina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:28:15', NULL),
(105, '105/AD1', 'Onita', 'Lorena-Simona', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:30:17', NULL),
(106, '106/AD1', 'Opric', 'Raul-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:30:17', NULL),
(107, '107/AD1', 'Pusdrea', 'Andrei-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:31:56', NULL),
(108, '108/AD1', 'Pusca', 'Bianca-Geanina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:31:56', NULL),
(109, '109/AD1', 'Sucioni', 'Daniel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:33:14', NULL),
(110, '110/AD1', 'Sucioni', 'Gabriel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:33:14', NULL),
(111, '111/AD1', 'Stefoni', 'Petronela-Eugenia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:34:54', NULL),
(112, '112/AD1', 'Toncea', 'Ludovica-Mariana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:34:54', NULL),
(113, '113/AD1', 'Virvoniu', 'Daniel-Iosif', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-23 11:35:49', NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `elevi2`
--

CREATE TABLE IF NOT EXISTS `elevi2` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `sex` char(1) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Salvarea datelor din tabel `elevi2`
--

INSERT INTO `elevi2` (`id`, `nr_matricol`, `nume`, `prenume`, `email`, `sex`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, '1/AD1', 'Baluse', 'Adrian-Alexandru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-23 22:00:00', '2017-11-20 17:45:29'),
(2, '2/AD1', 'Batuca', 'Dragos-Sebastian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-26 22:00:00', '2017-11-12 14:59:42'),
(3, '3/AD1', 'Butoi', 'Rebeca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-07-26 22:00:00', '2017-11-12 15:02:28'),
(4, '4/AD4', 'Costache', 'Mihai-Mihnea', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2015-07-31 22:00:00', '2015-08-01 00:00:00'),
(5, '5/AD1', 'Crai', 'Sorinela-Lorena', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-08-13 13:26:59', '2015-08-13 15:26:59'),
(6, '6/AD1', 'Cristescu', 'Iulia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2015-08-20 19:54:44', '2017-11-12 14:43:31'),
(7, '7/AD1', 'David', 'Simonel-Olimpiu', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-12 13:21:00', '2017-11-15 00:00:00'),
(8, '8/AD1', 'Dragoi', 'Andrei-Laurentiu', 'angelica@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-18 21:28:46', '2017-11-20 19:34:26'),
(9, '9/AD1', 'Filan', 'Paul-Mihai', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-18 22:15:06', '2017-11-20 21:03:43'),
(10, '10/AD1', 'Fuca', 'Razvzn-Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-19 13:45:03', '2017-11-19 19:46:50'),
(11, '11/AD1', 'Fulea', 'Stefania-Yasmine', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:45:45', NULL),
(12, '12/AD1', 'Gardean', 'Alexandru-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:45:45', NULL),
(13, '13/AD1', 'Gherghina', 'Darius-Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:49:45', NULL),
(14, '14/AD1', 'Ghita', 'Georgiana-Miriam', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:49:45', NULL),
(15, '15/AD1', 'Harangus', 'Robert-Adrian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:52:06', NULL),
(16, '16/AD1', 'Iovaneasa', 'Flavia-Giorgiana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:52:06', NULL),
(17, '17/AD1', 'Josan', 'Gelu-Samuel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:55:00', NULL),
(18, '18/AD1', 'Mezinesc', 'Adelina-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:55:00', NULL),
(19, '19/AD1', 'Mistau', 'Traian-Razvan', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:57:01', NULL),
(20, '20/AD1', 'Muntean', 'David-Mirel', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:57:01', NULL),
(21, '21/AD1', 'Murgoi', 'Ionela-Bianca', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:58:31', NULL),
(22, '22/AD1', 'Nadasan', 'Victor', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 10:58:31', NULL),
(23, '23/AD!', 'Nedelea', 'Igori', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:00:23', NULL),
(24, '24/AD1', 'Olarescu', 'Sergiu-Robert', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:00:23', NULL),
(25, '25/AD1', 'Onescu', 'Andeea-Ancuta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:02:19', NULL),
(26, '26/AD1', 'Oparlescu', 'Dina-Lidia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:02:19', NULL),
(27, '27/AD1', 'Popescu', 'Sergiu-Catalin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', NULL),
(28, '28/AD!', 'Tirea', 'Delia-Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', NULL),
(29, '29/AD1', 'Ciuca', 'Luis Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '30/AD1', 'Crisan', 'Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '31/AD1', 'Danciu', 'Andrea', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '32/AD1', 'Dragotesc', 'Antonela Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '33/AD3', 'Dumitresc', ' Carina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '34/AD1', 'Filan', 'Ioana-Karina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '35/AD1', 'Ganea', 'Lavinia Denisa', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '36/AD1', 'Gherman', 'Razvan Petru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '37/AD1', 'Goia', 'Robert Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '38/AD1', 'Hoalga', 'Maria-Cristina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '39/AD1', 'Istrate', 'Briana Alesia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '40/AD1', 'Leja', 'Ana Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '41/AD1', 'Mara', ' Andreas Emanuel', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '42/AD1', 'Mituca', 'Bianca Antonia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '43/AD1', 'Niculesc', 'Larissa Madalina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '44/Ad1', 'Pascal', 'Vasile Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '45/AD1', 'Petrescu', ' Ruxandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '46/AD1', 'Popa', ' Marc Antonio', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, '47/AD1', 'Priala', ' Alin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, '48/AD1', 'Priala', ' David', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, '49/AD1', 'Romanesc', ' Denis Raul', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, '50/AD1', 'Scorobete', ' Filip Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, '51/AD1', 'Stefoni', ' Denisa Nicoleta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, '52/AD1', 'Tabirgic', 'Alexandra Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, '53/AD1', 'Ungur', 'Natalia-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, '54/AD1', 'Vitionesc', 'Denisa Naomi', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, '55/AD1', 'Vitionescu', 'Denisa Octavia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, '56/AD1', 'Voda', ' Adela Doinita', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, '57/AD1', 'Zanon', ' Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infodirectori`
--

CREATE TABLE IF NOT EXISTS `infodirectori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_director` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infodirectori`
--

INSERT INTO `infodirectori` (`id`, `fk_director`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infodiriginti`
--

CREATE TABLE IF NOT EXISTS `infodiriginti` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_diriginte` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infodiriginti`
--

INSERT INTO `infodiriginti` (`id`, `fk_diriginte`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', '', '5', 'Hunedoara', 'HD', '688830', 'I', 0, '2017-11-15 13:37:15', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(3, 3, 'M', 'politehnica', '072549396', 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', 0, '2015-08-21 21:24:38', '2015-08-21 19:24:38'),
(4, 8, 'F', 'postliceale', '0745678908', 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', 0, '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 7, 'M', 'Universitare', '', 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', 0, '2015-10-11 21:37:53', '2015-10-11 19:37:53');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infoelevi`
--

CREATE TABLE IF NOT EXISTS `infoelevi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `tata` varchar(20) NOT NULL,
  `mama` varchar(20) NOT NULL,
  `ocupatie_mama` varchar(20) DEFAULT NULL,
  `ocupatie_tata` varchar(20) DEFAULT NULL,
  `religie` varchar(15) NOT NULL,
  `naveta` varchar(2) NOT NULL DEFAULT 'nu',
  `nr_frati` int(2) DEFAULT '0',
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `cod_num` varchar(13) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Salvarea datelor din tabel `infoelevi`
--

INSERT INTO `infoelevi` (`id`, `nr_matricol`, `telefon`, `tata`, `mama`, `ocupatie_mama`, `ocupatie_tata`, `religie`, `naveta`, `nr_frati`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `cod_num`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 'IZ21', '0722549396', '', '', 'profesor', 'profesor', '', '', 0, 'Hateg', 'Sarmizegetusa', '8', '19-B', 'A', '5', 'Hunedoara', 'HD', '688830', '1570521222030', '2017-11-20 17:45:29', '2015-08-10 19:09:45'),
(2, 'IZ22', '07317668877', '', '', 'profesor', 'inginer', '', '', 0, 'Hateg', 'Sarmizegetusa', '8', '19-B', 'A', '5', 'Hunedoara', 'HD', '688831', '', '2017-11-12 14:59:42', '2015-08-10 19:09:45'),
(3, 'IZ27', '072549396', '', '', 'profesor', 'profesor', '', '', 0, 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', '2017-11-12 15:02:28', '2015-08-21 19:24:38'),
(4, 'IZ28', '0745678908', '', '', 'pensionar', 'pensionar', '', '', 0, 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 'IZ29', '0734565678', 'Andrei', 'Ana', 'profesor', 'inginer', '', '', 0, 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', '2015-10-11 21:37:53', '2015-10-11 19:37:53'),
(6, 'IZ30', '0734567893', 'Andru', 'Ana', 'asistent medical', 'muncitor', 'ortodoxa', 'nu', 2, 'Hateg', 'Sarmizegetusa', '5', '17-B', 'A', '3', 'Hunedoara', 'HD', '345678', '1570521222030', '2017-11-12 14:43:31', '2017-11-09 15:03:27'),
(7, 'IZ23', '0734567543', 'Marian', 'Maria', 'vanzator', 'constructor', 'ortodox', 'nu', 1, 'Paclisa', 'principala', '2', NULL, NULL, NULL, 'Hunedoara', 'HD', '235678', '1234565432345', NULL, '2017-11-09 15:03:27'),
(8, 'IZ24', '0732456789', 'Nelu', 'Ana', 'functionar', 'muncitor', 'ortodox', '', 3, '', '', '', '', '', '', 'Hunedoara', 'HD', '234567', '', '2017-11-20 19:34:26', '2017-11-20 17:23:46'),
(9, 'IZ25', '0734565832', 'Matei', 'Laura', '', '', '', '', NULL, '', '', '', '', '', '', '', 'HD', '345678', '1234567654567', '2017-11-20 21:03:43', '2017-11-20 18:40:41');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infoprofesori`
--

CREATE TABLE IF NOT EXISTS `infoprofesori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_profesor` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infoprofesori`
--

INSERT INTO `infoprofesori` (`id`, `fk_profesor`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(3, 3, 'M', 'politehnica', '072549396', 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', 0, '2015-08-21 21:24:38', '2015-08-21 19:24:38'),
(4, 8, 'F', 'postliceale', '0745678908', 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', 0, '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 7, 'M', 'Universitare', '', 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', 0, '2015-10-11 21:37:53', '2015-10-11 19:37:53');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `listaclase`
--

CREATE TABLE IF NOT EXISTS `listaclase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Clasa` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Salvarea datelor din tabel `listaclase`
--

INSERT INTO `listaclase` (`id`, `Clasa`) VALUES
(1, 'IX-A'),
(2, 'IX-B'),
(3, 'IX-C'),
(4, 'IX-D'),
(5, 'IX-E'),
(6, 'IX-F'),
(7, 'IX-G'),
(8, 'X-A'),
(9, 'X-B'),
(10, 'X-C'),
(11, 'X-D'),
(12, 'X-E'),
(13, 'X-F'),
(14, 'X-G'),
(15, 'XI-A'),
(16, 'XI-B'),
(17, 'XI-C'),
(18, 'XI-D'),
(19, 'XI-E'),
(20, 'XI-F'),
(21, 'XI-G'),
(22, 'XII-A'),
(23, 'XII-B'),
(24, 'XII-C'),
(25, 'XII-D'),
(26, 'XII-E'),
(27, 'XII-F'),
(28, 'XII-G'),
(29, 'V'),
(30, 'VI'),
(31, 'VII'),
(32, 'VIII'),
(33, 'IX-H');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `listamaterii`
--

CREATE TABLE IF NOT EXISTS `listamaterii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia` char(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Salvarea datelor din tabel `listamaterii`
--

INSERT INTO `listamaterii` (`id`, `materia`) VALUES
(1, 'Engleza'),
(2, 'Franceza'),
(3, 'Romana'),
(4, 'Matematica'),
(5, 'Fizica'),
(6, 'Chimie'),
(7, 'Biologie'),
(8, 'Istorie'),
(9, 'Geografie'),
(10, 'Informatica'),
(11, 'Economie'),
(12, 'Filosofie'),
(13, 'Religie'),
(14, 'Logica'),
(15, 'Sport'),
(16, 'Desen'),
(17, 'Economie aplicata'),
(18, 'Tic'),
(19, 'Educatie artistica'),
(20, 'Educatie vizuala'),
(21, 'Educatie antreprenoriala'),
(22, 'M2'),
(23, 'Chimie optional'),
(24, 'Purtare');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `materiiclasa`
--

CREATE TABLE IF NOT EXISTS `materiiclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Salvarea datelor din tabel `materiiclasa`
--

INSERT INTO `materiiclasa` (`id`, `id_materie`, `id_clasa`, `an_scolar`) VALUES
(1, 1, 2, '2017-2018'),
(2, 2, 2, '2017-2018'),
(3, 3, 2, '2017-2018'),
(4, 4, 2, '2017-2018'),
(5, 5, 2, '2017-2018'),
(6, 6, 2, '2017-2018'),
(7, 7, 2, '2017-2018'),
(8, 8, 2, '2017-2018'),
(9, 9, 2, '2017-2018'),
(10, 10, 2, '2017-2018'),
(11, 1, 1, '2017-2018'),
(12, 2, 1, '2017-2018'),
(13, 3, 1, '2017-2018'),
(14, 4, 1, '2017-2018'),
(15, 5, 1, '2017-2018'),
(16, 6, 1, '2017-2018'),
(17, 7, 1, '2017-2018'),
(18, 8, 1, '2017-2018'),
(19, 9, 1, '2017-2018'),
(20, 10, 1, '2017-2018'),
(21, 11, 1, '2017-2018'),
(22, 12, 1, '2017-2018'),
(23, 13, 1, '2017-2018'),
(24, 14, 1, '2017-2018'),
(25, 15, 1, '2017-2018'),
(26, 16, 1, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mediigenerale`
--

CREATE TABLE IF NOT EXISTS `mediigenerale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `media` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Salvarea datelor din tabel `mediigenerale`
--

INSERT INTO `mediigenerale` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `media`) VALUES
(6, '32/AD1', 4, 2, '2017-2018', 6),
(5, '31/AD1', 4, 2, '2017-2018', 7.5),
(4, '29/AD1', 4, 2, '2017-2018', 8.5);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mediisemestriale`
--

CREATE TABLE IF NOT EXISTS `mediisemestriale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  `media` float NOT NULL DEFAULT '0',
  `sem` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Salvarea datelor din tabel `mediisemestriale`
--

INSERT INTO `mediisemestriale` (`id`, `nr_matricol`, `id_materie`, `id_clasa`, `an_scolar`, `media`, `sem`) VALUES
(1, '29/AD1', 4, 2, '2017-2018', 8, 'I'),
(2, '31/AD1', 4, 2, '2017-2018', 8, 'I'),
(3, '32/AD1', 4, 2, '2017-2018', 7, 'I'),
(4, '29/AD1', 4, 2, '2017-2018', 9, 'II'),
(5, '31/AD1', 4, 2, '2017-2018', 7, 'II'),
(6, '32/AD1', 4, 2, '2017-2018', 5, 'II'),
(7, '29/AD1', 24, 2, '2017-2018', 9, 'I'),
(8, '30/AD1', 24, 2, '2017-2018', 10, 'I');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `profesorclasa`
--

CREATE TABLE IF NOT EXISTS `profesorclasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profesor` int(2) NOT NULL,
  `id_clasa` int(2) NOT NULL,
  `id_materie` int(2) NOT NULL,
  `an_scolar` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Salvarea datelor din tabel `profesorclasa`
--

INSERT INTO `profesorclasa` (`id`, `id_profesor`, `id_clasa`, `id_materie`, `an_scolar`) VALUES
(1, 1, 8, 21, '2017-2018'),
(2, 1, 9, 21, '2017-2018'),
(3, 1, 10, 21, '2017-2018'),
(4, 1, 11, 21, '2017-2018'),
(5, 1, 12, 21, '2017-2018'),
(6, 1, 13, 21, '2017-2018'),
(7, 1, 14, 21, '2017-2018'),
(8, 1, 15, 11, '2017-2018'),
(9, 1, 16, 11, '2017-2018'),
(10, 1, 17, 11, '2017-2018'),
(11, 1, 18, 11, '2017-2018'),
(12, 1, 20, 11, '2017-2018'),
(13, 1, 21, 11, '2017-2018'),
(14, 1, 27, 11, '2017-2018'),
(15, 1, 28, 11, '2017-2018'),
(16, 1, 27, 22, '2017-2018'),
(17, 1, 28, 22, '2017-2018'),
(18, 1, 27, 17, '2017-2018'),
(19, 4, 2, 7, '2017-2018'),
(20, 4, 3, 7, '2017-2018'),
(21, 4, 5, 7, '2017-2018'),
(22, 4, 6, 7, '2017-2018'),
(23, 4, 7, 7, '2017-2018'),
(24, 4, 6, 6, '2017-2018'),
(25, 4, 33, 6, '2017-2018'),
(26, 4, 12, 7, '2017-2018'),
(27, 4, 13, 7, '2017-2018'),
(28, 4, 14, 7, '2017-2018'),
(29, 4, 31, 7, '2017-2018'),
(30, 4, 14, 6, '2017-2018'),
(31, 4, 25, 6, '2017-2018'),
(32, 4, 26, 6, '2017-2018'),
(33, 4, 26, 23, '2017-2018'),
(34, 5, 29, 20, '2017-2018'),
(35, 5, 32, 3, '2017-2018'),
(36, 5, 10, 3, '2017-2018'),
(37, 5, 13, 3, '2017-2018'),
(38, 5, 19, 3, '2017-2018'),
(39, 5, 21, 3, '2017-2018'),
(40, 5, 18, 19, '2017-2018'),
(41, 5, 19, 19, '2017-2018'),
(42, 5, 25, 19, '2017-2018'),
(43, 5, 26, 19, '2017-2018'),
(44, 41, 2, 4, '2017-2018'),
(45, 41, 3, 4, '2017-2018'),
(46, 41, 16, 4, '2017-2018'),
(47, 41, 22, 4, '2017-2018');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `profesori`
--

CREATE TABLE IF NOT EXISTS `profesori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Salvarea datelor din tabel `profesori`
--

INSERT INTO `profesori` (`id`, `nume`, `prenume`, `email`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(1, 'Adam', 'Costel', 'adam@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2015-07-14 22:00:00', '2015-07-15 00:00:00'),
(2, 'Alimpesc', 'Mihaela', 'mihaela@hotmail.com', '0d801be376ab1ee7515702986ed03710', '2015-07-19 22:00:00', '2015-12-02 22:08:29'),
(3, 'Ardelean', 'Marcel', 'marcel@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-10-20 20:35:44', '2017-10-20 23:35:44'),
(4, 'Avramoni', 'Adina', 'adina@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:48:39', NULL),
(5, 'Bacter', 'Gabriela', 'gabi@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:48:39', NULL),
(6, 'Badea', 'Rodica', 'badea@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:49:56', NULL),
(7, 'Badiu', 'Ioan', 'badiu@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:49:56', NULL),
(8, 'Basaraba', 'Gabriela', 'gabriela@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:51:14', NULL),
(9, 'Bolosin', 'Maria', 'maria@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:51:14', NULL),
(10, 'Bota', 'Cristian', 'bota@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:52:00', NULL),
(11, 'Bratu', 'Victor', 'victor@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:53:24', NULL),
(12, 'Bratu', 'Alina', 'alina@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:53:24', NULL),
(13, 'Bunea', 'Teodora', 'bunea@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:54:39', NULL),
(14, 'Cioara', 'Dana', 'cioara@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:54:39', NULL),
(15, 'Cosa', 'Rodica', 'cosa@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:56:00', NULL),
(16, 'Craciun', 'Raul', 'raul@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:56:00', NULL),
(17, 'Damian', 'Adrian', 'adi@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:57:36', NULL),
(18, 'DeValerio', 'Marilena', 'marilena@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:57:36', NULL),
(19, 'Filimon', 'Costel', 'filimon@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:58:41', NULL),
(20, 'Florea', 'Neluta', 'neluta@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:58:41', NULL),
(21, 'Florea', 'Anisoara', 'ani@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 11:59:07', NULL),
(22, 'Florescu', 'Ioana', 'ioana@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:02:46', NULL),
(23, 'Floroni', 'Loredana', 'floronim@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:02:46', NULL),
(24, 'Ghida', 'Romeo', 'ghida@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:03:53', NULL),
(25, 'Ghiura', 'Anca', 'ghiura@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:03:53', NULL),
(26, 'Ilioni', 'Manuela', 'ilioni@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:05:07', NULL),
(27, 'Indrecan', 'Mihaela', 'indrecan@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:05:07', NULL),
(28, 'Itul', 'Flaviu', 'itul@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:06:09', NULL),
(29, 'Iuga', 'Gabriel', 'iugagabi@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:06:09', NULL),
(30, 'Iuga', 'Aniela', 'iugaaniela@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:07:16', NULL),
(31, 'Lupulescu', 'Carmen', 'lupulescu@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:07:16', NULL),
(32, 'Marian', 'Marius', 'marian@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:08:09', NULL),
(33, 'Moga', 'Mirela', 'moga@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:08:09', NULL),
(34, 'Muntean', 'Cristina', 'cristimunt@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:09:23', NULL),
(35, 'Muntean', 'Marius', 'mar@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:09:23', NULL),
(36, 'Muntean', 'Diana', 'diamunt@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:10:18', NULL),
(37, 'Muntean', 'Iuliana', 'iuliana@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:10:18', NULL),
(38, 'Neiconi', 'Liliana', 'lilineiconi@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:11:24', NULL),
(39, 'Neiconi', 'Cornelia', 'cornelia@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:11:24', NULL),
(40, 'Nicoara', 'Adriana', 'adriana@hotmail.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:12:28', NULL),
(41, 'Nicoara', 'Daniel', 'nicadialex@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:12:28', NULL),
(42, 'Paraian', 'Ana-Maria', 'paraian@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:13:26', NULL),
(43, 'Pascotescu', 'Camelia', 'cami@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:13:26', NULL),
(44, 'Popa', 'Alina', 'popaalina@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:14:46', NULL),
(45, 'Rosian', 'Lucian', 'rosian@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:14:46', NULL),
(46, 'Sandor', 'Iozefina', 'sandor@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:15:51', NULL),
(47, 'Sbuchea', 'Liliana', 'sbuchea@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:15:51', NULL),
(48, 'Scorobete', 'Raluca', 'scorobete@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:16:58', NULL),
(49, 'Sintimbrean', 'Simona', 'simona@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:16:58', NULL),
(50, 'Socaci', 'Mihaela', 'socaci@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:17:56', NULL),
(51, 'Stanculesc', 'Diana', 'stadiana@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:17:56', NULL),
(52, 'Stanculesc', 'Paul', 'paul_st@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:18:56', NULL),
(53, 'Stoica', 'Alina', 'stoica@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:18:56', NULL),
(54, 'Susan', 'Daniela', 'susan@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:19:42', NULL),
(55, 'Tamas', 'Bianca', 'tamas@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:19:42', NULL),
(56, 'Terlea', 'Violeta', 'terlea@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:20:46', NULL),
(57, 'Tigaranu', 'Nicoleta', 'tigaranu@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:20:46', NULL),
(58, 'Varhedi', 'Ionel', 'varhedi@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:21:43', NULL),
(59, 'Voda', 'Maria', 'voda@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:21:43', NULL),
(60, 'Zaharie', 'Iosif-Liziana', 'zaharie@yahoo.com', '0d801be376ab1ee7515702986ed03710', '2017-11-23 15:22:17', NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_matricol` varchar(10) NOT NULL,
  `nume` varchar(15) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `sex` char(1) NOT NULL,
  `parola` varchar(50) NOT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_actualizarii` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Salvarea datelor din tabel `test`
--

INSERT INTO `test` (`id`, `nr_matricol`, `nume`, `prenume`, `email`, `sex`, `parola`, `data_inregistrarii`, `data_actualizarii`) VALUES
(29, '29/AD1', 'Ciuca', 'Luis Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(30, '30/AD1', 'Crisan', 'Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(31, '31/AD1', 'Danciu', 'Andrea', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(32, '32/AD1', 'Dragotesc', 'Antonela Mihaela', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(33, '33/AD3', 'Dumitresc', ' Carina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(34, '34/AD1', 'Filan', 'Ioana-Karina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(35, '35/AD1', 'Ganea', 'Lavinia Denisa', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(36, '36/AD1', 'Gherman', 'Razvan Petru', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(37, '37/AD1', 'Goia', 'Robert Cristian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(38, '38/AD1', 'Hoalga', 'Maria-Cristina', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(39, '39/AD1', 'Istrate', 'Briana Alesia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(40, '40/AD1', 'Leja', 'Ana Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(41, '41/AD1', 'Mara', ' Andreas Emanuel', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(42, '42/AD1', 'Mituca', 'Bianca Antonia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(43, '43/AD1', 'Niculesc', 'Larissa Madalina ', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(44, '44/Ad1', 'Pascal', 'Vasile Marian', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(45, '45/AD1', 'Petrescu', ' Ruxandra', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(46, '46/AD1', 'Popa', ' Marc Antonio', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(47, '47/AD1', 'Priala', ' Alin', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(48, '48/AD1', 'Priala', ' David', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(49, '49/AD1', 'Romanesc', ' Denis Raul', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(50, '50/AD1', 'Scorobete', ' Filip Ionut', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(51, '51/AD1', 'Stefoni', ' Denisa Nicoleta', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(52, '52/AD1', 'Tabirgic', 'Alexandra Maria', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(53, '53/AD1', 'Ungur', 'Natalia-Roxana', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(54, '54/AD1', 'Vitionesc', 'Denisa Naomi', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(55, '55/AD1', 'Vitionescu', 'Denisa Octavia', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(56, '56/AD1', 'Voda', ' Adela Doinita', 'elev@yahoo.com', 'F', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10'),
(57, '57/AD1', 'Zanon', ' Andrei', 'elev@yahoo.com', 'M', '41087aa1be106903ecee5af3892f7e88', '2017-11-22 11:04:10', '2017-11-22 13:04:10');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
