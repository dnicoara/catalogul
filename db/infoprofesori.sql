-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 31 Oct 2017 la 22:42
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `catalogul`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infoprofesori`
--

CREATE TABLE IF NOT EXISTS `infoprofesori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_profesor` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infoprofesori`
--

INSERT INTO `infoprofesori` (`id`, `fk_profesor`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(3, 3, 'M', 'politehnica', '072549396', 'Munhen', 'Strasse', '99', 'A-2', 'C', '10', 'land', 'AB', '233445', '', 0, '2015-08-21 21:24:38', '2015-08-21 19:24:38'),
(4, 8, 'F', 'postliceale', '0745678908', 'Cisnadie', 'Teilor', '13', '', '', '', 'Sibiu', 'SB', '233445', '', 0, '2015-12-02 21:58:46', '2015-09-18 11:21:42'),
(5, 7, 'M', 'Universitare', '', 'Craiova', 'Chimiei', '4', '', '', '', 'Dolj', 'DJ', '234678', '', 0, '2015-10-11 21:37:53', '2015-10-11 19:37:53');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
