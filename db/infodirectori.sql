-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Noi 2017 la 20:24
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `catalogul`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `infodirectori`
--

CREATE TABLE IF NOT EXISTS `infodirectori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_director` int(10) NOT NULL,
  `sex` char(1) NOT NULL,
  `studii` varchar(30) NOT NULL,
  `telefon` varchar(16) DEFAULT NULL,
  `localitate` varchar(25) NOT NULL,
  `strada` varchar(30) DEFAULT NULL,
  `numar` varchar(10) NOT NULL,
  `bloc` varchar(10) DEFAULT NULL,
  `scara` varchar(10) DEFAULT NULL,
  `apartament` varchar(10) DEFAULT NULL,
  `judet` varchar(15) NOT NULL,
  `buletin_seria` varchar(3) DEFAULT NULL,
  `buletin_nr` varchar(6) DEFAULT NULL,
  `grad_didactic` varchar(30) NOT NULL,
  `vechime` int(2) NOT NULL,
  `data_actualizarii` datetime DEFAULT NULL,
  `data_inregistrarii` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `infodirectori`
--

INSERT INTO `infodirectori` (`id`, `fk_director`, `sex`, `studii`, `telefon`, `localitate`, `strada`, `numar`, `bloc`, `scara`, `apartament`, `judet`, `buletin_seria`, `buletin_nr`, `grad_didactic`, `vechime`, `data_actualizarii`, `data_inregistrarii`) VALUES
(1, 1, 'F', 'liceu', '0722549396', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688830', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45'),
(2, 2, 'F', 'universitare', '07317668877', 'Hateg', 'Sarmizegetusa', '8', '19-B', NULL, '5', 'Hunedoara', 'HD', '688831', '', 0, '0000-00-00 00:00:00', '2015-08-10 19:09:45');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
