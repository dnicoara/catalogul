/**
 * Created by Daniel on 8/3/2018.
 */

if (document.cookie == "") {
    expireDate = new Date()
    expireDate.setSeconds(expireDate.getSeconds() + 24 * 60 * 60);
    document.cookie = "modalWindow=" + true + " ;expires=" + expireDate.toGMTString()

    $(window).on('load', function () {
        $('#myModal').modal('show');
    });
}