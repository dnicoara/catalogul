<?php
namespace app\controllers;

use app\controllers\autoGenerated\NotificaridiriginteControllerGenerated;
use app\models\Diriginti;
use app\models\Elevi;
use app\models\Listaclase;
use app\models\Notificaridiriginte;
use app\models\NotificaridiriginteSearch;
use Yii;

/**
 *
 * @author dan.nicoara
 *
 */
class NotificaridiriginteController extends NotificaridiriginteControllerGenerated
{

    /**
     * Lists all Notificaridiriginte models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (isset(Yii::$app->session['id_clasa']))
            $id_clasa = Yii::$app->session['id_clasa'];
        else
            return $this->redirect([
                'site/index'
            ]);

        $searchModel = new NotificaridiriginteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andWhere([
            'id_clasa' => $id_clasa,
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }
    /**
     * Updates an existing Notificaridiriginte model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $este_elevul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'elev' :
                                    false;
        if (!$este_elevul_conectat)
            return $this->redirect([
                'site/index'
            ]);
        
        $model = $this->findModel($id);

        $nr_matricol = Yii::$app->session['user']->nr_matricol;
        $nume_elev = Yii::$app->session['user']->nume;
        $prenume_elev = Yii::$app->session['user']->prenume;
        $an_scolar = \Yii::$app->params['anul_scolar'];
        $id_clasa = Elevi::getIdClasa($nr_matricol, $an_scolar);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            {
                Yii::$app->session->setFlash('success', "Notificarea a fost editata si trimisa dirigintelui !");
                return $this->redirect([
                    'site/index'
                ]);
            }

        } else {
            return $this->render('update', [
                'model' => $model,
                'nr_matricol' => $nr_matricol,
                'nume_elev' => $nume_elev,
                'prenume_elev' => $prenume_elev,
                'id_clasa' => $id_clasa,
                'an_scolar' => $an_scolar
            ]);
        }
    }

    /**
     * Creates a new Notificaridiriginte model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $este_elevul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'elev' :
                                    false;

        if (!$este_elevul_conectat || !Elevi::emailCorect()) {
            Yii::$app->session->setFlash('error', 'Pentru a continua , trebuie sa va conectati cu o adresa de e-mail valida !!!');
            return $this->redirect([
                'site/index'
            ]);
        }

        $model = new Notificaridiriginte();

        $nr_matricol = Yii::$app->session['user']->nr_matricol;
        $nume_elev = Yii::$app->session['user']->id;
        $prenume_elev = Yii::$app->session['user']->prenume;
        $an_scolar = \Yii::$app->params['anul_scolar'];
        $id_clasa = Elevi::getIdClasa($nr_matricol, $an_scolar);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'nr_matricol' => $nr_matricol,
                'nume_elev' => $nume_elev,
                'prenume_elev' => $prenume_elev,
                'id_clasa' => $id_clasa,
                'an_scolar' => $an_scolar
            ]);
        }
    }

    /**
     * Deletes an existing Notificaridiriginte model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash('success', "Notificarea  a fost stearsa din lista !");
        return $this->redirect([
            'index'
        ]);
    }

    /**
     *
     * @param
     *            $id_notificare
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException Dirigintele actualizeaza statutul unei notificari din tabla notificaridiriginte
     */
    public function actionActualizeaza($id_notificare)
    {
        $notificare = Notificaridiriginte::getNotificare($id_notificare);
        
        $model = $this->findModel($id_notificare);
        
        $nr_matricol = $notificare['nr_matricol'];
        $id_clasa = $notificare['id_clasa'];
        $nume_elev = $notificare['nume'];
        $prenume_elev = $notificare['prenume'];
        $an_scolar = \Yii::$app->params['anul_scolar'];
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            {
                Yii::$app->session['numar_notificari_nerezolvate'] = Notificaridiriginte::getNrNotificariNerezolvate($id_clasa);
                return $this->redirect([
                    'notificaridiriginte/index'
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'nr_matricol' => $nr_matricol,
                'nume_elev' => $nume_elev,
                'prenume_elev' => $prenume_elev,
                'id_clasa' => $id_clasa,
                'an_scolar' => $an_scolar
            ]);
        }
    }

    /**
     *
     * @param
     *            $id_notificare
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable Dirigintele sterge o notificare din tabla notificaridiriginte
     */
    public function actionSterge($id_notificare)
    {
        $this->findModel($id_notificare)->delete();
        return $this->redirect([
            'notificaridiriginte/index'
        ]);
    }

    /**
     *
     * @return string|\yii\web\Response dirigintele selecteaza elevii din clasa, carora doreste sa le trimita un e-mail si trimite e-mailul
     */
    public function actionTrimitEmailElevi()
    {
        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'diriginte' :
                                        false;

        if ($este_dirigintele_conectat) {
            $model = new Notificaridiriginte();
            
            // obtin id-ul dirigintelui pentru a obtine id-ul clasei dirigintelui
            $id_diriginte = Yii::$app->session['user']->id;
            $id_clasa = Diriginti::getIdClasa($id_diriginte);
            $nume_clasa = Listaclase::getNumeClasa($id_clasa);
            $an_scolar = \Yii::$app->params['anul_scolar'];
            $dataProvider = Diriginti::selectezEleviiClasei($id_clasa, $an_scolar);
            
            if (isset($_GET['Notificaridiriginte'])) {
                // obtin id-urile elevilor selectati si apoi e-mailurile acestore
                $lista_iduri = $_GET['Notificaridiriginte']['id'];
                $lista_emailuri = Notificaridiriginte::getEmailEleviSelectati($lista_iduri, $dataProvider->getModels());
                $mailcontent = $_GET['Notificaridiriginte']['notificarea'];
                
                $model->sendMailEleviSelectati($lista_emailuri, $mailcontent);
                Yii::$app->session->setFlash('success', 'E-mailul a fost expediat !!!');

                return $this->redirect([
                    'site/index',
                ]);
            }
            
            $lista_elevilor_clasei = Notificaridiriginte::getEleviSelectati($dataProvider);
            
            return $this->render('setemailmesaj', [
                'model' => $model,
                'id_clasa' => $id_clasa,
                'nume_clasa' => $nume_clasa,
                'lista_elevi' => $lista_elevilor_clasei
            ]);
        } else
            return $this->redirect([
                'site/index'
            
            ]);
    }

    /**
     * Displays a single Notificaridiriginte model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $este_elevul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'elev' :
                                    false;
        if (!$este_elevul_conectat)
            return $this->redirect([
                'site/index'
            ]);
        $nr_matricol = Yii::$app->session['user']->nr_matricol;
        $an_scolar = $anul_scolar = \Yii::$app->params['anul_scolar'];
        $id_clasa = Elevi::getIdClasa($nr_matricol, $an_scolar);
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'id_clasa' => $id_clasa
        ]);
    }

    /**
     * @return \yii\web\Response
     * se afiseaza notificarile facute de elev pentru diriginte
     */
    public function actionAfisezNotificarileElevului()
    {
        $este_elevul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'elev' :
                                    false;

        if (!$este_elevul_conectat || !Elevi::emailCorect()) {
            Yii::$app->session->setFlash('error', 'Pentru a continua , trebuie sa va conectati cu o adresa de e-mail valida !!!');
            return $this->redirect([
                'site/index'
            ]);
        }

        $nr_matricol = Yii::$app->session['user']->nr_matricol;
        $an_scolar = $anul_scolar = \Yii::$app->params['anul_scolar'];

        $dataProvider = Notificaridiriginte::getNotificariElev($nr_matricol, $an_scolar);

        return $this->render('afisarenotificarielev', [
            'dataProvider' => $dataProvider,
            'nr_matricol' => $nr_matricol

        ]);

    }
}

