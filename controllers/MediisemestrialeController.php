<?php
namespace app\controllers;

use app\controllers\autoGenerated\MediisemestrialeControllerGenerated;
use app\models\Diriginti;
use app\models\Elevclasa;
use app\models\ElevclasaSearch;
use app\models\Elevi;
use app\models\Listaclase;
use app\models\Listamaterii;
use app\models\Mediisemestriale;
use Yii;
use yii\filters\VerbFilter;

/**
 *
 * @author dan.nicoara
 *
 */
class MediisemestrialeController extends MediisemestrialeControllerGenerated
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'POST'
                    ]
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete', 'adaug-medii-sem-elevi-transferati', 'set-nota-purtare'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if (!isset(Yii::$app->session['user'])) {
                                Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a face aceasta operatie !!!');
                                return false;
                            }
                            if (Yii::$app->session['user']->role == 'admin' || Yii::$app->session['user']->role == 'profesor' || Yii::$app->session['user']->role == 'director' || Yii::$app->session['user']->role == 'diriginte') {
                                return true;
                            } else {
                                Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a face aceasta operatie !!!');
                                return false;
                            }

                        }
                    ],
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if (!isset(Yii::$app->session['user'])) {
                                Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a face aceasta operatie !!!');
                                return false;
                            }
                            if (Yii::$app->session['user']->role == 'profesor' || Yii::$app->session['user']->role == 'diriginte' || Yii::$app->session['user']->role == 'admin') {
                                return true;
                            } else {
                                Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a face aceasta operatie !!!');
                                return false;
                            }

                        }
                    ],
                    [
                        'actions' => ['set-nota-purtare'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if (!isset(Yii::$app->session['user'])) {
                                Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a face aceasta operatie !!!');
                                return false;
                            }
                            if (Yii::$app->session['user']->role == 'diriginte') {
                                return true;
                            } else {
                                Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a face aceasta operatie !!!');
                                return false;
                            }

                        }
                    ],
                    [
                        'actions' => ['adaug-medii-sem-elevi-transferati'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if (!isset(Yii::$app->session['user'])) {
                                Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a face aceasta operatie !!!');
                                return false;
                            }
                            if (Yii::$app->session['user']->role == 'diriginte' || Yii::$app->session['user']->role == 'admin') {
                                return true;
                            } else {
                                Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a face aceasta operatie !!!');
                                return false;
                            }

                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Mediisemestriale models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;

        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;
        $este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'profesor' :
            false;

        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'diriginte' :
            false;


        if (!$este_administratorul_conectat &&
            !$este_directorul_conectat &&
            !$este_profesorul_conectat &&
            !$este_dirigintele_conectat
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $searchModel = new MediisemestrialeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new Mediisemestriale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'profesor' :
            false;

        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'diriginte' :
            false;

        if (!$este_dirigintele_conectat && !$este_profesorul_conectat) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $model = new Mediisemestriale();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Mediisemestriale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'profesor' :
                                        false;

        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'diriginte' :
                                        false;

        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'admin' :
                                    false;

        if ((!$este_dirigintele_conectat) &&
            (!$este_profesorul_conectat) &&
            (!$este_administratorul_conectat)

        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $this->findModel($id)->delete();

        if ($este_dirigintele_conectat) {
            //dirigintele poate sterge doar nota la purtare
            return $this->redirect([
                'elevi/afisare-clasa-diriginte',

            ]);
        }
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Updates an existing Mediisemestriale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $este_profesorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'profesor' :
            false;

        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'diriginte' :
            false;

        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;

        if ((!$este_dirigintele_conectat) &&
            (!$este_profesorul_conectat) &&
            (!$este_administratorul_conectat)
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $model = $this->findModel($id);

        $nr_matricol = Mediisemestriale::getNumarMatricolElev($id);
        $id_materie = Mediisemestriale::getIdMaterie($id);
        $id_clasa = Mediisemestriale::getIdClasa($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'nr_matricol' => $nr_matricol,
            'id_materie' => $id_materie,
            'id_clasa' => $id_clasa

        ]);
    }


    /**
     *
     * @return string afisaza mediile semestriale ale elevului selectat
     */
    public function actionAfisareMediisem()
    {
        $anul_scolar = \Yii::$app->params['anul_scolar'];
        
        $nr_matricol = $_GET['nr_matricol'];
        $id_clasa = $_GET['id_clasa'];
        $semestrul = $_GET['semestrul'];
        $nume_elev = $_GET['nume_elev'];
        $prenume_elev = $_GET['prenume_elev'];
        
        $dataProvider = Mediisemestriale::getMediiSemElev($nr_matricol, $id_clasa, $semestrul, $anul_scolar);
        
        return $this->render('afisaremediisemelev', [
            'dataProvider' => $dataProvider,
            'nume_elev' => $nume_elev,
            'prenume_elev' => $prenume_elev,
            'id_clasa' => $id_clasa
        ]);
    }

    /**
     *
     * @return string|\yii\web\Response @Dirigintele stabileste nota la purtare a elevului
     */
    public function actionSetNotaPurtare()
    {
        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
                                        Yii::$app->session['user']->role == 'diriginte' :
                                        false;

        if ((!$este_dirigintele_conectat)

        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $model = new Mediisemestriale();
        $anul_scolar = \Yii::$app->params['anul_scolar'];
        
        // obtin id-ul materiei Purtare
        $id_materie = Listamaterii::getIdMaterie('Purtare');
        $materii_profesor = array(
            $id_materie => 'Purtare'
        );
        
        // obtin id-ul dirigintelui pentru a obtine id-ul clasei dirigintelui
        $id_diriginte = Yii::$app->session['user']->id;
        $id_clasa = Diriginti::getIdClasa($id_diriginte);
        
        if (isset($_GET['id_elev'])) {
            $id_elev = $_GET['id_elev'];
        } else {
            return $this->redirect([
                'elevi/afisare-clasa-diriginte'
            ]);
        }
        $nr_matricol = Elevi::getNumarMatricolElev($id_elev); // obtin numarul matricol al elevului din tabla elevi
        $nume_elev = Elevi::getNumeElev($nr_matricol); // obtin numele elevului
        $prenume_elev = Elevi::getPrenumeElev($nr_matricol); // obtin prenumele elevului

        if ($model->load(Yii::$app->request->post()) && $model->media != Null && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'id_materie' => $id_materie,
                'id_clasa' => $id_clasa,
                'nr_matricol' => $nr_matricol,
                'anul_scolar' => $anul_scolar,
                'materii_profesor' => $materii_profesor
            ]);
        }
        
        return $this->render('create', [
            'model' => $model,
            'nr_matricol' => $nr_matricol,
            'id_materie' => $id_materie,
            'id_clasa' => $id_clasa,
            'anul_scolar' => $anul_scolar,
            'nume_elev' => $nume_elev,
            'prenume_elev' => $prenume_elev,
            'materii_profesor' => $materii_profesor
        
        ]);
    }

    public function actionSelectareClasaElevi()
    {
        //$lista_elevi = Elevclasa::getListaElevilor();
        //$id_clasa = Elevclasa::getListaClaselor();
        $anul_scolar = \Yii::$app->params['anul_scolar'];
        $lista_clase = Listaclase::getListaClaselor();
        $clasa = new Listaclase();

        if (isset($_GET['Listaclase']['id'])) {
            $id_clasa = $_GET['Listaclase']['id'];

            $searchModel = new ElevclasaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere([
                'elevclasa.id_clasa' => $id_clasa,
                'elevclasa.an_scolar' => $anul_scolar
            ]);

            return $this->render('afisareclasaselectata', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'clasa_selectata' => $id_clasa,
            ]);

        } else {
            return $this->render('selectareclasa', [
                'model' => $clasa,
                'lista_claselor' => $lista_clase,
            ]);
        }
    }

    /**
     * @return string
     * adaug mediile semestriale (examene de diferenta) pentru elevii transferati
     */
    public function actionAdaugMediiSemEleviTransferati()
    {
        $este_dirigintele_conectat = isset(Yii::$app->session['user']->role) ?
        Yii::$app->session['user']->role == 'diriginte' :
        false;

        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'admin' :
                                    false;

        if ((!$este_dirigintele_conectat) &&
            (!$este_administratorul_conectat)

        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        if (isset($_GET['Listamaterii']['id']) && isset($_GET['Mediisemestriale']['media'])) {
            $nr_matricol = $_GET['Mediisemestriale']['nr_matricol'];
            $id_materie = $_GET['Listamaterii']['id'];
            $id_clasa = $_GET['Mediisemestriale']['id_clasa'];
            $an_scolar = \Yii::$app->params['anul_scolar'];
            $media = $_GET['Mediisemestriale']['media'];
            $sem = $_GET['Mediisemestriale']['sem'];

            //introducem media elevului in tabla mediisemestriale
            Mediisemestriale::insertManualMediaSemestriala($nr_matricol, $id_materie, $id_clasa, $an_scolar, $media, $sem);

            $searchModel = new ElevclasaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere([
                'elevclasa.id_clasa' => $id_clasa,
                'elevclasa.an_scolar' => $an_scolar
            ]);

            return $this->render('afisareclasaselectata', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'clasa_selectata' => $id_clasa,
            ]);

        }

        if (isset($_GET['id_elev']) && isset($_GET['id_clasa'])) {
            $anul_scolar = \Yii::$app->params['anul_scolar'];
            $id_elev = $_GET['id_elev'];
            $id_clasa = $_GET['id_clasa'];
            $nr_matricol = Elevclasa::getNumarMatricolElev($id_elev);

            $model1 = new Listamaterii();
            $model2 = new Mediisemestriale();

            //determin lista materiilor clasei
            $materiile_clasei = Listamaterii::getListaMateriilorClasei($id_clasa, $anul_scolar);

            return $this->render('insertmediisemform', [
                'model1' => $model1,
                'model2' => $model2,
                'materiile_clasei' => $materiile_clasei,
                'nr_matricol' => $nr_matricol,
                'id_clasa' => $id_clasa
            ]);

        } else {
            $model = new Listaclase();
            $lista_clase = Listaclase::getListaClaselor();

            return $this->render('selectareclasa', [
                'model' => $model,
                'lista_claselor' => $lista_clase,
            ]);
        }
    }

}

