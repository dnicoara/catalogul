<?php
namespace app\controllers;

use app\controllers\autoGenerated\InfodirectoriControllerGenerated;
use app\models\Directori;
use app\models\Infodirectori;
use Yii;

/**
 * InfodirectoriController implements the CRUD actions for Infodirectori model.
 */
class InfodirectoriController extends InfodirectoriControllerGenerated
{

    /**
     * Lists all Infodirectori models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;

        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;

        $este_secretarul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'secretar' :
            false;

        if (!$este_administratorul_conectat &&
            !$este_directorul_conectat &&
            !$este_secretarul_conectat
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $searchModel = new InfodirectoriSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Infodirectori model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;

        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;

        $este_secretarul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'secretar' :
            false;

        if (!$este_administratorul_conectat &&
            !$este_directorul_conectat &&
            !$este_secretarul_conectat
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Creates a new Infodirectori model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'admin' :
                                    false;

        if ((!$este_administratorul_conectat)
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $model = new Infodirectori();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing Infodirectori model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;

        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;

        if (!$este_administratorul_conectat &&
            !$este_directorul_conectat
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Infodirectori model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;

        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;

        if (!$este_administratorul_conectat &&
            !$este_directorul_conectat
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $this->findModel($id)->delete();

        return $this->redirect([
            'index'
        ]);
    }
    /**
     * Afiseaza contul directorului.
     */
    public function actionContDirector()
    {
        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;
        if ($este_directorul_conectat) {
            // cititorul se va identifica prin mail
            $mail_director = Yii::$app->session['user']->username;

            $sql = 'SELECT * FROM directori WHERE email="' . $mail_director . '"';
            $record = Directori::findBySql($sql, [])->all();

            $dataProvider = Infodirectori::selectInfodirector($mail_director);

            return $this->render('contdirector', [
                // 'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'email_director' => $mail_director,
                'id_director' => $record[0]['id'],
                'nume_director' => $record[0]['nume'],
                'prenume_director' => $record[0]['prenume']
            ]);
        } else {
            Yii::$app->session->setFlash('error', 'Pentru a continua trebuie sa va logati !!!');
            return $this->redirect([
                'site/index'
            ]);
        }

    }
}
