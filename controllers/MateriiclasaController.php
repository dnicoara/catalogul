<?php
namespace app\controllers;

use app\controllers\autoGenerated\MateriiclasaControllerGenerated;
use app\models\Listaclase;
use app\models\Listamaterii;
use app\models\Materiiclasa;
use Yii;

/**
 *
 * @author dan.nicoara
 *
 */
class MateriiclasaController extends MateriiclasaControllerGenerated
{

    /**
     * Seteaza materiile unei clase.
     */
    public function actionEditMateriiclase()
    {
        $lista_materii = Materiiclasa::getListaMateriilor();
        $lista_claselor = Materiiclasa::getListaClaselor();
        
        $materia = new Materiiclasa();
        
        if (isset($_POST['Materiiclasa']['id']) && isset($_POST['Materiiclasa']['an_scolar']) && isset($_POST['Materiiclasa']['id_clasa'])) {
            $nr_materii = count($_POST['Materiiclasa']['id']);
            
            $an_scolar = $_POST['Materiiclasa']['an_scolar'];
            $id_clasa_selectata = $_POST['Materiiclasa']['id_clasa'];
            $materii_clasa = $_POST['Materiiclasa']['id'];

            $rez = Materiiclasa::setMateriiClasa($nr_materii, $an_scolar, $id_clasa_selectata, $materii_clasa);
            if ($rez)
                $mesaj = $mesaj = 'Materiile clasei selectate au fost salvate in baza de date';
            else
                $mesaj = 'S-a produs o eroare .Materiile NU au fost salvate in baza de date';
        } else
            $mesaj = '';
        
        return $this->render('materiileclasei', [
            'model' => $materia,
            'id_clasa' => $lista_claselor,
            'lista_materii' => $lista_materii,
            'mesaj' => $mesaj
        ]);
    }

    /**
     * selectez clasa si materiile clasei selectate
     */
    public function actionSelectare()
    {
        $lista_materii = Materiiclasa::getListaMateriilor();
        $lista_claselor = Materiiclasa::getListaClaselor();
        
        $materia = new Materiiclasa();
        $mesaj = '';
        return $this->render('materiileclasei', [
            'model' => $materia,
            'lista_materii' => $lista_materii,
            'id_clasa' => $lista_claselor,
            'mesaj' => $mesaj
        ]);
    }

    /**
     * Creates a new Materiiclasa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Materiiclasa();
        $mesaj = '';
        
        $lista_materiilor = Listamaterii::getListaMateriilor();
        $lista_claselor = Listaclase::getListaClaselor();
        $an_scolar = \Yii::$app->params['anul_scolar'];
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        } else
            if ($model->load(Yii::$app->request->post()))
            $mesaj = 'Materia este deja introdusa la clasa selectata !!!';
        
        return $this->render('create', [
            'model' => $model,
            'lista_materiilor' => $lista_materiilor,
            'lista_claselor' => $lista_claselor,
            'an_scolar' => $an_scolar,
            'mesaj' => $mesaj
        ]);
    }

    /**
     * Updates an existing Materiiclasa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $lista_materiilor = Listamaterii::getListaMateriilor();
        $lista_claselor = Listaclase::getListaClaselor();
        
        $model = $this->findModel($id);
        $clasa = Listaclase::getNumeClasa($model->id_clasa);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'lista_materiilor' => $lista_materiilor,
                'lista_claselor' => $lista_claselor,
                'clasa' => $clasa
            ]);
        }
        
        return $this->render('update', [
            'model' => $model,
            'lista_materiilor' => $lista_materiilor,
            'lista_claselor' => $lista_claselor,
            'clasa' => $clasa
        ]);
    }
}

