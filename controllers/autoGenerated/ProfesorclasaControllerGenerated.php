<?php
namespace app\controllers\autoGenerated;

use app\models\Profesorclasa;
use app\models\ProfesorclasaSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ProfesorclasaControllerGenerated implements the CRUD actions for Profesorclasa model.
 */
class ProfesorclasaControllerGenerated extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'POST'
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Profesorclasa models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
                                    Yii::$app->session['user']->role == 'admin' :
                                    false;
        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;
        $este_secretarul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'secretar' :
            false;

        if (!$este_administratorul_conectat &&
            !$este_directorul_conectat &&
            !$este_secretarul_conectat
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $searchModel = new ProfesorclasaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Profesorclasa model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;
        $este_secretarul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'secretar' :
            false;

        if (!$este_administratorul_conectat &&
            !$este_directorul_conectat &&
            !$este_secretarul_conectat
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Finds the Profesorclasa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Profesorclasa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profesorclasa::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Deletes an existing Profesorclasa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $este_administratorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'admin' :
            false;
        $este_directorul_conectat = isset(Yii::$app->session['user']->role) ?
            Yii::$app->session['user']->role == 'director' :
            false;

        if (!$este_administratorul_conectat &&
            !$este_directorul_conectat
        ) {
            Yii::$app->session->setFlash('error', 'Nu aveti drepturi pentru a accesa aceasta sectiune !!!');
            return $this->redirect([
                'site/index'
            ]);
        }
        $this->findModel($id)->delete();
        
        return $this->redirect([
            'index'
        ]);
    }
}
