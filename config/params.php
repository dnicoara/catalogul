<?php
$data = date('Y-m-d');
$luna = (int) date('m');

$anul1 = date('Y');
if ($luna >= 9)
    $anul2 = date('Y') + 1;
else {
    $anul2 = $anul1;
    $anul1 --;
}

$anul_scolar = (string) $anul1 . '-' . (string) $anul2; // obtin anul scolar in formatul : 2017-2018
$anul_scolar = '2018-2019';

$sem = array(
    'I' => 'I',
    'II' => 'II'
);
$sex = array(
    'M' => 'M',
    'F' => 'F'
);

$naveta = array(
    'nu' => 'nu',
    'da' => 'da'
);

$statut_absenta = array(
    'Nemotivata' => 'nemotivata',
    'Motivata' => 'motivata'
    //0 => 'nemotivata',
    //1 => 'motivata'
);
$bazadate = array(
    '2018-2019' => '2018-2019',
    '2019-2020' => '2019-2020',
    '2020-2021' => '2020-2021',
    '2021-2022' => '2021-2022'
);

$judete = array(
    'Alba' => 'Alba',
    'Arad' => 'Arad',
    'Arges' => 'Arges',
    'Bacau' => 'Bacau',
    'Bihor' => 'Bihor',
    'Bistrita-Nasaud' => 'Bistrita-Nasaud',
    'Botosani' => 'Botosani',
    'Brasov' => 'Brasov',
    'Braila' => 'Braila',
    'Buzau' => 'Buzau',
    'Caras-Severin' => 'Caras-Severin',
    'Calarasi' => 'Calarasi',
    'Cluj' => 'Cluj',
    'Constanta' => 'Constanta',
    'Covasna' => 'Covasna',
    'Dambovita' => 'Dambovita',
    'Dolj' => 'Dolj',
    'Galati' => 'Galati',
    'Giurgiu' => 'Giurgiu',
    'Gorj' => 'Gorj',
    'Harghita' => 'Harghita',
    'Hunedoara' => 'Hunedoara',
    'Iasi' => 'Iasi',
    'Ilfov' => 'Ilfov',
    'Maramures' => 'Maramures',
    'Mehedinti' => 'Mehedinti',
    'Mures' => 'Mures',
    'Neamt' => 'Neamt',
    'Olt' => 'Olt',
    'Prahova' => 'Prahova',
    'Satu-Mare' => 'Satu-Mare',
    'Salaj' => 'Salaj',
    'Sibiu' => 'Sibiu',
    'Suceava' => 'Suceava',
    'Teleorman' => 'Teleorman',
    'Timis' => 'Timis',
    'Tulcea' => 'Tulcea',
    'Vaslui' => 'Vaslui',
    'Valcea' => 'Valcea',
    'Vrancea' => 'Vrancea',

);

$clasele_scolii = array(
    'IX-A' => 'IX-A',
    'IX-B' => 'IX-B',
    'IX-C' => 'IX-C',
    'IX-D' => 'IX-D',
    'IX-E' => 'IX-E',
    'IX-F' => 'IX-F',
    'IX-G' => 'IX-G',
    'IX-H' => 'IX-H',
    'IX-I' => 'IX-I',
    'IX-J' => 'IX-J',
    'X-A' => 'X-A',
    'X-B' => 'X-B',
    'X-C' => 'X-C',
    'X-D' => 'X-D',
    'X-E' => 'X-E',
    'X-F' => 'X-F',
    'X-G' => 'X-G',
    'X-H' => 'X-H',
    'X-I' => 'X-I',
    'X-J' => 'X-J',
    'XI-A' => 'XI-A',
    'XI-B' => 'XI-B',
    'XI-C' => 'XI-C',
    'XI-D' => 'XI-D',
    'XI-E' => 'XI-E',
    'XI-F' => 'XI-F',
    'XI-G' => 'XI-G',
    'XI-H' => 'XI-H',
    'XI-I' => 'XI-I',
    'XI-J' => 'XI-J',
    'XII-A' => 'XII-A',
    'XII-B' => 'XII-B',
    'XII-C' => 'XII-C',
    'XII-D' => 'XII-D',
    'XII-E' => 'XII-E',
    'XII-F' => 'XII-F',
    'XII-G' => 'XII-G',
    'XII-H' => 'XII-H',
    'XII-I' => 'XII-I',
    'XII-J' => 'XII-J',
    'V' => 'V',
    'VI' => 'VI',
    'VII' => 'VII',
    'VIII' => 'VIII',
    'V-A' => 'V-A',
    'VI-A' => 'VI-A',
    'VII-A' => 'VII-A',
    'VIII-A' => 'VIII-A',
    'V-B' => 'V-B',
    'VI-B' => 'VI-B',
    'VII-B' => 'VII-B',
    'VIII-B' => 'VIII-B',
    'V-C' => 'V-C',
    'VI-C' => 'VI-C',
    'VII-C' => 'VII-C',
    'VIII-C' => 'VIII-C',
    'V-D' => 'V-D',
    'VI-D' => 'VI-D',
    'VII-D' => 'VII-D',
    'VIII-D' => 'VIII-D',

);

//Clasele IX  si X  - turism  au module  , dar la materiile : engleza , franceza si Tic nu au module
//Numai clasele XI si XII - turism au module la aceste materii
$clase_cu_modul_exceptate = array(
    0 => 'IX',
    1 => 'X'
);


return [
    'adminEmail' => 'administrator@test.catalogul-digital.ro',
    'anul_scolar' => $anul_scolar,
    'data' => $data,
    'luna' => $luna,
    'semestrul' => $sem,
    'sexul' => $sex,
    'naveta' => $naveta,
    'statut_absenta' => $statut_absenta,
    'judete' => $judete,
    'clasele_scolii' => $clasele_scolii,
    'clase_cu_modul_exceptate' => $clase_cu_modul_exceptate,
    'database' => $bazadate
];
